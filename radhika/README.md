|             |                                           |
| :---------- | ----------------------------------------: |
| **Name**    |                           Radhika Chhabra |
| **College** |                                    DAIICT |
| **Hobbies** | Badminton,Dancing,Listening Songs,Reading |

<br><br>

**Task 1:** _Code Test Using Jest_

**task1_jestTest_3Programs** repository contains the js code and test code for 3 problem statements:\

1. Binary Search
2. [FizzBuzz](https://www.geeksforgeeks.org/fizz-buzz-implementation/)
3. [Acm_IPC_Team](https://www.hackerrank.com/challenges/acm-icpc-team/problem)

filename.js is the problem solution and filename.test.js is the corresponding test file for the same.\
e.g. binary_search.js is the problem statement for binary search and binary_search.test.js is the corresponding test file.

### Instructions

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Run .js file

> node filename.js

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Run .test.js file

> npm test filename.test.js
