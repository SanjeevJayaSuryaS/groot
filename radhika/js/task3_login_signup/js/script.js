/* istanbul ignore file */
// validations for registration form
function validateUsername() {
  const username = document?.registration?.username
  const letters = /^[A-Za-z]+$/
  if (username.value.match(letters)) {
    document.getElementById('username_error_message').innerHTML = ''
    return true
  } else {
    document.getElementById('username_error_message').style.color = 'red'
    document.getElementById('username_error_message').innerHTML =
      'username can have only alphabets'
    document.getElementById('registration_btn').disabled = true
    return false
  }
}

function validateEmail() {
  const email = document?.registration?.registration_emailid
  const mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
  if (!email.value.match(mailformat)) {
    document.getElementById('email_error_message').style.color = 'red'
    document.getElementById('email_error_message').innerHTML = 'Invalid email'
    return false
  } else {
    document.getElementById('email_error_message').innerHTML = ''
    return true
  }
}

function validatePassword() {
  const password = document?.registration?.new_password
  if (!password.value.match(/[a-z]/)) {
    document.getElementById('password_error_message').style.color = 'red'
    document.getElementById('password_error_message').innerHTML =
      'Passwords must contain lowercase alphabets'
    return false
  } else if (!password.value.match(/[A-Z]/)) {
    document.getElementById('password_error_message').style.color = 'red'
    document.getElementById('password_error_message').innerHTML =
      'Passwords must contain uppercase alphabets'
    return false
  } else if (!password.value.match(/[0-9]/)) {
    document.getElementById('password_error_message').style.color = 'red'
    document.getElementById('password_error_message').innerHTML =
      'Passwords must contain digits'
    return false
  } else if (password.value.length < 7) {
    document.getElementById('password_error_message').style.color = 'red'
    document.getElementById('password_error_message').innerHTML =
      'Passwords length must be atleast 7'
    return false
  } else {
    document.getElementById('password_error_message').innerHTML = ''
    return true
  }
}

function validateConfirmPassword() {
  const password = document?.registration?.registration.new_password
  const confirmPassword = document?.registration?.registration.confirm_password
  if (password.value !== confirmPassword.value) {
    document.getElementById('confirmPassword_error_message').style.color = 'red'
    document.getElementById('confirmPassword_error_message').innerHTML =
      'Passwords did not match'
    return false
  }
  document.getElementById('confirmPassword_error_message').innerHTML = ''
  return true
}
// eslint-disable-next-line no-unused-vars
function validateRegistration() {
  if (
    validateUsername() &&
    validateEmail() &&
    validatePassword() &&
    validateConfirmPassword()
  ) {
    window.location.href = 'login.html'
  }
}

// validations for login form
function validateLoginEmail() {
  const email = document?.login?.login_email
  const mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
  if (!email.value.match(mailformat)) {
    document.getElementById('email_error_message').style.color = 'red'
    document.getElementById('email_error_message').innerHTML = 'Invalid email'
    return false
  } else {
    document.getElementById('email_error_message').innerHTML = ''
    return true
  }
}

function validateLoginPassword() {
  const password = document?.login?.login_password
  if (password.value.length < 7) {
    document.getElementById('password_error_message').style.color = 'red'
    document.getElementById('password_error_message').innerHTML =
      'Invalid password'
    return false
  } else {
    document.getElementById('password_error_message').innerHTML = ''
    return true
  }
}
// eslint-disable-next-line no-unused-vars
function validateLoginForm() {
  if (validateLoginEmail() && validateLoginPassword()) {
    alert('Successfully logged in')
  } else {
    alert('Invalid email or password')
  }
}
