const generateImage = require('./robohash')

const printImages = async () => {
  const imageIndex = ['1', '2', '3', '4', '5']
  return Promise.all(
    imageIndex.map((imageIndex) => generateImage.generateImage(imageIndex))
  ).then((value) => value.every((status) => status))
}
printImages()
module.exports = printImages
