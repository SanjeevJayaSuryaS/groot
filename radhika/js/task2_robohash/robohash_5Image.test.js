const generateImages = require('./robohash_5Image')

describe('robohash promise.all', () => {
  test('all images uploaded successfully', async () => {
    const status = await generateImages()
    expect(status).toBe(true)
  })
})
