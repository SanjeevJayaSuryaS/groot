function acmTeam(topic) {
  if (topic.length < 2) {
    throw new Error('Invalid input')
  }
  const n = topic.length
  const m = topic[0].length
  const max = [0, 0]

  for (let i = 0; i < n - 1; i++) {
    for (let j = i + 1; j < n; j++) {
      // check topics
      let sum = 0
      for (let t = 0; t < m; t++) {
        if (topic[i][t] | topic[j][t]) {
          sum++
        }
      }

      if (sum > max[0]) {
        max[0] = sum
        max[1] = 1
      } else if (sum === max[0]) {
        max[1]++
      }
    }
  }

  return max
}

module.exports = acmTeam
