const binarySearch = require('./binary_search')

describe('binary search', () => {
  test('undefined array throws erroe', () => {
    const bsresult = () => binarySearch(0, 0, 1, undefined)
    expect(bsresult).toThrow('Invalid Input')
  })
  test('number to be searched if not number throws error', () => {
    const bsresult = () => binarySearch(0, 5, 'a', [1, 2, 3, 4, 5])
    expect(bsresult).toThrow('Invalid Input')
  })
  test('element to be search=mid => return mid', () => {
    const bsresult = binarySearch(0, 5, 3, [1, 2, 3, 4, 5])
    expect(bsresult).toBe(2)
  })
  test('element to be search in left =>return 0', () => {
    const bsresult = binarySearch(0, 5, 1, [1, 2, 3, 4, 5])
    expect(bsresult).toBe(0)
  })
  test('element to be searched in right =>return 4', () => {
    const bsresult = binarySearch(0, 5, 5, [1, 2, 3, 4, 5])
    expect(bsresult).toBe(4)
  })
  test('felement not found=>return -1', () => {
    const bsresult = binarySearch(0, 5, 10, [1, 2, 3, 4, 5])
    expect(bsresult).toBe(-1)
  })
})
