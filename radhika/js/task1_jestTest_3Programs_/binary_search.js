function binarySearch(left, right, numToSearch, arr) {
  if (!Array.isArray(arr) || isNaN(numToSearch)) {
    throw new Error('Invalid Input')
  }
  if (left < right) {
    const mid = left + Math.floor((right - left) / 2)
    if (arr[mid] === numToSearch) {
      return mid
    } else if (arr[mid] > numToSearch) {
      return binarySearch(left, mid, numToSearch, arr)
    } else {
      return binarySearch(mid + 1, right, numToSearch, arr)
    }
  }
  return -1
}

module.exports = binarySearch
