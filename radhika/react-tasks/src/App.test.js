import React from 'react'
import App from './App'
import '@testing-library/react'
import renderer from 'react-test-renderer'

describe('App.js', () => {
  test('App must render', () => {
    const tree = renderer.create(<App />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
