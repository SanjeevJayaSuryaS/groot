import React, { useState } from 'react'
import { NotRegistered } from '../button/NotRegisteredBtn'
import Email from '../email'
import Password from '../password/LoginPassword.jsx'
import LoginButton from '../button/LoginBtn'
import Header from '../Header'
import './forms.css'

const Login = () => {
  const [emailStatus, setEmailStatus] = useState({ emailStatus: true })
  const [passwordStatus, setPasswordStatus] = useState({
    passwordStatus: true
  })

  const successLogin = () => {
    /* istanbul ignore next */ if (!(emailStatus && passwordStatus))
      alert('successfully logged in')
    else alert('Invalid email or password')
  }

  return (
    <div>
      <Header />
      <section className="login">
        <form name="login" className="form">
          <h1>Sign In</h1>
          <Email emailValidate={setEmailStatus} />
          <Password passwordValidate={setPasswordStatus} />
          <LoginButton handleClick={successLogin} />
          <NotRegistered />
        </form>
      </section>
    </div>
  )
}

export default Login
