import React, { useState } from 'react'

export const USERNAME_REQUIRED_MESSAGE = 'Username is required'
export const USERNAME_INVALID_MESSAGE = 'Only characters'

// eslint-disable-next-line react/prop-types
const Username = ({ usernameValidate }) => {
  const [username, setUsername] = useState()
  const [error, setError] = useState()

  function usernameValidation(event) {
    let valid = false
    const name = event?.target?.value
    setUsername(name)
    const regex = /[A-Za-z]/
    if (!name) {
      setError(USERNAME_REQUIRED_MESSAGE)
      valid = true
    } else if (!name.match(regex)) {
      setError(USERNAME_INVALID_MESSAGE)
      valid = true
    } else {
      setError('')
    }
    usernameValidate(valid)
  }

  return (
    <div>
      <input
        className="form_content-input"
        type="text"
        placeholder="username"
        name="username"
        onChange={usernameValidation}
        value={username}
      />
      <span data-testid="error">{error}</span>
    </div>
  )
}

export default Username
