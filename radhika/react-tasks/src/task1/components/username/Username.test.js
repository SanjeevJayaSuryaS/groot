import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import Username, {
  USERNAME_INVALID_MESSAGE,
  USERNAME_REQUIRED_MESSAGE
} from './index'

const setup = () => {
  const mockFunc = jest.fn()
  const UsernameComponent = render(<Username usernameValidate={mockFunc} />)
  const input = UsernameComponent.getByPlaceholderText('username')
  return {
    input,
    ...UsernameComponent
  }
}

describe('Username Validation', () => {
  test('Is Username Valid', () => {
    const { input, ...UsernameComponent } = setup()
    fireEvent.change(input, { target: { value: 'radhika' } })
    const errorValue = UsernameComponent.getByTestId('error').textContent
    expect(errorValue).toBe('')
  })

  test('Username must only contain alphabats', () => {
    const { input, ...UsernameComponent } = setup()
    fireEvent.change(input, { target: { value: '123' } })
    const errorValue = UsernameComponent.getByTestId('error').textContent
    expect(errorValue).toBe(USERNAME_INVALID_MESSAGE)
  })

  test('no input give error Username required', () => {
    const { input, ...UsernameComponent } = setup()
    fireEvent.change(input, { target: { value: 'test' } })
    fireEvent.change(input, { target: { value: '' } })
    const errorValue = UsernameComponent.getByTestId('error').textContent
    expect(errorValue).toBe(USERNAME_REQUIRED_MESSAGE)
  })
})
