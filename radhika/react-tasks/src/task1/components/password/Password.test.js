import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import Password, {
  PASSWORD_REQUIRED_MESSAGE,
  PASSWORD_INVALID_MESSAGE,
  PASSWORD_NOT_MATCHED_MESSAGE
} from './Password.jsx'

const setup = () => {
  const mockFunc = jest.fn()
  const PASSWORDComponent = render(<Password passwordValidate={mockFunc} />)
  const inputPassword = PASSWORDComponent.getByPlaceholderText('password')
  const inputConfirmPassword = PASSWORDComponent.getByPlaceholderText(
    'confirm password'
  )
  return {
    inputPassword,
    inputConfirmPassword,
    ...PASSWORDComponent
  }
}

describe('PASSWORD Validation', () => {
  test('Is PASSWORD Valid', () => {
    const { inputPassword, ...PASSWORDComponent } = setup()
    fireEvent.change(inputPassword, { target: { value: '12345678' } })
    const errorValue = PASSWORDComponent.getByTestId('passwordError')
      .textContent
    expect(errorValue).toBe('')
  })
  test('password is required', () => {
    const { inputPassword, ...PASSWORDComponent } = setup()
    fireEvent.change(inputPassword, { target: { value: '1234567' } })
    fireEvent.change(inputPassword, { target: { value: null } })
    const errorValue = PASSWORDComponent.getByTestId('passwordError')
      .textContent
    expect(errorValue).toBe(PASSWORD_REQUIRED_MESSAGE)
  })
  test('password length should be greater than or equal to 8', () => {
    const { inputPassword, ...PASSWORDComponent } = setup()
    fireEvent.change(inputPassword, { target: { value: '1234567' } })
    const errorValue = PASSWORDComponent.getByTestId('passwordError')
      .textContent
    expect(errorValue).toBe(PASSWORD_INVALID_MESSAGE)
  })
})

describe('CONFIRM PASSWORD Validation', () => {
  test('confirm password is same as the password', () => {
    const {
      inputPassword,
      inputConfirmPassword,
      ...PASSWORDComponent
    } = setup()
    fireEvent.change(inputPassword, { target: { value: '12345678' } })
    fireEvent.change(inputConfirmPassword, { target: { value: '12345678' } })
    const errorValue = PASSWORDComponent.getByTestId('confirmPasswordError')
      .textContent
    expect(errorValue).toBe('')
  })
  test('confirm password is not same as the password', () => {
    const {
      inputPassword,
      inputConfirmPassword,
      ...PASSWORDComponent
    } = setup()
    fireEvent.change(inputPassword, { target: { value: '12345678' } })
    fireEvent.change(inputConfirmPassword, { target: { value: '123' } })
    const errorValue = PASSWORDComponent.getByTestId('confirmPasswordError')
      .textContent
    expect(errorValue).toBe(PASSWORD_NOT_MATCHED_MESSAGE)
  })
  test('confirm password is required', () => {
    const {
      inputPassword,
      inputConfirmPassword,
      ...PASSWORDComponent
    } = setup()
    fireEvent.change(inputPassword, { target: { value: '12345678' } })
    fireEvent.change(inputConfirmPassword, { target: { value: '123' } })
    fireEvent.change(inputConfirmPassword, { target: { value: '' } })
    const errorValue = PASSWORDComponent.getByTestId('confirmPasswordError')
      .textContent
    expect(errorValue).toBe(PASSWORD_REQUIRED_MESSAGE)
  })
})
