import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import Email, { EMAIL_REQUIRED_MESSAGE, EMAIL_INVALID_MESSAGE } from './index'

const setup = () => {
  const mockFunc = jest.fn()
  const EmailComponent = render(<Email emailValidate={mockFunc} />)
  const input = EmailComponent.getByPlaceholderText('email')
  return {
    input,
    ...EmailComponent
  }
}

describe('Email Validation', () => {
  test('Is email Valid', () => {
    const { input, ...EmailComponent } = setup()
    fireEvent.change(input, { target: { value: 'radhika@gmil.com' } })
    const errorValue = EmailComponent.getByTestId('error').textContent
    expect(errorValue).toBe('')
  })

  test('@ not present throw invalid email', () => {
    const { input, ...EmailComponent } = setup()
    fireEvent.change(input, { target: { value: 'radhika' } })
    const errorValue = EmailComponent.getByTestId('error').textContent
    expect(errorValue).toBe(EMAIL_INVALID_MESSAGE)
  })

  test('. not present throw invalid email', () => {
    const { input, ...EmailComponent } = setup()
    fireEvent.change(input, { target: { value: 'radhika.' } })
    const errorValue = EmailComponent.getByTestId('error').textContent
    expect(errorValue).toBe(EMAIL_INVALID_MESSAGE)
  })
  test('length less than 2 throw invalid email', () => {
    const { input, ...EmailComponent } = setup()
    fireEvent.change(input, { target: { value: 'r' } })
    const errorValue = EmailComponent.getByTestId('error').textContent
    expect(errorValue).toBe(EMAIL_INVALID_MESSAGE)
  })

  test('no input give error email required', () => {
    const { input, ...EmailComponent } = setup()

    fireEvent.change(input, { target: { value: 'test' } })
    fireEvent.change(input, { target: { value: '' } })
    const errorValue = EmailComponent.getByTestId('error').textContent
    expect(errorValue).toBe(EMAIL_REQUIRED_MESSAGE)
  })
})
