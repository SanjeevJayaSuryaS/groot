import React from 'react'
import { BrowserRouter as Router, Route, useHistory } from 'react-router-dom'
import Login from '../../forms/login.jsx'

// eslint-disable-next-line react/prop-types
export const RegisterButton = ({ value }) => {
  const history = useHistory()
  const handleClick = () => {
    history.push('/login')
  }
  return (
    <button
      type="button"
      className="form_content-btn"
      onClick={handleClick}
      disabled={value}
      data-testid="register"
    >
      Register
    </button>
  )
}

function RegisterApp() {
  return (
    <Router>
      <Route path="/login" exact component={Login} />
      <Route path="/" exact component={RegisterButton} />
    </Router>
  )
}

export default RegisterApp
