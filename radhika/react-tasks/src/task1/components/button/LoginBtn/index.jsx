import React from 'react'

// eslint-disable-next-line react/prop-types
const LoginButton = ({ handleClick }) => {
  return (
    <button
      className="form_content-btn"
      onClick={handleClick}
      data-testid="login"
    >
      Login
    </button>
  )
}

export default LoginButton
