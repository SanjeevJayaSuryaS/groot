import React from 'react'
import { render } from '@testing-library/react'
import LoginBtn from './index'

describe('Login Button', () => {
  test('login button must render', () => {
    const mockFunc = jest.fn()
    const btnComponent = render(<LoginBtn handleClick={mockFunc} />)
    const btn = btnComponent.getByTestId('login')
    expect(btn).toBeInTheDocument()
  })
})
