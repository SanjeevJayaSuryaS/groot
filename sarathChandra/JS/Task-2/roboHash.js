const fs = require('fs')
const fetch = require('fetch')

// utility functions:
function generateRandomString(stringLength) {
  let result = ''
  const characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
  const charactersLength = characters.length
  for (let i = 0; i < stringLength; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength))
  }
  return result
}

// promises:
const stringGenerationPromise = (stringLength) => {
  return new Promise((resolve, reject) => {
    if (typeof stringLength !== 'number') reject(new Error(''))
    resolve(generateRandomString(stringLength))
  })
}

const imageRequestPromise = (string) => {
  return new Promise((resolve, reject) => {
    fetch.fetchUrl(`https://robohash.org/${string}`, (e, meta, data) => {
      resolve(data)
    })
  })
}

const writeFilePromise = (data, fileName) => {
  return new Promise((resolve, reject) => {
    fs.writeFile('./Task-2/imagesGenerated/' + fileName, data, () => {
      resolve()
    })
  })
}

async function roboHash(stringLengths) {
  try {
    const strings = await Promise.all(
      stringLengths.map((item) => stringGenerationPromise(item))
    )
    const data = await Promise.all(
      strings.map((item) => imageRequestPromise(item))
    )
    await Promise.all(
      data.map((item, index) => {
        return writeFilePromise(item, index + 1 + '.png')
      })
    )
  } catch (err) {}
}

module.exports = roboHash
