const contestInfo = require('./codeForcesContest')

describe('Should test codeForcesContest', () => {
  test('should console log  6 times', async () => {
    const spy = jest.spyOn(console, 'log').mockImplementation()
    const contestId = 566
    await contestInfo(contestId)
    expect(spy).toHaveBeenCalledTimes(6)
    expect(spy.mock.calls[0][0]).toEqual('filtered rows with 1 member:')
    expect(spy.mock.calls[2][0]).toEqual('\n extracted member handles:')
    expect(spy.mock.calls[4][0]).toEqual(
      '\n extracted all newRatings of handles:'
    )
  })
  test('should console log error', async () => {
    const spy = jest.spyOn(console, 'error').mockImplementation()
    const contestId = undefined
    await contestInfo(contestId)
    expect(spy).toHaveBeenCalledTimes(1)
  })
})
