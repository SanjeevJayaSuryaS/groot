const roboHashAsyncAwait = require('./roboHashAsyncAwait')
const fs = require('fs')

const fileExists = (file) => {
  try {
    if (fs.existsSync(file)) {
      return true
    } else {
      return false
    }
  } catch (err) {
    console.error(err)
  }
}

describe('Test roboHash()', () => {
  test('should console log: task successfully completed for given length and check for file exists', async () => {
    const stringLength = 5
    const roboName = 'robo.png'
    await roboHashAsyncAwait(stringLength, roboName)
    const response = fileExists(roboName)
    expect(response).toBe(true)
  })
  test('should console log: type a number for length for given length', async () => {
    const stringLength = undefined
    const roboName = 'robo2.png'
    await roboHashAsyncAwait(stringLength, roboName)
    const response = fileExists(roboName)
    expect(response).toBe(false)
  })
})
