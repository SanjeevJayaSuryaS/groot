/* istanbul ignore file */
// eslint-disable-next-line no-unused-vars
function check() {
  const firstNameValue = document.forms.inputForm.fName.value
  const lastNameValue = document.forms.inputForm.lName.value
  const emailValue = document.forms.inputForm.email.value
  const passwordValue = document.forms.inputForm.pass.value

  const firstNameDOM = document.getElementById('fName')
  const lastNameDOM = document.getElementById('lName')
  const emailDOM = document.getElementById('email')
  const passwordDOM = document.getElementById('pass')

  const errorOneDOM = document.getElementById('error1')
  const errorTwoDOM = document.getElementById('error2')
  const errorThreeDOM = document.getElementById('error3')
  const errorFourDOM = document.getElementById('error4')

  // firstNameField :
  if (firstNameValue === '') {
    firstNameDOM.classList.add('error-display')
    errorOneDOM.classList.add('error-message')
  } else {
    firstNameDOM.classList.remove('error-display')
    errorOneDOM.classList.remove('error-message')
  }

  // lastNameField:
  if (lastNameValue === '') {
    lastNameDOM.classList.add('error-display')
    errorTwoDOM.classList.add('error-message')
  } else {
    lastNameDOM.classList.remove('error-display')
    errorTwoDOM.classList.remove('error-message')
  }

  // emailField:
  if (
    emailValue === '' ||
    !emailValue.match(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/)
  ) {
    emailDOM.classList.add('error-display')
    errorThreeDOM.classList.add('error-message')
  } else {
    emailDOM.classList.remove('error-display')
    errorThreeDOM.classList.remove('error-message')
  }

  // passwordField:
  if (passwordValue === '') {
    passwordDOM.classList.add('error-display')
    errorFourDOM.classList.add('error-message')
  } else {
    passwordDOM.classList.remove('error-display')
    errorFourDOM.classList.remove('error-message')
  }
}
