/*
Input: leftIndex and rightIndex of array for search number in this bound, number to be search
and array of element.
Return : index of element in array (-1 if not found)
*/

function binarySearch(leftIndex, rightIndex, number, array) {
  if (!Array.isArray(array) || isNaN(number)) {
    throw new Error('Invalid Input')
  }
  while (rightIndex >= leftIndex) {
    const mid = leftIndex + Math.floor((rightIndex - leftIndex) / 2)
    if (array[mid] === number) {
      return mid
    } else if (array[mid] > number) {
      rightIndex = mid - 1
    } else {
      leftIndex = mid + 1
    }
  }
  return -1
}

module.exports = binarySearch
