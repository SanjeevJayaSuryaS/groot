const binarySearch = require('./binary_search')
describe('binarySearch', () => {
  test('function should return error', () => {
    const result = () => binarySearch(0, 4, 1, undefined)
    expect(result).toThrow('Invalid Input')
  })
  test('function should return error for Invalid element to be search', () => {
    const result = () => binarySearch(0, 4, 'dilip', [1, 2, 3, 4, 5])
    expect(result).toThrow('Invalid Input')
  })
  test('function should return index 2', () => {
    const result = binarySearch(0, 4, 3, [1, 2, 3, 4, 5])
    expect(result).toBe(2)
  })
  test('function should return index 0', () => {
    const result = binarySearch(0, 4, 1, [1, 2, 3, 4, 5])
    expect(result).toBe(0)
  })
  test('function should return index 4', () => {
    const bin = binarySearch(0, 4, 5, [1, 2, 3, 4, 5])
    expect(bin).toBe(4)
  })
  test('function should return index -1', () => {
    const result = binarySearch(0, 4, 6, [1, 2, 3, 4, 5])
    expect(result).toBe(-1)
  })
})
