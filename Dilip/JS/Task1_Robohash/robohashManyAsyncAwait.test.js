const generateRobots = require('./robohashManyAsyncAwait')
const singleImage = require('./robohashAsyncAwait')
describe('Robohash for 5 Images', () => {
  test('testing for RobohashMany for promise.all', async () => {
    const allImageStatus = await generateRobots()
    expect(allImageStatus).toBe(true)
  })
  test('testing for RobohashAsyncAwait for single image', async () => {
    const singleImageAsynsAwait = await singleImage()
    await expect(singleImageAsynsAwait).toBe(true)
  })
})
