const fs = require('fs')
const myRobot = require('./robohash')
describe('Robohash', () => {
  test('Image should be uploded successfully', async () => {
    await myRobot(0)
    const doesImageExist = fs.existsSync('robot0.jpg')
    expect(doesImageExist).toBeTruthy()
  })
  test('When file not exist should through error', async () => {
    await myRobot(0)
    const doesImageExist = fs.existsSync(undefined)
    expect(doesImageExist).toBeFalsy()
  })
})
