/*
This code will generate a Random Image of robot and put it to folder
named robohashImages with name 'robot0.jpg'
Here I used Promise.then method.
*/

const axios = require('axios')
const fs = require('fs')

const myURL = 'https://robohash.org/'

function generateString() {
  return new Promise((resolve) => {
    const randomString = Math.random().toString(36).substring(7)
    resolve(randomString)
  })
}

function getImage(myURL) {
  return axios.get(myURL, { responseType: 'arraybuffer' })
}

function writeToFile(image, index) {
  return new Promise((resolve) => {
    const loc = './'
    fs.writeFile(loc + 'robot' + index + '.jpg', image.data, () =>
      resolve(true)
    )
  })
}

function getAndWriteImage(index) {
  const imageStatus = generateString()
    .then((str) => getImage(myURL + str))
    .then((response) => {
      writeToFile(response, index).then((msg) => msg)
    })
  return imageStatus
}

getAndWriteImage(0)
module.exports = getAndWriteImage
