# Repository

## Directory Structure

![readme and personal folder](./start_readme.png)

For the first time, create a branch with your name and create a Readme with following content.

- Name: Vipin Dev S
- College: NMAM Institute of Technology, Nitte
- Hobbies: Badminton, Cricket, Reading
- Website:

Any other detail you wanna add to make it cool!

![directory structure](./directory_structure.png)

## React Directory Structure

Whenever you create a react project, make sure that you do that inside your personal directory(eg. Vipin) and name the project as react. All the tasks will come under a single project and each task will be having their own components.

![React Directory Structure](./react_directory_str.png)

## Branching

Whenever you receive a task create a branch with your name followed by the task and number,
vipin/js/task1

## fixing all the lint error

`npm run lint:all:fix`

## get the coverage report

`npm run test:coverage`
