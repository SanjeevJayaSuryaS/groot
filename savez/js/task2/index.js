const { default: axios } = require('axios')
const fs = require('fs')

const readFile = () => {
  return new Promise((resolve, reject) => {
    fs.readFile('./task2/toBeRead.txt', 'utf8', (err, data) => {
      if (err) reject(err)
      else resolve(data)
    })
  })
}

const getImage = async (breed) => {
  try {
    const resp = await axios.get(
      `https://dog.ceo/api/breed/${breed}/images/random`
    )
    return resp.data.message
  } catch (error) {
    console.error(error.message)
  }
}

const getImages = async (breed) => {
  try {
    const promises = new Array(5).fill(0).map(() => getImage(breed))
    const images = await Promise.all(promises)
    return images
  } catch (err) {
    console.error(err.message)
  }
}

const writeFile = (url) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(
      './task2/toBeWritten.txt',
      JSON.stringify(url, undefined, 2),
      (err) => {
        if (err) reject(err)
        else resolve('File Written')
      }
    )
  })
}

const main = async () => {
  try {
    const breed = await readFile()
    const images = await getImages(breed)
    await writeFile(images)
  } catch (error) {
    console.error(error.message)
  }
}

module.exports = { readFile, writeFile, getImage, getImages, main }
