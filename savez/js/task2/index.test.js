const fs = require('fs')
const axios = require('axios')
const { readFile, writeFile, getImage, main, getImages } = require('./index')

beforeEach(() => jest.restoreAllMocks())

afterEach(() =>
  fs.unlink('./task2/toBeWritten.txt', (err) => {
    if (err) console.error(err)
  })
)

describe('tests for getImage', () => {
  test(`should return 1234567`, async () => {
    const spy = jest
      .spyOn(axios, 'get')
      .mockImplementation(() =>
        Promise.resolve({ data: { message: '1234567' } })
      )

    const resp1 = await getImage('african')
    expect(resp1).toBe('1234567')
    expect(spy).toHaveBeenCalledTimes(1)
  })

  test(`should log an Error`, async () => {
    const axiosSpy = jest
      .spyOn(axios, 'get')
      .mockImplementation(() => Promise.reject(new Error('404 not found')))
    await getImage('dummy url')
    expect(axiosSpy).toHaveBeenCalledTimes(1)
  })
})

describe('tests for writeFile', () => {
  test('should generate file with image url', async () => {
    const spy = jest.spyOn(fs, 'writeFile').mockImplementation((_, __, cb) => {
      cb()
    })

    const data = await writeFile('https://dummy/url')
    expect(data).toBe('File Written')
    expect(spy).toHaveBeenCalledTimes(1)
  })

  test('should reject if file not generated', () => {
    jest.spyOn(fs, 'writeFile').mockImplementation((_, __, cb) => {
      cb(new Error('something went wrong'))
    })

    return expect(writeFile('https://dummy/url')).rejects.toMatchSnapshot()
  })
})

describe('tests for readFile', () => {
  test('should read breed from file', async () => {
    const spy = jest.spyOn(fs, 'readFile').mockImplementation((_, __, cb) => {
      cb()
    })

    await readFile()
    expect(spy).toHaveBeenCalledTimes(1)
  })

  test('should throw an Error', async () => {
    const spy = jest.spyOn(fs, 'readFile').mockImplementation((_, __, cb) => {
      cb(new Error('something went wrong'))
    })

    await expect(readFile()).rejects.toThrow()
    expect(spy).toHaveBeenCalledTimes(1)
  })
})

describe('tests main', () => {
  test('should write to file (check axios)', async () => {
    const axiosSpy = jest
      .spyOn(axios, 'get')
      .mockImplementation(() =>
        Promise.resolve({ data: { message: '1234567' } })
      )

    await main()
    expect(axiosSpy).toHaveBeenCalledTimes(5)
  })

  test('Should write to file (check readFile)', async () => {
    const readFileSpy = jest
      .spyOn(fs, 'readFile')
      .mockImplementation((_, __, cb) => {
        cb()
      })

    await main()
    expect(readFileSpy).toHaveBeenCalledTimes(1)
  })

  test('Should write to file (check writeFile)', async () => {
    const writeFileSpy = jest
      .spyOn(fs, 'writeFile')
      .mockImplementation((_, __, cb) => {
        cb()
      })

    await main()
    expect(writeFileSpy).toHaveBeenCalledTimes(1)
  })

  test('should reject due to read Error', async () => {
    const spy = jest.spyOn(fs, 'readFile').mockImplementation((_, __, cb) => {
      cb(new Error('something went wrong'))
    })

    await main()
    expect(spy).toHaveBeenCalledTimes(1)
  })
})

describe('test getImages', () => {
  test('should log an error', async () => {
    const fetchSpy = jest
      .spyOn(Promise, 'all')
      .mockImplementation(() =>
        Promise.reject(new Error('Error in Promise.all'))
      )
    await getImages('african')
    expect(fetchSpy).toHaveBeenCalledTimes(1)
  })
})
