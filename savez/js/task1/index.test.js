const fs = require('fs')
const axios = require('axios')
const { readFile, writeFile, fetchUrl, main } = require('./index')

beforeEach(() => jest.restoreAllMocks())

afterEach(() =>
  fs.unlink('./task1/toBeWritten.txt', (err) => {
    if (err) console.error(err)
  })
)

describe('axiosGetSpy', () => {
  test(`should return 1234567`, () => {
    const spy = jest
      .spyOn(axios, 'get')
      .mockImplementation(() =>
        Promise.resolve({ data: { message: '1234567' } })
      )

    return fetchUrl('african').then((data) => {
      expect(data).toBe('1234567')
      expect(spy).toHaveBeenCalledTimes(1)
    })
  })

  test(`should throw an Error`, () => {
    const spy = jest
      .spyOn(axios, 'get')
      .mockImplementation(() => Promise.reject(new Error('404 not found')))

    return fetchUrl('african').catch(() => {
      expect(spy).toHaveBeenCalledTimes(1)
    })
  })
})

describe('fsWriteFileSpy', () => {
  test('should generate file with image url', () => {
    const spy = jest.spyOn(fs, 'writeFile').mockImplementation((_, __, cb) => {
      cb()
    })

    return writeFile('https://dummy/url').then((data) => {
      expect(data).toBe('File Written')
      expect(spy).toHaveBeenCalledTimes(1)
    })
  })

  test('should reject if file not generated', () => {
    jest.spyOn(fs, 'writeFile').mockImplementation((_, __, cb) => {
      cb(new Error('something went wrong'))
    })

    return expect(writeFile('https://dummy/url')).rejects.toMatchSnapshot()
  })
})

describe('readFileSpy', () => {
  test('should read breed from file', () => {
    const spy = jest.spyOn(fs, 'readFile').mockImplementation((_, __, cb) => {
      cb()
    })

    return readFile().then(() => {
      expect(spy).toHaveBeenCalledTimes(1)
    })
  })

  test('should throw an Error', () => {
    const spy = jest.spyOn(fs, 'readFile').mockImplementation((_, __, cb) => {
      cb(new Error('something went wrong'))
    })

    return readFile().catch(() => {
      expect(spy).toHaveBeenCalledTimes(1)
    })
  })
})

describe('test for main function', () => {
  test('should return true', () => {
    return expect(main()).resolves.toBe(true)
  })

  test('should reject', () => {
    const spy = jest
      .spyOn(axios, 'get')
      .mockImplementation(() => Promise.reject(new Error('404 not found')))

    return main().catch(() => {
      expect(spy).toHaveBeenCalledTimes(1)
    })
  })
})
