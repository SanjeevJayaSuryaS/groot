const { default: axios } = require('axios')
const fs = require('fs')

const readFile = () => {
  return new Promise((resolve, reject) => {
    fs.readFile('./task1/toBeRead.txt', 'utf8', (err, data) => {
      if (err) reject(new Error('Read Error'))
      else resolve(data)
    })
  })
}

const fetchUrl = (breed) => {
  return new Promise((resolve, reject) => {
    axios
      .get(`https://dog.ceo/api/breed/${breed}/images/random`)
      .then((resp) => {
        const { message } = resp.data
        resolve(message)
      })
      .catch((err) => {
        reject(err)
      })
  })
}

const writeFile = (url) => {
  return new Promise((resolve, reject) => {
    fs.writeFile('./task1/toBeWritten.txt', url, (err) => {
      if (err) reject(err)
      else resolve('File Written')
    })
  })
}

const main = () => {
  return new Promise((resolve, reject) => {
    readFile()
      .then((breed) => fetchUrl(breed))
      .then((url) => writeFile(url))
      .then(() => {
        resolve(true)
      })
      .catch((err) => {
        reject(err)
      })
  })
}

module.exports = { readFile, fetchUrl, writeFile, main }
