const express = require('express')
const cookieParser = require('cookie-parser')
const path = require('path')
const cors = require('cors')
const app = express()
const port = 3000

// Middlewares
app.use(cors())
app.use(express.static(path.join(__dirname, 'public')))
app.set('view engine', 'ejs')
app.use(express.json())
app.use(cookieParser())

app.get('/', (req, res) => {
  res.render(path.join(__dirname, './views/home'))
})

app.post('/login', (req, res) => {
  const { token } = req.body
  res.cookie('session-token', token)
  res.send('success')
})

app.get('/logout', (req, res) => {
  res.clearCookie('session-token')
  res.send('cookies cleared')
})

app.get('/task3', (req, res) => {
  res.render(path.join(__dirname, './views/task3'))
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
