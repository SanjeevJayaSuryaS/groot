const express = require('express')
const cors = require('cors')
const matchesPlayedPerYear = require('../task3/output/matchesPlayedPerYear.json')
const matchesWonPerTeamPerYear = require('../task3/output/matchesWonPerTeamPerYear.json')
const extraRunsPerTeamIn2016 = require('../task3/output/extraRunsPerTeamIn2016.json')
const app = express()
const port = 3000

app.use(cors())
app.use(express.static('public'))

app.get('/matchesPlayedPerYear', (req, res) => res.json(matchesPlayedPerYear))
app.get('/matchesWonPerTeamPerYear', (req, res) =>
  res.json(matchesWonPerTeamPerYear)
)
app.get('/extraRunsPerTeamIn2016', (req, res) =>
  res.json(extraRunsPerTeamIn2016)
)

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
