/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const drawBarGraph = async () => {
  const resp = await fetch('http://localhost:3000/matchesPlayedPerYear')
  const data = await resp.json()

  const svg = d3.select('div#subtask1').append('svg')
  svg
    .attr('preserveAspectRatio', 'xMinYMin meet')
    .attr('height', '500')
    .attr('width', '600')

  const margin = 200
  const width = svg.attr('width') - margin
  const height = svg.attr('height') - margin
  const xScale = d3.scaleBand().range([0, width]).padding(0.4)
  const yScale = d3.scaleLinear().range([height, 0])

  const g = svg
    .append('g')
    .attr('transform', 'translate(' + 100 + ',' + 100 + ')')

  xScale.domain(Object.keys(data))
  yScale.domain([0, 100])

  // adds the x-axis
  g.append('g')
    .attr('transform', 'translate(0,' + height + ')')
    .call(d3.axisBottom(xScale))

  // adds the y-axis
  g.append('g').call(d3.axisLeft(yScale))

  // adds the bars
  g.selectAll('.bar')
    .data(Object.entries(data))
    .enter()
    .append('rect')
    .attr('fill', 'steelblue')
    .attr('x', ([key, value]) => xScale(key))
    .attr('y', ([key, value]) => yScale(value))
    .attr('width', xScale.bandwidth())
    .attr('height', ([key, value]) => height - yScale(value))

  svg
    .append('text')
    .attr('transform', 'translate(200,0)')
    .attr('x', 50)
    .attr('y', 50)
    .attr('font-size', '24px')
    .text('Subtask 1')

  g.append('g')
    .attr('transform', 'translate(0,' + height + ')')
    .call(d3.axisBottom(xScale))
    .append('text')
    .attr('y', height - 250)
    .attr('x', width - 100)
    .attr('text-anchor', 'end')
    .attr('stroke', 'black')
    .text('Year')

  g.append('g')
    .call(d3.axisLeft(yScale))
    .append('text')
    .attr('transform', 'rotate(-90)')
    .attr('y', 6)
    .attr('dy', '-5.1em')
    .attr('text-anchor', 'end')
    .attr('stroke', 'black')
    .text('Matches Played')
}

const drawLineChart = async () => {
  const resp = await fetch('http://localhost:3000/matchesWonPerTeamPerYear')
  const data = await resp.json()
  const seriesData = Object.values(data).map((item) => Object.entries(item))
  const seriesLabel = Object.keys(data)

  const xLabels = Array.from({ length: 12 }, (_, i) => (i + 2008).toString())

  const chartOptions = {
    title: {
      text: 'Subtask2'
    },
    subtitle: {
      text: 'matches won per team per year'
    },
    xAxis: {
      categories: xLabels,
      title: {
        text: 'Years'
      }
    },
    series: seriesLabel.map((label, i) => {
      return {
        data: seriesData[i],
        turboThreshold: 0,
        type: 'line',
        name: label
      }
    }),
    yAxis: [
      {
        index: 0,
        alignTicks: false,
        max: 15,
        min: 0,
        title: {
          text: 'matches won'
        },
        labels: {}
      }
    ]
  }

  Highcharts.chart('subtask2', chartOptions)
}

const drawBarChart = async () => {
  const resp = await fetch('http://localhost:3000/extraRunsPerTeamIn2016')
  const data = await resp.json()
  const seriesData = Object.entries(data)

  Highcharts.chart('subtask3', {
    chart: {
      type: 'bar'
    },
    title: {
      text: 'Subtask 3'
    },
    subtitle: {
      text: 'extra runs per team in 2016'
    },
    xAxis: {
      categories: seriesData.map(([key, value]) => key)
    },
    yAxis: {
      min: 0,
      title: {
        text: 'runs'
      }
    },
    plotOptions: {
      bar: {
        dataLabels: {
          enabled: true
        }
      }
    },
    series: [
      {
        name: 'runs',
        data: seriesData.map(([key, value]) => value)
      }
    ]
  })
}

const download = async (filename) => {
  const element = document.createElement('a')
  const resp = await fetch(`http://localhost:3000/${filename}`)
  const data = await resp.json()
  const text = JSON.stringify(data, null, 2)
  element.setAttribute(
    'href',
    'data:text/plain;charset=utf-8,' + encodeURIComponent(text)
  )
  element.setAttribute('download', `${filename}.json`)
  element.style.display = 'none'
  document.body.appendChild(element)
  element.click()
  document.body.removeChild(element)
}
