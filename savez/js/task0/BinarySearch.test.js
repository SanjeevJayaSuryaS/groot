const binarySearch = require('./BinarySearch')

// if element is present in the array
test(`Should return index of element in array`, () => {
  expect(binarySearch([15, 20, 34, 44, 56, 69, 71, 82, 96, 104], 56)).toBe(5)
})

// if element is not present in the array
test(`Should return -1`, () => {
  expect(binarySearch([15, 20, 34, 44, 56, 69, 71, 82, 96, 104], 5)).toBe(-1)
})

// when A is not an array
test('Should give an Error when A is not an array', () => {
  expect(() => {
    binarySearch(34, 6)
  }).toThrowError('Invalid Input')
})

// when key is NaN
test('Should give an Error', () => {
  expect(() => {
    binarySearch([1, 2, 3, 4, 5], null)
  }).toThrowError('Invalid Input')
})

// when array is not sorted
test('Should give an Error when array is not sorted', () => {
  expect(() => {
    binarySearch([1, 3, 2, 5, 7], 7)
  }).toThrowError('Array must be sorted')
})
