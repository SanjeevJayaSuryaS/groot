const fizzBuzz = (n) => {
  if (isNaN(n) || n <= 0) throw new Error('Invalid Input')

  const ar = []
  for (let i = 1; i <= n; i++) {
    if (i % 15 === 0) ar.push('FizzBuzz')
    else if (i % 3 === 0) ar.push('Fizz')
    else if (i % 5 === 0) ar.push('Buzz')
    else ar.push(i.toString())
  }

  return ar
}

module.exports = fizzBuzz
