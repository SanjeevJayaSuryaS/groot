const FizzBuzz = require('./FizzBuzz')

test('Standard Test Case', () => {
  expect(FizzBuzz(15)).toEqual([
    '1',
    '2',
    'Fizz',
    '4',
    'Buzz',
    'Fizz',
    '7',
    '8',
    'Fizz',
    'Buzz',
    '11',
    'Fizz',
    '13',
    '14',
    'FizzBuzz'
  ])
})

test('Should give an Error when 0 or negative number is passed', () => {
  expect(() => {
    FizzBuzz(0)
  }).toThrowError('Invalid Input')
})

test('Should give an Error when NaN is passed', () => {
  expect(() => {
    FizzBuzz('string')
  }).toThrowError('Invalid Input')
})
