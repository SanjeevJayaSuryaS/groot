const fs = require('fs/promises')

const {
  matchesPlayedPerYear,
  matchesWonPerTeamPerYear,
  extraRunsPerTeamIn2016
} = require('./ipl')

const writeAnswersToFile = async () => {
  let answer
  answer = await extraRunsPerTeamIn2016()
  await fs.writeFile(
    '../output/extraRunsPerTeamIn2016.json',
    JSON.stringify(answer, null, 2)
  )
  answer = await matchesWonPerTeamPerYear()
  await fs.writeFile(
    '../output/matchesWonPerTeamPerYear.json',
    JSON.stringify(answer, null, 2)
  )
  answer = await matchesPlayedPerYear()
  await fs.writeFile(
    '../output/matchesPlayedPerYear.json',
    JSON.stringify(answer, null, 2)
  )
}

module.exports = writeAnswersToFile
