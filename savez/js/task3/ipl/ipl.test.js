const matchesPlayed = require('../output/matchesPlayedPerYear.json')
const matchesWon = require('../output/matchesWonPerTeamPerYear.json')
const extraRuns2016 = require('../output/extraRunsPerTeamIn2016.json')

const {
  matchesPlayedPerYear,
  matchesWonPerTeamPerYear,
  extraRunsPerTeamIn2016
} = require('./ipl')

describe('tests for matchesPlayedPerYear,  matchesWonPerTeamPerYear and extraRunsPerTeamIn2016', () => {
  test('should match matchesPlayedPerYear', async () => {
    const data = await matchesPlayedPerYear()
    expect(data).toEqual(matchesPlayed)
  })

  test('should match matchesWonPerTeamPerYear', async () => {
    const data = await matchesWonPerTeamPerYear()
    expect(data).toEqual(matchesWon)
  })

  test('should match extraRunsPerTeamIn2016', async () => {
    const data = await extraRunsPerTeamIn2016()
    expect(data).toEqual(extraRuns2016)
  })
})
