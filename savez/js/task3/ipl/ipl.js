const csv = require('csvtojson')
const path = require('path')

const getData = (filename) => {
  return csv().fromFile(path.join(__dirname, `../data/${filename}.csv`))
}

const matchesPlayedPerYear = async () => {
  const matches = await getData('matches')
  return matches.reduce((acc, { season }) => {
    acc[season] = acc[season] || 1
    acc[season]++
    return acc
  }, {})
}

const matchesWonPerTeamPerYear = async () => {
  const matches = await getData('matches')
  return matches.reduce((acc, { season, winner }) => {
    if (winner === 'Delhi Capitals' || winner === 'Delhi Daredevils') {
      acc['Delhi Daredevils'] = acc['Delhi Daredevils'] || {}
      acc['Delhi Daredevils'][season] = acc['Delhi Daredevils'][season] || 1
      acc['Delhi Daredevils'][season]++
    } else {
      acc[winner] = acc[winner] || {}
      acc[winner][season] = acc[winner][season] || 1
      acc[winner][season]++
    }
    return acc
  }, {})
}

const extraRunsPerTeamIn2016 = async () => {
  const matches = await getData('matches')
  const deliveries = await getData('deliveries')
  const matches2016 = matches.filter(({ season }) => season === '2016')
  const matchIds = matches2016.map(({ id }) => id)
  // eslint-disable-next-line camelcase
  return deliveries.reduce((acc, { match_id, bowling_team, extra_runs }) => {
    if (matchIds.includes(match_id)) {
      acc[bowling_team] = acc[bowling_team] || 0
      acc[bowling_team] += parseInt(extra_runs)
    }
    return acc
  }, {})
}

module.exports = {
  matchesPlayedPerYear,
  matchesWonPerTeamPerYear,
  extraRunsPerTeamIn2016
}
