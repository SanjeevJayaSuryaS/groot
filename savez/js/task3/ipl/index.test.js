const writeAnswer = require('./index')
const fs = require('fs/promises')

beforeEach(() => jest.restoreAllMocks())

describe('tests for writeAnswer', () => {
  test('should generate file', async () => {
    const spy = jest
      .spyOn(fs, 'writeFile')
      .mockImplementation(() => Promise.resolve(true))
    await writeAnswer()
    expect(spy).toHaveBeenCalledTimes(3)
  })

  test('should throw', async () => {
    jest.spyOn(fs, 'writeFile').mockImplementation(async () => {
      throw new Error('Error')
    })
    await expect(writeAnswer()).rejects.toEqual(new Error('Error'))
  })
})
