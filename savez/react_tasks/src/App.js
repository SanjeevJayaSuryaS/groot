import React from 'react'
// import Home from './components/task2/Home/Home'
// import Board from './components/task2/Board/Board'
import Todo from './components/task1/Todo/Todo'
import { HashRouter, Switch, Route, Redirect } from 'react-router-dom'
import './App.css'
import Trello from './components/task2/Trello'

const App = () => {
  return (
    <HashRouter>
      <div className="content">
        <Switch>
          <Route exact path="/">
            <Redirect to="/boards" />
          </Route>
          <Route exact path="/Todo">
            <Todo />
          </Route>
          <Route exact path="/boards">
            <Trello />
          </Route>
          <Route path="/board">
            <Trello />
          </Route>
        </Switch>
      </div>
    </HashRouter>
  )
}

export default App
