import React, { useState } from 'react'
import './Sidebar.css'

const Sidebar = () => {
  const [showDropdown, toggleDropdown] = useState(true)
  return (
    <div className="sidebar d-sm">
      <div>
        <div className="sidebar__card">
          <i className="fa fa-trello p-1"></i>Boards
        </div>
        <div className="sidebar__card">
          <i className="fa fa-trello p-1"></i>Template
        </div>
        <div className="sidebar__card">
          <i className="fas fa-wave-square p-1"></i>Home
        </div>
      </div>
      <div className="sidebar__teams">
        <div>TEAMS</div>
        <div className="font--lg">+</div>
      </div>
      <div className="sidebar__workspace">
        <div className="flex__row">
          <i className="fas fa-user-friends p-1"></i>
          <div>
            Savez Siddiqui&apos;s <br />
            Workspace
          </div>
        </div>
        <i
          onClick={() => toggleDropdown(!showDropdown)}
          data-testid="dropdown"
          className={
            showDropdown ? 'fas fa-chevron-up p-1' : 'fas fa-chevron-down p-1'
          }
        ></i>
      </div>
      {showDropdown && (
        <div className="sidebar__bottom">
          <div className="sidebar__card">
            <i className="far fa-check-square p-1"></i>Getting started
          </div>
          <div className="sidebar__card">
            <i className="fa fa-trello p-1"></i>Boards
          </div>
          <div className="sidebar__card">
            <i className="far fa-heart p-1"></i>Highlights
          </div>
          <div className="sidebar__card">
            <i className="fas fa-table p-1"></i>Team Table
          </div>
          <div className="sidebar__card">
            <i className="fas fa-user-friends p-1"></i>Members
          </div>
          <div className="sidebar__card">
            <i className="fas fa-cog p-1"></i>Settings
          </div>
        </div>
      )}
    </div>
  )
}

export default Sidebar
