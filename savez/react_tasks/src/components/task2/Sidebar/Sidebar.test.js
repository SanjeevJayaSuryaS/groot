import React from 'react'
import Sidebar from './Sidebar'
import {
  fireEvent,
  getByTestId,
  getByText,
  render
} from '@testing-library/react'

describe('test for Sidebar', () => {
  test('should toggle sidebar', () => {
    const container = render(<Sidebar />).container
    const dropdown = getByTestId(container, 'dropdown')
    fireEvent.click(dropdown)
    expect(() => {
      getByText(container, 'Highlights')
    }).toThrow()
  })
})
