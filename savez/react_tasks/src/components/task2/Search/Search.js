import React from 'react'
import './Search.css'

const Search = ({ filterBoards }) => {
  return (
    <input
      type="text"
      className="search-box"
      placeholder="filter boards"
      onChange={(e) => filterBoards(e.target.value)}
    />
  )
}

export default Search
