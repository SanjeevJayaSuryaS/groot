import React, { useState, useEffect } from 'react'
import { token, key } from '../config/config.json'
import { Link } from 'react-router-dom'
import Sidebar from '../Sidebar/Sidebar'
import axios from 'axios'
import Search from '../Search/Search'
import './Home.css'

const Home = () => {
  const [boards, setBoards] = useState(null)
  const [boardUi, toggleBoardUi] = useState(false)
  const [newBoardName, setNewBoardName] = useState('')
  const [filteredBoards, setFilteredBoards] = useState(boards)
  const [isLoading, setLoading] = useState(true)

  const getBoards = async () => {
    try {
      const resp = await axios.get(
        `https://api.trello.com/1/members/me/boards?key=${key}&token=${token}`
      )
      setBoards(resp.data)
      setFilteredBoards(resp.data)
      setLoading(false)
    } catch (err) {
      console.log(err.message)
      setLoading(false)
    }
  }

  const filterBoards = (text) => {
    const newFilteredBoards = boards.filter(({ name }) =>
      name.toLowerCase().includes(text.toLowerCase())
    )
    setFilteredBoards(newFilteredBoards)
  }

  useEffect(() => {
    getBoards()
  }, [])

  const addBoard = async (e) => {
    try {
      setLoading(true)
      e.preventDefault()
      await axios.post(
        `https://api.trello.com/1/boards/?key=${key}&token=${token}&name=${newBoardName}`
      )
      setNewBoardName('')
      toggleBoardUi(!boardUi)
      getBoards()
    } catch (error) {
      console.log(error.message)
      setLoading(false)
    }
  }

  const deleteBoard = async (name, id) => {
    const agree = confirm(`board ${name} will be deleted. Are you Sure ?`)
    if (agree) {
      try {
        setLoading(true)
        await axios.delete(
          `https://api.trello.com/1/boards/${id}?key=${key}&token=${token}`
        )
        getBoards()
      } catch (error) {
        console.log(error.message)
        setLoading(false)
      }
    }
  }

  if (isLoading) {
    return (
      <div className="loading">
        <div className="spinner"></div>
      </div>
    )
  } else {
    return (
      <div className="root__home">
        <div className="flex__row--center">
          <Search filterBoards={filterBoards} />
        </div>
        <div className="flex__row">
          <Sidebar />
          <div className="board__container">
            {filteredBoards &&
              filteredBoards.map(({ name, id, prefs: { backgroundImage } }) => (
                <div
                  className="board"
                  style={{
                    backgroundImage: `url(${backgroundImage})`
                  }}
                  key={id}
                >
                  <Link to={`/board/${id}`} className="board__link">
                    <p className="text">
                      {name.length <= 32 ? name : name.slice(0, 32) + '...'}
                    </p>
                  </Link>
                  <div>
                    <button
                      className="close-button"
                      data-testid="delete-board-button"
                      onClick={() => deleteBoard(name, id)}
                    >
                      <i className="far fa-trash-alt"></i>
                    </button>
                  </div>
                </div>
              ))}

            {boardUi ? (
              <form className="add-board__card " onSubmit={(e) => addBoard(e)}>
                <input
                  type="text"
                  data-testid="add-board-input"
                  className="custom-input"
                  value={newBoardName}
                  onChange={(e) => setNewBoardName(e.target.value)}
                />
                <div>
                  <button type="submit" className="add-card__button">
                    Add Board
                  </button>
                  <button
                    className="cancel"
                    onClick={() => toggleBoardUi(!boardUi)}
                  >
                    Cancel
                  </button>
                </div>
              </form>
            ) : (
              <div className="board">
                <p className="text" onClick={() => toggleBoardUi(!boardUi)}>
                  + add board
                </p>
              </div>
            )}
          </div>
        </div>
      </div>
    )
  }
}

export default Home
