import React from 'react'
import { Link } from 'react-router-dom'
import './Header.css'

const Header = () => {
  return (
    <div className="header__root">
      <div className="header">
        <div>
          <div className="header__container">
            <div className="header__icons">
              <i className="fas fa-th"></i>
            </div>
            <div className="header__icons">
              <i className="fas fa-home"></i>
            </div>
            <Link to={'../boards'} className="header__icons d-sm">
              <i className="fa fa-trello"></i>
              <p className="header__p">Boards</p>
            </Link>
          </div>
        </div>
        <div className="header__logo">
          <i className="fa fa-trello"></i>
          <p className="header__p">Trello</p>
        </div>
        <div className="header__container">
          <div className="header__icons">
            <i className="fas fa-plus"></i>
          </div>
          <div className="header__icons d-sm">
            <i className="fas fa-exclamation-circle"></i>
          </div>
          <div className="header__icons red">
            <i className="far fa-bell"></i>
          </div>
          <div className="header__icons--circle">SS</div>
        </div>
      </div>
    </div>
  )
}

export default Header
