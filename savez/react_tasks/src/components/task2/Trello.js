import React from 'react'
import Home from './Home/Home'
import Board from './Board/Board'
import Header from './Header/Header'
import { Switch, Route } from 'react-router-dom'
import './Trello.css'

const Trello = () => {
  return (
    <div className="root">
      <Header />
      <Switch>
        <Route exact path="/board/:boardId">
          <Board />
        </Route>
        <Route>
          <Home />
        </Route>
      </Switch>
    </div>
  )
}

export default Trello
