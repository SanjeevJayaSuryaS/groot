function acmTeam(teams, students, topic) {
  if (
    teams < 2 ||
    students < 2 ||
    !Array.isArray(topic) ||
    isNaN(teams) ||
    isNaN(students)
  ) {
    throw new Error('Invalid input')
  }

  let mx = 0
  let count = 0

  for (let i = 0; i < topic.length; i++) {
    for (let j = i + 1; j < topic.length; j++) {
      let CurMax = 0

      for (let k = 0; k < topic[0].length; k++) {
        if (Number(topic[i][k]) === 1 || Number(topic[j][k]) === 1) CurMax++
      }
      if (CurMax > mx) {
        mx = CurMax
        count = 1
      } else if (CurMax === mx) {
        count++
      }
    }
  }

  return [mx, count]
}

module.exports = acmTeam
