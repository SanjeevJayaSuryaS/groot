const acmTeam = require('./acm')

describe('acm', () => {
  test('should return [2,1]', () => {
    const result = acmTeam(2, 3, ['110', '100'])
    const expectedResult = [2, 1]
    expect(result).toEqual(expectedResult)
  })

  test('should return [3,4]', () => {
    const result = acmTeam(4, 3, ['110', '100', '111', '101'])
    const expectedResult = [3, 4]
    expect(result).toEqual(expectedResult)
  })

  test('should throw error when array is not an array', () => {
    expect(() => acmTeam(3, 3, undefined)).toThrow('Invalid input')
  })

  test('should throw when no of teams are less than 2', () => {
    const expectedResult = new Error('Invalid input')
    expect(() => acmTeam(1, 4, ['1010'])).toThrow(expectedResult)
  })

  test('should throw when no. of students are less than 2', () => {
    const expectedResult = new Error('Invalid input')
    expect(() => acmTeam(3, 1, ['1', '0', '1'])).toThrow(expectedResult)
  })

  test('should throw when no. of students are not a number', () => {
    const expectedResult = new Error('Invalid input')
    expect(() => acmTeam(2, 's', ['110', '100'])).toThrow(expectedResult)
  })

  test('should throw when no. of teams are not a number', () => {
    const expectedResult = new Error('Invalid input')
    expect(() => acmTeam('s', 3, ['110', '100'])).toThrow(expectedResult)
  })
})
