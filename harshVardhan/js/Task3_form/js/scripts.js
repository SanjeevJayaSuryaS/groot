/* istanbul ignore file */

function verifyLoginEmail() {
  const email = document.login.email.value
  const regEmail = /\S+@\S+\.\S+/

  if (email.match(regEmail)) {
    document.getElementById('email_message').innerHTML = ''
    return true
  } else {
    document.getElementById('email_message').innerHTML =
      '**Fill the email please!'
    return false
  }
}
function verifyLoginPassword() {
  const password = document.login.password.value

  if (password.length < 8) {
    document.getElementById('password_message').innerHTML =
      '**password must be 8 characters long'
    return false
  } else {
    document.getElementById('password_message').innerHTML = ''
    return true
  }
}

function verifyRegisterEmail() {
  const email = document.register.email.value
  const regEmail = /\S+@\S+\.\S+/

  if (email.match(regEmail)) {
    document.getElementById('email_message').innerHTML = ''
    return true
  } else {
    document.getElementById('email_message').innerHTML =
      '**Fill the email please!'
    return false
  }
}

function verifyRegisterName() {
  const name = document.register.name.value
  const regName = /^[a-zA-Z]+ [a-zA-Z]+$/
  if (name.match(regName)) {
    document.getElementById('name_message').innerHTML = ''
    return true
  } else {
    document.getElementById('name_message').innerHTML =
      '**Fill the Full Name please!'
    return false
  }
}
function verifyRegisterPassword() {
  const password = document.register.password.value
  if (password.length < 8) {
    document.getElementById('password_message').innerHTML =
      '**password must be 8 characters long'
    return false
  } else {
    document.getElementById('password_message').innerHTML = ''
    return true
  }
}

function confirmPassword() {
  const password = document.register.password.value
  const confirm = document.register.confirm_password.value

  if (password === confirm) {
    document.getElementById('confirm_message').innerHTML = ''
    return true
  } else {
    document.getElementById('confirm_message').innerHTML =
      '**Please input same password as above!'
    return false
  }
}

// ValidateAllLogin is not called in this file so to remove eslint error eslint-disable-next-line line is added
// eslint-disable-next-line no-unused-vars
function validateAllLogin() {
  if (verifyLoginPassword() && verifyLoginEmail()) {
    alert('Login Successful')
  } else {
    alert('Invalid Inputs')
  }
}

// ValidateAllRegister is not called in this file so to remove eslint error eslint-disable-next-line line is added
// eslint-disable-next-line no-unused-vars
function validateAllRegister() {
  if (
    verifyRegisterName() &&
    verifyRegisterPassword() &&
    verifyRegisterEmail() &&
    confirmPassword()
  ) {
    alert('Registration Successful')
  } else {
    alert('Invalid Inputs')
  }
}
