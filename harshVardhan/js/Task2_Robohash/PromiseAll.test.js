const saveAllImages = require('./PromiseAll')

describe('Promise all testing', () => {
  test('should fetch and save all 5 images', async () => {
    const expectedResult = await saveAllImages()
    expect(expectedResult).toBe(true)
  })
})
