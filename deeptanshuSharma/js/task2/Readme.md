**JavaScript Task 2**

API End point: https://dog.ceo/dog-api/documentation/breed

1. read the breed name from the .txt file eg: retriever.
2. use Fetch or Axios to call your api and get the response.
3. Get the image from the response and store it in a file.
4. Write the unit tests for the task.

Using async, await, Promise.all, map etc.
