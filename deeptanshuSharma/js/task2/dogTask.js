const fs = require('fs')
const axios = require('axios')

const readFilePromise = (fileName) => {
  return new Promise(function (resolve, reject) {
    fs.readFile(fileName, function (error, data) {
      error
        ? reject(new Error('Error in reading file'))
        : resolve(data.toString()) // returns dogName
    })
  })
}

const writeFilePromise = (dogImage) => {
  return new Promise(function (resolve, reject) {
    fs.writeFile('dogImgUrl.txt', dogImage, function (er) {
      er ? reject(new Error('Write file error')) : resolve('Saved!')
    })
  })
}

const dogsFetch = async (fileName) => {
  const dogName = await readFilePromise(fileName)
  const link = `https://dog.ceo/api/breed/${dogName}/images/random`

  const response = await Promise.all([
    axios.get(link),
    axios.get(link),
    axios.get(link),
    axios.get(link),
    axios.get(link)
  ])
  const imageUrls = response.map((ele) => ele.data.message)
  const imgUrlString = imageUrls.join('\n')
  return writeFilePromise(imgUrlString)
}

module.exports = dogsFetch
