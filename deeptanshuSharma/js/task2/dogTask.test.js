const dogsFetch = require('./dogTask.js')
const axios = require('axios')
const fs = require('fs')

jest.mock('axios')
jest.mock('fs')

describe('Task 2 tests', () => {
  beforeEach(() => {
    fs.readFile.mockImplementation((fileName, callBack) =>
      callBack(undefined, 'retriever')
    )
    fs.writeFile.mockImplementation((fileName, text, callBack) =>
      callBack(undefined)
    )

    const resp = {
      data: {
        message: [
          'https://images.dog.ceo/breeds/retriever-chesapeake/n02099849_1068.jpg'
        ]
      }
    }

    axios.get.mockResolvedValue(resp)
  })

  test('should save the image url received from the API successfully', async () => {
    const successMsg = await dogsFetch('breed.txt')

    expect(successMsg).toBe('Saved!')
  })

  test('should write the data to the file successfully', async () => {
    const successMsg = await dogsFetch('breed.txt')

    expect(successMsg.toString()).toBe('Saved!')
  })

  test('Should return Correct response', async () => {
    const data = await dogsFetch('breed.txt')

    expect(data).toBe('Saved!')
  })

  test('Should return an error showing the API is Down', async () => {
    axios.get.mockImplementationOnce(() =>
      Promise.reject(new Error('API down'))
    )

    await expect(dogsFetch('breed.txt')).rejects.toEqual(new Error('API down'))
  })

  test('should return an error if the specified file does not exist', async () => {
    fs.readFile.mockImplementationOnce((fileName, callBack) =>
      callBack(new Error('Error in reading file'))
    )

    await expect(dogsFetch('breed.txt')).rejects.toEqual(
      new Error('Error in reading file')
    )
  })

  test('should return an error if cannot write to a file successfully', async () => {
    fs.writeFile.mockImplementationOnce((fileName, text, callBack) =>
      callBack(new Error('Write file error'))
    )

    await expect(dogsFetch('breed.txt')).rejects.toEqual(
      new Error('Write file error')
    )
  })
})
