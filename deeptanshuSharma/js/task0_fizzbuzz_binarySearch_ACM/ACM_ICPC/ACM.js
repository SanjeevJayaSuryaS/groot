function acm(arr) {
  if (arr.length < 2) {
    throw new Error('at least 2 teams required in input')
  }

  if (!Array.isArray(arr)) {
    throw new Error('not an array')
  }

  let max = 0
  const totalSubArr = []

  for (let team1 = 0; team1 < arr.length; team1++) {
    for (let team2 = team1 + 1; team2 < arr.length; team2++) {
      let totalSub = 0

      for (let topicIndex = 0; topicIndex < arr[team1].length; topicIndex++) {
        totalSub +=
          parseInt(arr[team1][topicIndex]) | parseInt(arr[team2][topicIndex])
        totalSubArr.push(totalSub)
      }

      if (totalSub > max) {
        max = totalSub
      }
    }
  }

  let teamCount = 0
  totalSubArr.forEach((Element) => {
    if (Element === max) {
      teamCount++
    }
  })

  return [max, teamCount]
}

module.exports = acm
