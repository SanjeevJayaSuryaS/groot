function fizzBuzz(n) {
  const arr = []
  const isInt = Number.isInteger(n)
  if (n <= 0 || !isInt) throw new Error('Invalid Input')
  for (let numIndex = 1; numIndex <= n; numIndex++) {
    if (numIndex % 5 === 0 && numIndex % 3 === 0) {
      arr.push('FizzBuzz')
    } else if (numIndex % 5 === 0) {
      arr.push('Buzz')
    } else if (numIndex % 3 === 0) {
      arr.push('Fizz')
    } else {
      arr.push(numIndex.toString(10))
    }
  }
  return arr
}

module.exports = fizzBuzz
