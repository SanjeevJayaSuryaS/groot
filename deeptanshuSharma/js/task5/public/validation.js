/*
  istanbul ignore file
*/

/* eslint-disable */

function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(String(email).toLowerCase())
}

function validate(event) {
  const fname = document.forms['form']['fname']
  const email = document.forms['form']['email']
  const lname = document.forms['form']['lname']
  const pass = document.forms['form']['pass']

  if (pass.value.length < 8) {
    pass.style.borderColor = 'red'
    pass.style.background = 'url(Group.svg) no-repeat scroll'
    pass.style.backgroundPosition = 'right 1em bottom 1em'
    pass.style.paddingRight = '3em'

    let text
    pass.value === ''
      ? (text = 'password cannot be empty')
      : (text = 'password must be atleast 8 characters')

    document.getElementById('pass-error').innerHTML = text
    pass.focus()
    event.preventDefault()
  }

  if (!validateEmail(email.value)) {
    email.style.borderColor = 'red'
    email.style.background = 'url(Group.svg) no-repeat scroll'
    email.style.backgroundPosition = 'right 1em bottom 1em'
    email.style.paddingRight = '3em'

    let text
    email.value === ''
      ? (text = 'email cannot be empty')
      : (text = 'not a valid email address')

    document.getElementById('email-error').innerHTML = text
    email.focus()
    event.preventDefault()
  }

  if (lname.value === '') {
    lname.style.borderColor = 'red'
    lname.style.background = 'url(Group.svg) no-repeat scroll'
    lname.style.backgroundPosition = 'right 1em bottom 1em'
    lname.style.paddingRight = '3em'

    document.getElementById('lname-error').innerHTML =
      'Last name cannot be empty'
    lname.focus()
    event.preventDefault()
  }

  if (fname.value === '') {
    fname.style.borderColor = 'red'
    fname.style.background = 'url(Group.svg) no-repeat scroll'
    fname.style.backgroundPosition = 'right 1em bottom 1em'
    fname.style.paddingRight = '3em'

    document.getElementById('fname-error').innerHTML =
      'First name cannot be empty'
    fname.focus()
    event.preventDefault()
  }

  return true
}
