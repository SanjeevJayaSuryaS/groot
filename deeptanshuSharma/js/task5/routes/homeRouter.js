/*
  istanbul ignore file
*/

const express = require('express')
const router = express.Router()

router.get('/', function (req, res, next) {
  res.render('login.ejs')
})

router.get('/dashboard', function (req, res, next) {
  res.render('dashboard.ejs')
})

module.exports = router
