/*
  istanbul ignore file
*/

const express = require('express')
const homeRouter = require('./routes/homeRouter')
const path = require('path')

const port = 5000

const app = express()
app.use(express.static(path.join(__dirname, 'public')))
app.set('views', path.join(__dirname, './views'))
app.set('view engine', 'ejs')

app.use(express.json())
app.use('/', homeRouter)
app.use(express.urlencoded({ extended: false }))

module.exports = app

// Listen on Port 5000
app.listen(port, () => console.info(`App listening on port ${port}`))
