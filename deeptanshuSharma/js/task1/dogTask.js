const fs = require('fs')
const axios = require('axios')

const readFilePromise = (fileName) => {
  return new Promise(function (resolve, reject) {
    fs.readFile(fileName, function (error, data) {
      if (error) {
        return reject(new Error('Error in reading file'))
      } else {
        const dogName = data.toString()
        resolve(dogName) // returns dogName
      }
    })
  })
}

const writeFilePromise = (dogImage) => {
  return new Promise(function (resolve, reject) {
    fs.writeFile('dogImgUrl.txt', dogImage, function (er) {
      if (er) {
        return reject(new Error('Write file error'))
      } else {
        resolve('Saved!')
      }
    })
  })
}

const fetchDog = (fileName) => {
  return new Promise(function (resolve, reject) {
    readFilePromise(fileName)
      .then((dogName) =>
        axios.get(`https://dog.ceo/api/breed/${dogName}/images`)
      )
      .then((response) => {
        return response.data.message[0]
      })
      .then((dogImageUrl) => {
        return writeFilePromise(dogImageUrl)
      })
      .then(() => {
        resolve('Saved!')
      })
      .catch((error) => {
        reject(error)
      })
  })
}

module.exports = fetchDog
