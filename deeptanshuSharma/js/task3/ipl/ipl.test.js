const {
  getTotalMatchesPerYear,
  getMatchesWonPerTeamPerYear,
  getExtraRunsPerTeam
} = require('./ipl.js')

const { getDeliveriesAsJson } = require('./index')

describe('Testing all functions for the 3 subtasks', () => {
  test('should return correct output for task 1', () => {
    const input = require('./testInput/matchesTest.json')

    const output = require('./expectedOutput/totalMatchesOutput.json')

    expect(getTotalMatchesPerYear(input)).toEqual(output)
  })

  test('should return correct output for task 2', () => {
    const input = require('./testInput/matchesTest.json')

    const output = require('./expectedOutput/matchPerTeamPerYearOutput.json')

    expect(getMatchesWonPerTeamPerYear(input)).toEqual(output)
  })

  test('should return correct output for task 3', async () => {
    const inputMatches = require('../data/matches.json')

    const output = require('./expectedOutput/extraRunsOutput.json')

    const deliveries = await getDeliveriesAsJson()

    const input = getExtraRunsPerTeam(inputMatches, deliveries)

    expect(input).toEqual(output)
  })
})
