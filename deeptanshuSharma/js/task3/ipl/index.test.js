const fs = require('fs/promises')
jest.mock('fs/promises')

const { matchesWonPerTeamPerYearPromise } = require('./index')

describe('Testing all functions for the 3 subtasks', () => {
  beforeEach(() => {
    fs.writeFile.mockImplementation((fileName, text, callBack) =>
      callBack(undefined)
    )
  })
  test('Should throw an error when cannot write to file', async () => {
    fs.writeFile.mockImplementationOnce((fileName, text) => {
      throw new Error('Error writing data')
    })

    const input = await matchesWonPerTeamPerYearPromise()
    return await expect(input).toEqual(new Error('Error writing data'))
  })
})
