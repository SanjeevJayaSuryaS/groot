// Utility Functions For Subtasks

/*
    SubTask 1 utility function for getting total matches per Year
*/
const getTotalMatchesPerYear = (data) => {
  const totalMatchesPerYear = {}

  const allMatchesYear = data.map((match) => match.season)

  allMatchesYear.map((matchYear) => {
    totalMatchesPerYear[matchYear]
      ? (totalMatchesPerYear[matchYear] += 1)
      : (totalMatchesPerYear[matchYear] = 1)
    return totalMatchesPerYear
  })

  return totalMatchesPerYear
}

/*
    SubTask 2 utility function for getting matches Won per Year per Team
*/
const getMatchesWonPerTeamPerYear = (data) => {
  const allYearsObj = getTotalMatchesPerYear(data)
  const matchesPerTeamPerYear = {}

  Object.keys(allYearsObj).map((year) => {
    matchesPerTeamPerYear[year] = {}
    return matchesPerTeamPerYear
  })

  data.map((match) => {
    let winner = match.winner.split(' ')

    winner[0] === 'Delhi' ? (winner = 'Delhi') : (winner = match.winner)

    if (match.winner !== '') {
      matchesPerTeamPerYear[match.season][winner]
        ? (matchesPerTeamPerYear[match.season][winner] += 1)
        : (matchesPerTeamPerYear[match.season][winner] = 1)
    }

    return matchesPerTeamPerYear
  })

  return matchesPerTeamPerYear
}

/*
    SubTask 3 utility function for getting Extra Runs per Team
*/
const getExtraRunsPerTeam = (data, getDeliveriesAsJson) => {
  const deliveries = getDeliveriesAsJson

  const extraRunsPerTeam = {}

  const matches2016 = data
    .filter((match) => match.season === 2016)
    .map((match2016) => match2016.id)

  deliveries.map((delivery) => {
    if (delivery.match_id in matches2016) {
      extraRunsPerTeam[delivery.bowling_team]
        ? (extraRunsPerTeam[delivery.bowling_team] += parseInt(
            delivery.extra_runs
          ))
        : (extraRunsPerTeam[delivery.bowling_team] = parseInt(
            delivery.extra_runs
          ))
    }
    return extraRunsPerTeam
  })

  return extraRunsPerTeam
}

module.exports = {
  getTotalMatchesPerYear: getTotalMatchesPerYear,
  getMatchesWonPerTeamPerYear: getMatchesWonPerTeamPerYear,
  getExtraRunsPerTeam: getExtraRunsPerTeam
}
