const fs = require('fs/promises')
const path = require('path')
const csvtojson = require('csvtojson')
const data = require('../data/matches.json')
const {
  getTotalMatchesPerYear,
  getMatchesWonPerTeamPerYear,
  getExtraRunsPerTeam
} = require('./ipl.js')

const writeData = async (totalMatchesPerYear, fileName) => {
  try {
    await fs.writeFile(
      path.join(__dirname, fileName),
      JSON.stringify(totalMatchesPerYear, null, 2)
    )
    return 'file written'
  } catch {
    return new Error('Error writing data')
  }
}

/*
    subTask 1 Promise for writing data returned from utility function for task 1 to a JSON file
*/
const matchesPerYearPromise = async () => {
  const totalMatchesPerYear = getTotalMatchesPerYear(data)

  await writeData(totalMatchesPerYear, '../output/matchesPerYear.json')
}

/*
    subTask 2 Promise for writing data returned from utility function for task 2 to a JSON file
*/
const matchesWonPerTeamPerYearPromise = async () => {
  const matchesPerTeamPerYear = getMatchesWonPerTeamPerYear(data)

  const ret = await writeData(
    matchesPerTeamPerYear,
    '../output/matchesWonOfPerTeamPerYear.json'
  )

  return ret
}

// convert csv to json deliveries
const getDeliveriesAsJson = async () => {
  const deliveriesData = await csvtojson().fromFile(
    path.join(__dirname, '../data/deliveries.csv')
  )

  return deliveriesData
}

/*
    subTask 3 Promise for writing data returned from utility function for task 3 to a JSON file
*/
const extraRunsPerTeamPromise = async () => {
  const deliveriesData = await getDeliveriesAsJson()
  const extraRunsPerTeam = getExtraRunsPerTeam(data, deliveriesData)
  await writeData(extraRunsPerTeam, '../output/extraRunsIn2016.json')
}

matchesPerYearPromise()

matchesWonPerTeamPerYearPromise()

extraRunsPerTeamPromise()

module.exports = {
  matchesPerYearPromise,
  matchesWonPerTeamPerYearPromise,
  extraRunsPerTeamPromise,
  getDeliveriesAsJson
}
