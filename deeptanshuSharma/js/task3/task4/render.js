/*
  istanbul ignore file
*/

/* eslint-disable */

const renderTotalMatchesPerYear = async () => {
  const matchesData = await d3.json('../output/matchesPerYear.json')

  const color = '#1DB954'
  const height = 200
  const width = 350
  const margin = { top: 30, right: 0, bottom: 40, left: 40 }

  const data = Object.assign(
    Object.keys(matchesData).map((key) => {
      return {
        name: key,
        value: matchesData[key]
      }
    }),
    { y: '↑ Number of matches played' }
  )

  const x = d3
    .scaleBand()
    .domain(d3.range(data.length))
    .range([margin.left, width - margin.right])
    .padding(0.1)

  const y = d3
    .scaleLinear()
    .domain([0, d3.max(data, (d) => d.value)])
    .nice()
    .range([height - margin.bottom, margin.top])

  const xAxis = (g) =>
    g.attr('transform', `translate(0,${height - margin.bottom})`).call(
      d3
        .axisBottom(x)
        .tickFormat((i) => data[i].name)
        .tickSizeOuter(0)
    )

  const yAxis = (g) =>
    g
      .attr('transform', `translate(${margin.left},0)`)
      .call(d3.axisLeft(y).ticks(null, data.format))
      .call((g) => g.select('.domain').remove())
      .call((g) =>
        g
          .append('text')
          .attr('x', -margin.left)
          .attr('y', 10)
          .attr('fill', 'currentColor')
          .attr('text-anchor', 'start')
          .text(data.y)
      )

  const labelX = (g) =>
    g.append('text').attr('x', 150).attr('y', 200).text('Season')

  const svg = d3.create('svg').attr('viewBox', [0, 0, width, height])

  svg
    .append('g')
    .attr('fill', color)
    .selectAll('rect')
    .data(data)
    .join('rect')
    .attr('x', (d, i) => x(i))
    .attr('y', (d) => y(d.value))
    .attr('height', (d) => y(0) - y(d.value))
    .attr('width', x.bandwidth())

  svg.append('g').call(xAxis)

  svg.append('g').call(yAxis)

  svg.append('g').call(labelX)

  let element = document.getElementById('container')
  element.removeChild(element.childNodes[0])
  element.append(svg.node())
  let download = document.getElementById('download')
  download.setAttribute('href', '../output/matchesPerYear.json')

  document.getElementsByClassName('active')[0].classList.remove('active')
  document.getElementsByClassName('nav-item')[0].classList.add('active')

  return svg.node()
}

const renderMatchesPerTeamPerYear = async () => {
  const response = await fetch('../output/matchesWonOfPerTeamPerYear.json')
  const data = await response.json()

  const distinctTeams = {}
  Object.keys(data).map((year) => {
    Object.keys(data[year]).map((team) => {
      team in distinctTeams
        ? distinctTeams[team].push(data[year][team])
        : (distinctTeams[team] = [data[year][team]])
    })
  })

  const seriesData = []
  Object.entries(distinctTeams).map((teamData) => {
    seriesData.push({
      name: teamData[0],
      data: teamData[1]
    })
  })

  Highcharts.chart('container', {
    title: {
      text: 'Total number of matches win per Team per Year'
    },

    subtitle: {
      text: 'Source: Kaggle'
    },

    yAxis: {
      title: {
        text: 'Total Matches Win'
      }
    },

    xAxis: {
      accessibility: {
        rangeDescription: 'Range: 2008 to 2019'
      }
    },

    legend: {
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle'
    },

    plotOptions: {
      series: {
        label: {
          connectorAllowed: false
        },
        pointStart: Math.min(...Object.keys(data))
      }
    },

    series: seriesData,

    responsive: {
      rules: [
        {
          condition: {
            maxWidth: 500
          },
          chartOptions: {
            legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom'
            }
          }
        }
      ]
    }
  })
  let download = document.getElementById('download')
  download.setAttribute('href', '../output/matchesWonOfPerTeamPerYear.json')

  document.getElementsByClassName('active')[0].classList.remove('active')
  document.getElementsByClassName('nav-item')[1].classList.add('active')
}

const renderExtraRuns2016 = async () => {
  const matchesData = await d3.json('../output/extraRunsIn2016.json')

  const color = '#1D90C0'
  const height = 400
  const width = 700
  const margin = { top: 30, right: 70, bottom: 120, left: 40 }

  const data = Object.assign(
    Object.keys(matchesData).map((key) => {
      return {
        name: key,
        value: matchesData[key]
      }
    }),
    { y: '↑ Extra runs conceded by teams' }
  )

  const x = d3
    .scaleBand()
    .domain(d3.range(data.length))
    .range([margin.left, width - margin.right])
    .padding(0.1)

  const y = d3
    .scaleLinear()
    .domain([0, d3.max(data, (d) => d.value)])
    .nice()
    .range([height - margin.bottom, margin.top])

  const xAxis = (g) =>
    g.attr('transform', `translate(0,${height - margin.bottom})`).call(
      d3
        .axisBottom(x)
        .tickFormat((i) => data[i].name)
        .tickSizeOuter(0)
    )

  const yAxis = (g) =>
    g
      .attr('transform', `translate(${margin.left},0)`)
      .call(d3.axisLeft(y).ticks(null, data.format))
      .call((g) => g.select('.domain').remove())
      .call((g) =>
        g
          .append('text')
          .attr('x', -margin.left)
          .attr('y', 10)
          .attr('fill', 'currentColor')
          .attr('text-anchor', 'start')
          .text(data.y)
      )

  const labelX = (g) =>
    g.append('text').attr('x', 300).attr('y', 400).text('Teams')

  const svg = d3.create('svg').attr('viewBox', [0, 0, width, height])

  svg
    .append('g')
    .attr('fill', color)
    .selectAll('rect')
    .data(data)
    .join('rect')
    .attr('x', (d, i) => x(i))
    .attr('y', (d) => y(d.value))
    .attr('height', (d) => y(0) - y(d.value))
    .attr('width', x.bandwidth())

  svg
    .append('g')
    .call(xAxis)
    .selectAll('text')
    .attr('y', 0)
    .attr('x', 9)
    .attr('dy', '.35em')
    .attr('transform', 'rotate(45)')
    .style('text-anchor', 'start')

  svg.append('g').call(labelX)

  svg.append('g').call(yAxis)

  //document.getElementById("extra_runs").append(svg.node());
  let element = document.getElementById('container')
  element.removeChild(element.childNodes[0])
  element.append(svg.node())
  let download = document.getElementById('download')
  download.setAttribute('href', '../output/extraRunsIn2016.json')

  document.getElementsByClassName('active')[0].classList.remove('active')
  document.getElementsByClassName('nav-item')[2].classList.add('active')

  return svg.node()
}

document.addEventListener('DOMContentLoaded', function () {
  renderMatchesPerTeamPerYear()
})
