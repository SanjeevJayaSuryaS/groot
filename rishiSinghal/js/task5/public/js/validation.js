/* eslint-disable */
/* istanbul ignore file */
const firstName = document.getElementById('firstName')
const lastName = document.getElementById('lastName')
const email = document.getElementById('email')
const password = document.getElementById('password')
const claimBtn = document.getElementById('claimBtn')

const firstNameWarning = document.getElementById('firstName-warning')
const lastNameWarning = document.getElementById('lastName-warning')
const emailWarning = document.getElementById('email-warning')
const passwordWarning = document.getElementById('password-warning')

const firstNameWarningLogo = document.getElementById('firstName-warning-logo')
const lastNameWarningLogo = document.getElementById('lastName-warning-logo')
const emailWarningLogo = document.getElementById('email-warning-logo')
const passwordWarningLogo = document.getElementById('password-warning-logo')

claimBtn.addEventListener('click', (event) => {
  event.preventDefault()
  const inputsInfo = document.querySelectorAll('.inputs-info')
  firstName.value.trim() === ''
    ? ((firstNameWarning.innerText = "First name can't be blank"),
      (firstNameWarningLogo.style.visibility = 'visible'),
      (inputsInfo[0].style.borderColor = '#FF7979'))
    : ((firstNameWarning.innerText = ''),
      (firstNameWarningLogo.style.visibility = 'hidden'),
      (inputsInfo[0].style.borderColor = '#AC9595'))

  lastName.value.trim() === ''
    ? ((lastNameWarning.innerText = "Last name can't be blank"),
      (lastNameWarningLogo.style.visibility = 'visible'),
      (inputsInfo[1].style.borderColor = '#FF7979'))
    : ((lastNameWarning.innerText = ''),
      (lastNameWarningLogo.style.visibility = 'hidden'),
      (inputsInfo[1].style.borderColor = '#AC9595'))

  password.value.trim() === ''
    ? ((passwordWarning.innerText = "Password can't be empty"),
      (passwordWarningLogo.style.visibility = 'visible'),
      (inputsInfo[3].style.borderColor = '#FF7979'))
    : ((passwordWarning.innerText = ''),
      (passwordWarningLogo.style.visibility = 'hidden'),
      (inputsInfo[3].style.borderColor = '#AC9595'))

  const emailPattern = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/

  if (email.value.trim() === '') {
    emailWarning.innerText = 'Looks like this is not an Email'
    emailWarningLogo.style.visibility = 'visible'
    inputsInfo[2].style.borderColor = '#FF7979'
    inputsInfo[2].querySelector('input').style.color = 'red'
  } else if (!emailPattern.test(email.value)) {
    emailWarning.innerText = 'Email format is wrong'
    emailWarningLogo.style.visibility = 'visible'
    inputsInfo[2].style.borderColor = '#FF7979'
    inputsInfo[2].querySelector('input').style.color = 'red'
  } else {
    emailWarning.innerText = ''
    emailWarningLogo.style.visibility = 'hidden'
    inputsInfo[2].style.borderColor = '#AC9595'
    inputsInfo[2].querySelector('input').style.color = 'black'
  }
})
