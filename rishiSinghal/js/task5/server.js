const express = require('express')
const path = require('path')
const app = express()
const cookieParser = require('cookie-parser')

app.set('view engine', 'ejs')
app.set('views', path.join(__dirname, '/views'))

app.use(express.json())
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use('/public', express.static(path.join(__dirname, 'public')))

const PORT = process.env.PORT || 5000

const { OAuth2Client } = require('google-auth-library')

const CLIENT_ID =
  '597757022807-nbeu9c4lajvbgoo83qpat1g3apg9nmp3.apps.googleusercontent.com'
const client = new OAuth2Client(CLIENT_ID)

app.get('/', (req, res) => {
  res.render('index')
})

app.post('/', (req, res) => {
  const token = req.body.token
  console.log(token)

  async function verify() {
    const ticket = await client.verifyIdToken({
      idToken: token,
      audience: CLIENT_ID
    })
    const payload = ticket.getPayload()
    // const userid = payload['sub']
    console.log(payload)
  }
  verify()
    .then(() => {
      res.cookie('session-token', token)
      res.send('success')
    })
    .catch(console.error)
})

app.get('/dashboard', checkAuthenticated, (req, res) => {
  const user = req.user
  res.render('dashboard', { user })
})

app.get('/logout', checkAuthenticated, (req, res) => {
  res.clearCookie('session-token')
  res.redirect('/')
})

function checkAuthenticated(req, res, next) {
  const token = req.cookies['session-token']

  const user = {}
  async function verify() {
    const ticket = await client.verifyIdToken({
      idToken: token,
      audience: CLIENT_ID
    })
    const payload = ticket.getPayload()
    user.name = payload.name
    user.email = payload.email
    user.picture = payload.picture
  }
  verify()
    .then(() => {
      req.user = user
      next()
    })
    .catch(() => {
      res.redirect('/')
    })
}

app.listen(PORT, () => {
  console.log('server running on ' + PORT)
})
