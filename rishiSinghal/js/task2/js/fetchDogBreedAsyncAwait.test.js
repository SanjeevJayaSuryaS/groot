const axios = require('axios')
const {
  readDogBreeds,
  writeDogImagesUrl,
  storeImages
} = require('./fetchDogBreedAsyncAwait.js')

const fs = require('fs')
const path = require('path')

describe('StoreImages :', () => {
  test('axios should run exactly five times', async () => {
    const spy = jest.spyOn(axios, 'get')
    await storeImages('./dogBreeds.txt')
    expect(spy).toHaveBeenCalledTimes(5)
  })

  test('storeImages func should return an error on wrong readfile path', async () => {
    const text = await storeImages('sdj.txt')
    expect(text).toStrictEqual(new Error('File not found'))
  })
})

describe('WriteDogImagesUrl:', () => {
  test('writeDogImagesUrl func should throw error if data cannot be write in file', async () => {
    await expect(writeDogImagesUrl('', '')).rejects.toStrictEqual(
      new Error('Cannot write Dog image urls in given file')
    )
  })

  test('writeDogImagesUrl func should return mentioned response on writing in a file', async () => {
    const response = await writeDogImagesUrl(
      './dogBreedImagesUrl.txt',
      'https://someImageUrl.jpeg'
    )
    expect(response).toBe('Successfully all urls stored in file')
    fs.unlinkSync(path.join(__dirname, './dogBreedImagesUrl.txt'))
  })
})

test('readDogBreeds func should throw error if file not found', async () => {
  await expect(readDogBreeds('')).rejects.toStrictEqual(
    new Error('File not found')
  )
})
