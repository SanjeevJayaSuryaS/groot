const axios = require('axios')
const fs = require('fs')
const path = require('path')

const readDogBreeds = async (inputFilePath) => {
  return new Promise((resolve, reject) => {
    fs.readFile(path.join(__dirname, inputFilePath), 'utf-8', (err, breeds) => {
      if (err) {
        return reject(new Error('File not found'))
      } else {
        return resolve(breeds)
      }
    })
  })
}

const writeDogImagesUrl = (outputFilePath, listOfUrls) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(path.join(__dirname, outputFilePath), listOfUrls, (err) => {
      if (err) {
        return reject(new Error('Cannot write Dog image urls in given file'))
      } else {
        return resolve('Successfully all urls stored in file')
      }
    })
  })
}

const storeImages = async (inputFile) => {
  try {
    let dogBreeds = await readDogBreeds(inputFile)

    dogBreeds = dogBreeds.split('\n')

    const dogApiData = await Promise.all(
      dogBreeds.map((breed) => {
        return axios.get(`https://dog.ceo/api/breed/${breed}/images/random`)
      })
    )

    const dogImagesUrl = dogApiData.map((url) => url.data.message)
    await writeDogImagesUrl('./dogBreedImagesUrl.txt', dogImagesUrl.join('\n'))
  } catch (err) {
    return err
  }
}
module.exports = { readDogBreeds, writeDogImagesUrl, storeImages }
