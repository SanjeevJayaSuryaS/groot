const fizzBuzz = require('./problem1.js')

describe('fizzBuzz', () => {
  describe('test on invalid input', () => {
    test('Should give error on non numerical input is passed', () => {
      const result = () => {
        fizzBuzz(undefined)
      }
      expect(result).toThrow('Non numerical input is passed')
    })

    test('Should throw the error on passing the negative number', () => {
      const value = () => fizzBuzz(-1)
      expect(value).toThrow('Negative numbers not allowed')
    })
  })

  describe('test for valid inputs', () => {
    test.each([
      [15, 'FizzBuzz'],
      [12, 'Fizz'],
      [10, 'Buzz'],
      [2, 2]
    ])('given %p as arguments,return %s', (firstArg, expectedResult) => {
      const result = fizzBuzz(firstArg)
      expect(result).toEqual(expectedResult)
    })
  })
})
