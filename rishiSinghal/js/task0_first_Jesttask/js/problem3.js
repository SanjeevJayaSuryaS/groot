const commonTopics = (attendee1, attendee2) => {
  let commonTopics = 0
  for (let inx1 = 0; inx1 < attendee1.length; inx1++) {
    if (attendee1[inx1] !== attendee2[inx1]) {
      commonTopics += 1
    } else if (attendee1[inx1] === '1') {
      commonTopics += 1
    }
  }
  return commonTopics
}

const acmIcpcTeam = (topicsList, attendee, topics) => {
  if (attendee < 2 || attendee > 500) {
    throw new Error('attendee are not in range 2 to 500')
  }
  if (topics < 1 || topics > 500) {
    throw new Error('topics are not in range 1 to 500')
  }
  let maxTopics = 0
  let maxAttendee = 0
  for (let inx1 = 0; inx1 < attendee; inx1++) {
    for (let inx2 = inx1 + 1; inx2 < attendee; inx2++) {
      const commonTopic = commonTopics(topicsList[inx1], topicsList[inx2])

      if (commonTopic > maxTopics) {
        maxTopics = commonTopic
        maxAttendee = 1
      } else if (commonTopic === maxTopics) {
        maxAttendee += 1
      }
    }
  }
  return [maxTopics, maxAttendee]
}

module.exports = acmIcpcTeam
