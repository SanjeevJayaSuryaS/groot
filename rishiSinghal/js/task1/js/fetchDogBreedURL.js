const fs = require('fs')
const axios = require('axios')
const path = require('path')

const readDogBreed = () => {
  return new Promise((resolve, reject) => {
    fs.readFile(
      path.join(__dirname, './dogBreed.txt'),
      'utf-8',
      (err, breedName) => {
        return err ? reject(new Error('File Not Found')) : resolve(breedName)
      }
    )
  })
}
const writeDogBreedUrl = (filepath, dogBreedImageURL) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(path.join(__dirname, filepath), dogBreedImageURL, (err) => {
      return err
        ? reject(new Error('Cannot write Dog image url in given path'))
        : resolve('Successfully written in file')
    })
  })
}
const fetchDogApiUrl = (dogBreedApiURL) => {
  return new Promise((resolve, reject) => {
    axios
      .get(dogBreedApiURL)
      .then((dogBreedApiData) => {
        const message = dogBreedApiData.data.message

        resolve(message)
      })
      .catch((_error) => {
        reject(new Error('Could not fetch the image Url'))
      })
  })
}

const fetchStoreImageUrl = () => {
  return new Promise((resolve, reject) => {
    readDogBreed()
      .then((breedName) => {
        const dogBreedApiURL = `https://dog.ceo/api/breed/${breedName}/images/random`
        return fetchDogApiUrl(dogBreedApiURL)
      })
      .then((breedImageURL) => {
        return writeDogBreedUrl('./dogBreedImageURL.txt', breedImageURL)
      })
      .then(() => {
        return resolve(true)
      })
      .catch((err) => {
        return reject(err)
      })
  })
}

module.exports = {
  readDogBreed,
  writeDogBreedUrl,
  fetchDogApiUrl,
  fetchStoreImageUrl
}
