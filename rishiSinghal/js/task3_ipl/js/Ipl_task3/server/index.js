const fs = require('fs')
const path = require('path')
const csvToJson = require('csvtojson')
const {
  findPlayedMatchesPerYear,
  findWonMatchesPerTeamPerYear,
  findExtraRunsIn2016PerTeam
} = require('./ipl')

const writeData = (filepath, iplData) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(path.join(__dirname, filepath), iplData, (err) => {
      return err
        ? reject(new Error('Cannot write ipl data in given path'))
        : resolve('Successfully written in file')
    })
  })
}

const writeAllIplData = async (matchesCsvFilePath, deliveriesCsvFilePath) => {
  try {
    const matchesJsonData = await csvToJson().fromFile(
      path.join(__dirname, matchesCsvFilePath)
    )
    const deliveriesJsonData = await csvToJson().fromFile(
      path.join(__dirname, deliveriesCsvFilePath)
    )
    const matchesPerYear = await findPlayedMatchesPerYear(matchesJsonData)
    const wonMatchesPerTeamPerYear = findWonMatchesPerTeamPerYear(
      matchesJsonData
    )
    const deliveries2016 = findExtraRunsIn2016PerTeam(
      matchesJsonData,
      deliveriesJsonData
    )
    writeData(
      '../output/matchesPerYear.json',
      JSON.stringify(matchesPerYear, null, 4)
    )

    writeData(
      '../output/matchesWonOfPerTeamPerYear.json',
      JSON.stringify(wonMatchesPerTeamPerYear, null, 4)
    )

    writeData(
      '../output/extraRunIn2016.json',
      JSON.stringify(deliveries2016, null, 4)
    )
    return 'Successfully : All ipl data written'
  } catch (err) {
    return new Error('could not get the ipl data')
  }
}

module.exports = { writeAllIplData, writeData }
