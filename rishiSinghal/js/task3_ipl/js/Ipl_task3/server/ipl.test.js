const {
  findPlayedMatchesPerYear,
  findWonMatchesPerTeamPerYear,
  findExtraRunsIn2016PerTeam
} = require('./ipl')
const path = require('path')
const csvToJson = require('csvtojson')
const outputMatchesPerYear = require('../output/matchesPerYear.json')
const outputMatchesWonPerTeam = require('../output/matchesWonOfPerTeamPerYear.json')
const outputExtraRun2016 = require('../output/extraRunIn2016.json')

test('findPlayedMatchesPerYear:should give the same output', async () => {
  const matchesJsonData = await csvToJson().fromFile(
    path.join(__dirname, '../data/matches.csv')
  )
  const result = findPlayedMatchesPerYear(matchesJsonData)

  expect(result).toEqual(outputMatchesPerYear)
})

test('findWonMatchesPerTeamPerYear:should give the same output', async () => {
  const matchesJsonData = await csvToJson().fromFile(
    path.join(__dirname, '../data/matches.csv')
  )
  expect(findWonMatchesPerTeamPerYear(matchesJsonData)).toEqual(
    outputMatchesWonPerTeam
  )
})
test('findExtraRunsIn2016PerTeam:', async () => {
  const matchesJsonData = await csvToJson().fromFile(
    path.join(__dirname, '../data/matches.csv')
  )
  const deliveriesJsonData = await csvToJson().fromFile(
    path.join(__dirname, '../data/deliveries.csv')
  )
  expect(
    findExtraRunsIn2016PerTeam(matchesJsonData, deliveriesJsonData)
  ).toEqual(outputExtraRun2016)
})
