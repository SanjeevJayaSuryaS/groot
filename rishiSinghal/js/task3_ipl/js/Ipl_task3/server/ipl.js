/* eslint-disable array-callback-return */
const createYearWiseArray = (startYear, endYear) => {
  const yearMatchesPlayed = {}
  const length = endYear - startYear + 1
  Array.from({ length }, (_, i) => startYear + i).map((value) => {
    yearMatchesPlayed[value] = 0
  })
  return yearMatchesPlayed
}

const findPlayedMatchesPerYear = (
  matchesJsonData,
  startYear = 2008,
  endYear = 2019
) => {
  const matchesPerYear = createYearWiseArray(startYear, endYear)
  matchesJsonData.map((match) => {
    matchesPerYear[match.season] += 1
  })
  return matchesPerYear
}
const findWonMatchesPerTeamPerYear = (
  matchesJsonData,
  startYear = 2008,
  endYear = 2019
) => {
  const wonMatchesPerTeamPerYear = createYearWiseArray(startYear, endYear)
  matchesJsonData.map((match) => {
    const { season, winner } = match
    if (wonMatchesPerTeamPerYear[season] === 0) {
      const temp = {}
      temp[winner] = Number(1)
      wonMatchesPerTeamPerYear[season] = temp
    } else {
      if (wonMatchesPerTeamPerYear[season][winner] === undefined) {
        wonMatchesPerTeamPerYear[season][winner] = 1
      }
      wonMatchesPerTeamPerYear[season][winner] += 1
    }
  })

  return wonMatchesPerTeamPerYear
}
const findExtraRunsIn2016PerTeam = (matchesJsonData, deliveriesJsonData) => {
  const list2016MatchId = matchesJsonData
    .filter((match) => match.season === '2016')
    .map((match) => match.id)

  const checker = (matchId, lis) => {
    return lis.filter((item) => item === matchId).length
  }
  const deliveries2016 = deliveriesJsonData
    .filter((deliveries) => checker(deliveries.match_id, list2016MatchId))
    .map((delivery) => {
      return {
        bowling_team: delivery.bowling_team,
        extra_runs: delivery.extra_runs
      }
    })
    .reduce((acc, com) => {
      if (acc[com.bowling_team]) {
        acc[com.bowling_team] += Number(com.extra_runs)
      } else {
        acc[com.bowling_team] = Number(com.extra_runs)
      }
      return acc
    }, {})
  return deliveries2016
}
module.exports = {
  findPlayedMatchesPerYear,
  findWonMatchesPerTeamPerYear,
  findExtraRunsIn2016PerTeam
}
