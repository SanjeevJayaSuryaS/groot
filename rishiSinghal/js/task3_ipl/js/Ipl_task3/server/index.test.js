const { writeAllIplData, writeData } = require('./index')

const fs = require('fs')

describe('writeAllIplData:', () => {
  test('should return successful message on storing all ipl data', async () => {
    const result = await writeAllIplData(
      '../data/matches.csv',
      '../data/deliveries.csv'
    )
    expect(result).toEqual('Successfully : All ipl data written')
  })
  test('should throw an error on receiving wrong csv filepaths', async () => {
    const result = await writeAllIplData('./wrongfilepaths.csv', '')
    expect(result).toEqual(new Error('could not get the ipl data'))
  })
})

describe('WriteData:', () => {
  test('should throw an error on passing empty file path', () => {
    const emptyPathError = new Error('Cannot write ipl data in given path')
    jest.spyOn(fs, 'writeFile').mockImplementation((_, __, cb) => {
      cb(emptyPathError)
    })
    return writeData('', {}).catch((err) => {
      expect(err).toEqual(new Error('Cannot write ipl data in given path'))
    })
  })
  test('should successfully write in file', () => {
    jest.spyOn(fs, 'writeFile').mockImplementation((_, __, cb) => {
      cb()
    })
    return writeData('something.txt', {}).then((message) => {
      expect(message).toEqual('Successfully written in file')
    })
  })
})
