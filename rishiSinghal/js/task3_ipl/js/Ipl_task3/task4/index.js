/* eslint-disable */

let chartId = 1

const svg = document.querySelector('svg')
const container = document.querySelector('#container')

const onClickNavbar = (event, chartNumber) => {
  switch (chartNumber) {
    case 1:
      container.classList.add('hidden')
      svg.classList.remove('hidden')
      chartId = 1
      createUsingD3()
      break
    case 2:
      svg.classList.add('hidden')
      container.classList.remove('hidden')
      createUsingHighCharts()
      console.log('for chart 2')
      break
    case 3:
      container.classList.add('hidden')

      svg.classList.remove('hidden')
      chartId = 3
      createUsingD3()
      break
  }
  const allLiTags = document.querySelectorAll('li')
  allLiTags.forEach((li) => li.classList.remove('selected-task'))
  event.classList.add('selected-task')
}

const createUsingD3 = async (increaseHeight) => {
  d3.selectAll('rect').remove()
  d3.selectAll('text').remove()

  const downloadButton = document.querySelector('.download-button')

  d3.selectAll('g').remove()
  let iplData = []
  if (chartId == 1) {
    downloadButton.setAttribute('href', '../output/matchesPerYear.json')
    downloadButton.setAttribute('download', 'matchesPerYear.json')
    const matchesPerYear = await d3.json('../output/matchesPerYear.json')

    Object.keys(matchesPerYear).map((val) => {
      iplData.push({
        year: val,
        matches: matchesPerYear[val]
      })
    })
  } else {
    downloadButton.setAttribute('href', '../output/extraRunIn2016.json')
    downloadButton.setAttribute('download', 'extraRunIn2016.json')
    const extraRunIn2016 = await d3.json('../output/extraRunIn2016.json')

    Object.keys(extraRunIn2016).map((val) => {
      iplData.push({
        team: val,
        extraRun: extraRunIn2016[val]
      })
    })
  }

  let svg = document.querySelector('svg')
  const chart_margins = { top: 20, bottom: 50, right: 100 }
  const chart_width = svg.clientWidth
  const chart_height =
    svg.clientHeight -
    chart_margins.top -
    chart_margins.bottom -
    chart_margins.right

  const chart_container = d3.select('svg')
  let chartHeight =
    chart_height +
    chart_margins.top +
    chart_margins.bottom +
    chart_margins.right
  chart_container.attr('width', chart_width).attr('height', chartHeight)

  const xScale = d3
    .scaleBand()
    .domain(iplData.map((match) => (chartId === 1 ? match.year : match.team)))
    .rangeRound([0, chart_width])
    .padding(0.5)
  const yScale = d3
    .scaleLinear()
    .domain([
      0,
      Math.max(
        ...iplData.map((match) =>
          chartId === 1 ? match.matches : match.extraRun
        )
      ) + 10
    ])
    .range([chart_height, 0])

  const chart_bars = chart_container.append('g')

  const xLables = chart_bars
    .append('g')
    .classed('xLables', true)
    .call(d3.axisBottom(xScale).tickSizeOuter(0))
    // .attr('transform', 'rotate(-90)');
    .attr('transform', `translate(0,${chart_height})`)
    .selectAll('text')

    .attr('dx', '+0.1em')
    .attr('dy', '+1.8em')
    .attr('transform', 'rotate(-15)')

  chart_bars
    .append('g')
    .selectAll('.bar')
    .data(iplData)
    .enter()
    .append('rect')
    .classed('bar', true)
    .attr('width', xScale.bandwidth())
    .attr(
      'height',
      (data) =>
        chart_height - yScale(chartId === 1 ? data.matches : data.extraRun)
    )
    .attr('x', (data) => xScale(chartId === 1 ? data.year : data.team))
    .attr('y', (data) => yScale(chartId === 1 ? data.matches : data.extraRun))

  chart_bars
    .append('g')
    .selectAll('text')
    .data(iplData)
    .enter()
    .append('text')
    .text((data) => (chartId === 1 ? data.matches : data.extraRun))
    .attr(
      'x',
      (data) =>
        xScale(chartId === 1 ? data.year : data.team) + xScale.bandwidth() / 2
    )
    .attr(
      'y',
      (data) => yScale(chartId === 1 ? data.matches : data.extraRun) - 5
    )
    .attr('text-anchor', 'middle')
}

window.addEventListener('resize', createUsingD3)
createUsingD3(chartId)

const createUsingHighCharts = async () => {
  const matchesWonOfPerTeamPerYear = await d3.json(
    '../output/matchesWonOfPerTeamPerYear.json'
  )

  const downloadButton = document.querySelector('.download-button')

  downloadButton.setAttribute(
    'href',
    '../output/matchesWonOfPerTeamPerYear.json'
  )
  downloadButton.setAttribute('download', 'matchesWonOfPerTeamPerYear.json')

  const iplSeasons = Object.keys(matchesWonOfPerTeamPerYear)

  const teams = Object.values(matchesWonOfPerTeamPerYear)

  let getAllIplTeams = new Set()
  teams.map((year) => {
    const teams = Object.keys(year)

    teams.map((team) => getAllIplTeams.add(team))
  })
  getAllIplTeams.delete('')
  getAllIplTeams = [...getAllIplTeams]

  const teamsSeasonWiseMatches = Object.values(matchesWonOfPerTeamPerYear)

  const series = getAllIplTeams.map((team) => {
    const winMatches = teamsSeasonWiseMatches.map((season) => {
      if (season[team]) {
        return season[team]
      } else {
        return 0
      }
    })
    return {
      name: team,
      data: winMatches
    }
  })

  series.pop()
  Highcharts.chart('container', {
    chart: {
      type: 'line',
      backgroundColor: '#f2f2f2'
    },
    title: {
      text: 'Matches Won Per Team Per Year'
    },
    xAxis: {
      categories: iplSeasons,
      crosshair: true,
      title: {
        text: 'year'
      }
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Matches Won'
      }
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: series
  })
}
