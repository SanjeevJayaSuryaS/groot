/* istanbul ignore file */
/* eslint-disable */

const validateForm = (e) => {
  e.preventDefault()
  const formEl = document.forms.claimTrialForm
  const formData = new FormData(formEl)
  const firstNameErrorElement = document.querySelector('#firstNameError')
  const lastNameErrorElement = document.querySelector('#lastNameError')
  const emailErrorElement = document.querySelector('#emailError')
  const passwordErrorElement = document.querySelector('#passwordError')

  formData.get('firstName').length === 0
    ? (firstNameErrorElement.innerText = 'First Name cant be empty')
    : (firstNameErrorElement.innerText = '')

  formData.get('lastName').length === 0
    ? (lastNameErrorElement.innerText = 'Last Name cant be empty')
    : (lastNameErrorElement.innerText = '')

  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  if (formData.get('email').length === 0) {
    emailErrorElement.innerText = 'Email cant be empty'
  } else if (!re.test(formData.get('email').toLowerCase())) {
    emailErrorElement.innerText = 'Looks like this is not an email'
  } else {
    emailErrorElement.innerText = ''
  }

  formData.get('password').length === 0
    ? (passwordErrorElement.innerText = 'Password cant be empty')
    : (passwordErrorElement.innerText = '')
}
