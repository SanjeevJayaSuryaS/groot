/* istanbul ignore file */

const express = require('express')
const app = express()
const config = require('./config')
const path = require('path')

app.set('view engine', 'ejs')
app.use(express.static(path.join(__dirname, '/public')))
app.use(express.static(path.join(__dirname, '../ipl_task3')))
app.set('views', path.join(__dirname, '../../js'))

app.get('/', function (req, res) {
  res.render('task5/views/pages/login')
})

app.get('/dashboard', function (req, res) {
  // res.sendFile(path.join(__dirname, '../ipl_task3/task4/index.html'))
  res.render('ipl_task3/task4/index')
})

app.listen(config.port, () => {
  console.log(`${config.port} is the magic port`)
})
