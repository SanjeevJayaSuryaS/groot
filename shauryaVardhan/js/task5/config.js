/* istanbul ignore file */

const port = 3002
const baseURL = `http://localhost:${port}`
module.exports = {
  baseURL: baseURL,
  port: port
}
