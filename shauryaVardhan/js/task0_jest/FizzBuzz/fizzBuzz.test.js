const fizzBuzz = require('./fizzBuzz')

describe('fizzBuzz tests', () => {
  test('should throw error when input is not a number', () => {
    expect(() => fizzBuzz('qw')).toThrow(
      'Input is invalid, it must be a number'
    )
  })
  test('should throw error for empty string', () => {
    expect(() => fizzBuzz('')).toThrow('Input must be a number greater than 0')
  })
  test('should throw error when input is 0', () => {
    expect(() => fizzBuzz('0')).toThrow('Input must be a number greater than 0')
  })
  test('should throw error when input is a negative number', () => {
    expect(() => fizzBuzz('-7')).toThrow()
  })
  test('should return numbers as array of strings if numbers are not a multiple of 3 and 5', () => {
    expect(fizzBuzz(2)).toStrictEqual(['1', '2'])
  })
  test('should return Fizz for multiple of 3 and return the number itself if number is not a multiple of 3 and 5.', () => {
    expect(fizzBuzz(3)).toStrictEqual(['1', '2', 'Fizz'])
  })
  test('should return Buzz for multiple of 5 in array', () => {
    expect(fizzBuzz(5)).toStrictEqual(['1', '2', 'Fizz', '4', 'Buzz'])
  })
  test('should return Fizz if number is multiple of 3, Buzz if number is multiple of 5 and return the number itself if number is not a multiple of both 3 and 5.', () => {
    expect(fizzBuzz(12)).toStrictEqual([
      '1',
      '2',
      'Fizz',
      '4',
      'Buzz',
      'Fizz',
      '7',
      '8',
      'Fizz',
      'Buzz',
      '11',
      'Fizz'
    ])
  })
  test('should return Fizz if number is multiple of 3, Buzz if number is multiple of 5, FizzBuzz if number is multiple of 3 and 5 and return the number itself if number is not a multiple of both 3 and 5.', () => {
    expect(fizzBuzz(15)).toStrictEqual([
      '1',
      '2',
      'Fizz',
      '4',
      'Buzz',
      'Fizz',
      '7',
      '8',
      'Fizz',
      'Buzz',
      '11',
      'Fizz',
      '13',
      '14',
      'FizzBuzz'
    ])
  })
})
