function binarySearch(leftIndex, rightIndex, inputArray, searchInput) {
  if (isNaN(searchInput)) {
    throw new Error('invalid search input provided')
  }
  if (!Array.isArray(inputArray)) {
    throw new Error('invalid input array provided')
  }
  inputArray.forEach((ele, index) => {
    if (ele < inputArray[index - 1]) {
      throw new Error('Array must be sorted')
    }
  })
  while (leftIndex <= rightIndex) {
    const mid = leftIndex + Math.floor((rightIndex - leftIndex) / 2)
    if (inputArray[mid] === searchInput) {
      return mid
    } else if (inputArray[mid] < searchInput) {
      leftIndex = mid + 1
    } else {
      rightIndex = mid - 1
    }
  }
  return -1
}

module.exports = binarySearch
