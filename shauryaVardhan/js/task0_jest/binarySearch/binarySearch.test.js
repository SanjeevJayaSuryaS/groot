const binarySearch = require('./binarySearch')

const inputArray = [-5, -2, 0, 3, 5, 6, 7, 8, 9]
const invalidArray = { q: 1 }
const unsortedArray = [10, 2, 1, 5, 9]
const leftIndex = 0
const rightIndex = inputArray.length

describe('binarySearch tests', () => {
  test('should throw error for invalid search input', () => {
    expect(() => binarySearch(leftIndex, rightIndex, inputArray, 'qw')).toThrow(
      'invalid search input provided'
    )
  })
  test('should throw error for invalid input array', () => {
    expect(() => binarySearch(leftIndex, rightIndex, invalidArray, 1)).toThrow(
      'invalid input array provided'
    )
  })
  test('should throw error for input array being invalid', () => {
    expect(() => binarySearch(leftIndex, rightIndex, 2, 1)).toThrow(
      'invalid input array provided'
    )
  })
  test('should throw error for unsorted array', () => {
    expect(() => binarySearch(leftIndex, rightIndex, unsortedArray, 1)).toThrow(
      'Array must be sorted'
    )
  })
  test('should return -1 for elements not in array', () => {
    expect(binarySearch(leftIndex, rightIndex, inputArray, 10)).toBe(-1)
  })
  test('should return -1 for empty array', () => {
    expect(binarySearch(leftIndex, rightIndex, [], 10)).toBe(-1)
  })
  test('should return the correct index if element is present in the array', () => {
    expect(binarySearch(leftIndex, rightIndex, inputArray, 3)).toBe(3)
  })
  test('should work as expected for elements found right of mid in array', () => {
    expect(binarySearch(leftIndex, rightIndex, inputArray, 8)).toBe(7)
  })
  test('should return the correct index for searching negative elements', () => {
    expect(binarySearch(leftIndex, rightIndex, inputArray, -2)).toBe(1)
  })
})
