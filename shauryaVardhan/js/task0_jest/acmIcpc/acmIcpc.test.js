const acmIcpc = require('./acmIcpc')
const invalidArray = { q: 3 }

describe('acmIcpc tests', () => {
  test('should throw error for invalid array', () => {
    expect(() => acmIcpc(invalidArray)).toThrow('invalid input array')
  })
  test('should throw error for students less than 2', () => {
    expect(() => acmIcpc(['1100'])).toThrow('students should be greater than 1')
  })
  test('should throw error for subjects less than 2', () => {
    expect(() => acmIcpc(['1', '0', '0'])).toThrow(
      'subjects should be greater than 1'
    )
  })
  test('should work as expected for any valid input', () => {
    expect(acmIcpc(['11', '10', '00', '01'])).toEqual([2, 4])
  })
})
