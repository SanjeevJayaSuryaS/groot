const getDistinctYears = (inputArray) => {
  const arrayOfYears = inputArray.map((match) => match.season)
  const distinctYears = [...new Set(arrayOfYears)]
  return distinctYears
}

const getDistinctTeams = (inputArray) => {
  const arrayOfTeams1 = inputArray.map((match) => match.team1)
  const arrayOfTeams2 = inputArray.map((match) => match.team2)
  const totalTeams = [...arrayOfTeams1, ...arrayOfTeams2]
  const distinctTeams = [...new Set(totalTeams)]
  return distinctTeams
}

const totalMatchesPlayedPerYear = (inputArray) => {
  const distinctYears = getDistinctYears(inputArray)

  const totalMatches = distinctYears.map((year) => {
    const count = inputArray.filter((ele) => ele.season === year).length
    return {
      season: year,
      count
    }
  })
  return totalMatches
}

const matchesWonPerTeamPerYear = (inputArray) => {
  const distinctYears = getDistinctYears(inputArray)
  const distinctTeams = getDistinctTeams(inputArray)
  const response = distinctYears.map((year) => {
    return {
      year,
      data: distinctTeams.map((team) => {
        const matchWonCount = inputArray.filter(
          (match) => match.winner === team && match.season === year
        ).length
        return {
          team,
          matchWonCount
        }
      })
    }
  })
  return response
}

const getExtraRunsIn2016 = (matchesArray, deliveriesArray) => {
  const matchesIn2016 = matchesArray.filter((match) => match.season === '2016')
  const matchIdsFor2016 = matchesIn2016.map((match) => match.id)

  const deliveriesFor2016 = deliveriesArray.filter((delivery) =>
    matchIdsFor2016.includes(delivery.match_id)
  )

  const distinctTeams = getDistinctTeams(matchesArray)

  const deliveriesPerTeam = distinctTeams.map((team) => ({
    teamName: team,
    deliveries: deliveriesFor2016.filter(
      (delivery) => delivery.bowling_team === team
    )
  }))

  const extraRuns = deliveriesPerTeam
    .map((team) => {
      const { teamName } = team
      const runsConceded = team.deliveries.reduce(
        (accumulator, currentValue) =>
          accumulator + parseInt(currentValue.extra_runs),
        0
      )
      return {
        teamName,
        extraRuns: runsConceded
      }
    })
    .filter((teams) => teams.extraRuns > 0)
  return extraRuns
}

module.exports = {
  totalMatchesPlayedPerYear,
  matchesWonPerTeamPerYear,
  getExtraRunsIn2016,
  getDistinctYears,
  getDistinctTeams
}
