const csv = require('csvtojson')
const fs = require('fs').promises
const path = require('path')
const ipl = require('./ipl')

const writeFile = async (fileToWriteTo, dataToWrite) => {
  try {
    await fs.writeFile(
      path.join(__dirname, fileToWriteTo),
      JSON.stringify(dataToWrite)
    )
    return 'written successfully'
  } catch (err) {
    throw new Error('failed to write to file')
  }
}

const csvToJson = async (filePath) => {
  const csvFilePath = path.join(__dirname, filePath)
  const response = await csv().fromFile(csvFilePath)
  return response
}

const getData = async (filePathForMatches, filePathForDeliveries) => {
  try {
    const matchesInJson = await csvToJson(filePathForMatches)
    const deliveriesInJson = await csvToJson(filePathForDeliveries)
    writeFile(
      '../output/matchesPerYear.json',
      ipl.totalMatchesPlayedPerYear(matchesInJson)
    )
    writeFile(
      '../output/matchesWonPerTeamPerYear.json',
      ipl.matchesWonPerTeamPerYear(matchesInJson)
    )
    writeFile(
      '../output/extraRunsIn2016.json',
      ipl.getExtraRunsIn2016(matchesInJson, deliveriesInJson)
    )
    return 'executed successfully'
  } catch {
    throw new Error('failed to convert csv to JSON')
  }
}

module.exports = {
  writeFile,
  csvToJson,
  getData
}
