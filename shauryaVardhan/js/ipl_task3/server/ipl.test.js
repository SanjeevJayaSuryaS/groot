const {
  getDistinctTeams,
  getDistinctYears,
  totalMatchesPlayedPerYear,
  matchesWonPerTeamPerYear,
  getExtraRunsIn2016
} = require('./ipl')
const testDataMatches = require('../data/testDataMatches.json')
const testDataDeliveries = require('../data/testDataDeliveries.json')

const distinctTeams = [
  'Kolkata Knight Riders',
  'Royal Challengers Bangalore',
  'Gujarat Lions',
  'Chennai Super Kings',
  'Kings XI Punjab',
  'Mumbai Indians',
  'Sunrisers Hyderabad',
  'Rajasthan Royals',
  'Delhi Capitals'
]
const distinctYears = ['2008', '2016', '2018', '2019']

const responseOfTotalMatches = [
  { season: '2008', count: 1 },
  { season: '2016', count: 2 },
  { season: '2018', count: 1 },
  { season: '2019', count: 2 }
]

const responseOfMatchesWon = [
  {
    data: [
      {
        matchWonCount: 1,
        team: 'Kolkata Knight Riders'
      },
      {
        matchWonCount: 0,
        team: 'Royal Challengers Bangalore'
      },
      {
        matchWonCount: 0,
        team: 'Gujarat Lions'
      },
      {
        matchWonCount: 0,
        team: 'Chennai Super Kings'
      },
      {
        matchWonCount: 0,
        team: 'Kings XI Punjab'
      },
      {
        matchWonCount: 0,
        team: 'Mumbai Indians'
      },
      {
        matchWonCount: 0,
        team: 'Sunrisers Hyderabad'
      },
      {
        matchWonCount: 0,
        team: 'Rajasthan Royals'
      },
      {
        matchWonCount: 0,
        team: 'Delhi Capitals'
      }
    ],
    year: '2008'
  },
  {
    data: [
      {
        matchWonCount: 0,
        team: 'Kolkata Knight Riders'
      },
      {
        matchWonCount: 0,
        team: 'Royal Challengers Bangalore'
      },
      {
        matchWonCount: 0,
        team: 'Gujarat Lions'
      },
      {
        matchWonCount: 0,
        team: 'Chennai Super Kings'
      },
      {
        matchWonCount: 0,
        team: 'Kings XI Punjab'
      },
      {
        matchWonCount: 1,
        team: 'Mumbai Indians'
      },
      {
        matchWonCount: 1,
        team: 'Sunrisers Hyderabad'
      },
      {
        matchWonCount: 0,
        team: 'Rajasthan Royals'
      },
      {
        matchWonCount: 0,
        team: 'Delhi Capitals'
      }
    ],
    year: '2016'
  },
  {
    data: [
      {
        matchWonCount: 0,
        team: 'Kolkata Knight Riders'
      },
      {
        matchWonCount: 0,
        team: 'Royal Challengers Bangalore'
      },
      {
        matchWonCount: 0,
        team: 'Gujarat Lions'
      },
      {
        matchWonCount: 1,
        team: 'Chennai Super Kings'
      },
      {
        matchWonCount: 0,
        team: 'Kings XI Punjab'
      },
      {
        matchWonCount: 0,
        team: 'Mumbai Indians'
      },
      {
        matchWonCount: 0,
        team: 'Sunrisers Hyderabad'
      },
      {
        matchWonCount: 0,
        team: 'Rajasthan Royals'
      },
      {
        matchWonCount: 0,
        team: 'Delhi Capitals'
      }
    ],
    year: '2018'
  },
  {
    data: [
      {
        matchWonCount: 0,
        team: 'Kolkata Knight Riders'
      },
      {
        matchWonCount: 0,
        team: 'Royal Challengers Bangalore'
      },
      {
        matchWonCount: 0,
        team: 'Gujarat Lions'
      },
      {
        matchWonCount: 1,
        team: 'Chennai Super Kings'
      },
      {
        matchWonCount: 1,
        team: 'Kings XI Punjab'
      },
      {
        matchWonCount: 0,
        team: 'Mumbai Indians'
      },
      {
        matchWonCount: 0,
        team: 'Sunrisers Hyderabad'
      },
      {
        matchWonCount: 0,
        team: 'Rajasthan Royals'
      },
      {
        matchWonCount: 0,
        team: 'Delhi Capitals'
      }
    ],
    year: '2019'
  }
]

const responseOfExtraRuns = [{ teamName: 'Sunrisers Hyderabad', extraRuns: 2 }]

describe('ipl tests', () => {
  test('should return distinct teams as array', () => {
    expect(getDistinctTeams(testDataMatches)).toStrictEqual(distinctTeams)
  })
  test('should return distinct years as array', () => {
    expect(getDistinctYears(testDataMatches)).toStrictEqual(distinctYears)
  })
  test('should return total matches played per year', () => {
    expect(totalMatchesPlayedPerYear(testDataMatches)).toStrictEqual(
      responseOfTotalMatches
    )
  })
  test('should return matches won per year per team', () => {
    expect(matchesWonPerTeamPerYear(testDataMatches)).toStrictEqual(
      responseOfMatchesWon
    )
  })
  test('should return extra runs conceded per team in 2016', () => {
    expect(
      getExtraRunsIn2016(testDataMatches, testDataDeliveries)
    ).toStrictEqual(responseOfExtraRuns)
  })
})
