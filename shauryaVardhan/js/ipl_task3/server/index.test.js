const index = require('./index')
const fs = require('fs')
const path = require('path')

const JSONRESPONSE = [
  {
    city: 'Hyderabad',
    date: '2017-04-05',
    field9: '',
    id: '1',
    season: '2017',
    team1: 'Sunrisers Hyderabad',
    team2: 'Royal Challengers Bangalore',
    toss_decision: 'field',
    toss_winner: 'Royal Challengers Bangalore'
  },
  {
    city: 'Pune',
    date: '2017-04-06',
    field9: '',
    id: '2',
    season: '2017',
    team1: 'Mumbai Indians',
    team2: 'Rising Pune Supergiant',
    toss_decision: 'field',
    toss_winner: 'Rising Pune Supergiant'
  }
]

describe('task3 tests to convert csv data to json and write to file', () => {
  test('should write data to file correctly', async () => {
    const text = await index.writeFile('test.txt', { test: 'test' })
    expect(text).toBe('written successfully')
    fs.unlinkSync(path.join(__dirname, 'test.txt'))
  })
  test('should throw error for writing to wrong filename', async () => {
    expect.assertions(1)
    await expect(index.writeFile('')).rejects.toStrictEqual(
      new Error('failed to write to file')
    )
  })
  test('should convert csv to JSON correctly', async () => {
    const jsonResponse = await index.csvToJson('../data/test.csv')
    expect(jsonResponse).toStrictEqual(JSONRESPONSE)
  })
  test('should call getData function as expected', async () => {
    const text = await index.getData(
      '../data/matches.csv',
      '../data/deliveries.csv'
    )
    expect(text).toBe('executed successfully')
  })
  test('should throw error if cannot convert csv to json', async () => {
    await expect(
      index.getData('../data/matches.csv', '../data/deliveris.csv')
    ).rejects.toStrictEqual(new Error('failed to convert csv to JSON'))
  })
})
