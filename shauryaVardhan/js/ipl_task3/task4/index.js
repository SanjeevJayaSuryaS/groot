/* eslint-disable */

let currentChartNumber = 1

const svg1 = document.querySelector('svg')
const container = document.querySelector('#container')

const onClickNavbar = (event, chartNumber) => {
  switch (chartNumber) {
    case 1:
      container.classList.add('hidden')
      svg1.classList.remove('hidden')
      currentChartNumber = 1
      loadData()
      break
    case 2:
      svg1.classList.add('hidden')
      container.classList.remove('hidden')
      createUsingHighCharts()
      break
    case 3:
      container.classList.add('hidden')
      svg1.classList.remove('hidden')
      currentChartNumber = 3
      loadData()
      break
    default:
      throw new Error('invalid chart number')
  }
  const allLiTags = document.querySelectorAll('li')
  allLiTags.forEach((li) => li.classList.remove('selected-task'))
  event.classList.add('selected-task')
}

const svg = d3.select('svg')
const margin = { top: 30, right: 40, bottom: 180, left: 70 }

const xScale = d3.scaleBand().padding(0.5)

const yScale = d3.scaleLinear()

let data = undefined

const g = svg
  .append('g')
  .attr('class', 'temp')
  .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')

svg
  .append('text')
  .attr('class', 'svg-heading')
  .attr('transform', 'translate(100,0)')
  .attr('x', 50)
  .attr('y', 50)
  .attr('font-size', '1.5em')

g.append('g').attr('class', 'x-axis')
g.append('g').attr('class', 'y-axis')

g.append('text')
  .attr('class', 'y-legend')
  .attr('transform', 'rotate(-90)')
  .attr('y', 6)
  .attr('dy', '-3em')
  .attr('text-anchor', 'end')
  .attr('fill', '#333')

g.append('text')
  .attr('class', 'x-legend')
  .attr('y', 30)
  .attr('x', 50)
  .attr('text-anchor', 'end')
  .attr('fill', '#333')
  .attr('font-size', '1em')

const loadData = async () => {
  const dataLocation =
    currentChartNumber === 1
      ? '../output/matchesPerYear.json'
      : '../output/extraRunsIn2016.json'
  data = await d3.json(dataLocation)
  xScale.domain(
    data.map(function (d) {
      return currentChartNumber === 1 ? d.season : d.teamName
    })
  )
  yScale.domain([
    0,
    d3.max(data, function (d) {
      return currentChartNumber === 1 ? d.count + 20 : d.extraRuns + 20
    })
  ])

  drawSvg()
}

const drawSvg = () => {
  const bounds = svg.node().getBoundingClientRect()
  const width = bounds.width - margin.left - margin.right
  const height =
    bounds.height -
    margin.top -
    margin.bottom +
    (currentChartNumber === 1 ? 120 : 0)

  xScale.rangeRound([0, width])
  yScale.rangeRound([height, 0])

  g.select('.x-axis')
    .attr('transform', 'translate(0,' + height + ')')
    .call(d3.axisBottom(xScale))
    .selectAll('text')
    .attr('y', -4)
    .attr('x', -9)
    // .attr('dy', '.35em')
    .attr('transform', 'rotate(-90)')
    .style('text-anchor', 'end')

  g.select('.x-legend')
    .attr('x', width / 2 + 10)
    .attr('y', currentChartNumber === 1 ? height + 50 : height + 175)

  g.select('.y-axis').call(d3.axisLeft(yScale).ticks(10))

  svg
    .select('.svg-heading')
    .text(
      currentChartNumber === 1
        ? 'Matches Played Per Year'
        : 'Extra Runs Conceded in 2016'
    )
  g.select('.x-legend').text(currentChartNumber === 1 ? 'Year' : 'Team Name')
  g.select('.y-legend').text(
    currentChartNumber === 1 ? 'Matches Played' : 'Extra Runs'
  )

  const bars = g.selectAll('.bar').data(data)

  bars
    .enter()
    .append('rect')
    .attr('class', 'bar')
    .attr('x', function (d) {
      return xScale(currentChartNumber === 1 ? d.season : d.teamName)
    })
    .attr('y', function (d) {
      return yScale(currentChartNumber === 1 ? d.count : d.extraRuns)
    })
    .attr('width', xScale.bandwidth())
    .attr('height', function (d) {
      return height - yScale(currentChartNumber === 1 ? d.count : d.extraRuns)
    })

  bars
    .attr('x', function (d) {
      return xScale(currentChartNumber === 1 ? d.season : d.teamName)
    })
    .attr('y', function (d) {
      return yScale(currentChartNumber === 1 ? d.count : d.extraRuns)
    })
    .attr('width', xScale.bandwidth())
    .attr('height', function (d) {
      return height - yScale(currentChartNumber === 1 ? d.count : d.extraRuns)
    })

  bars.exit().remove()
}

const downloadButton = document.querySelector('.download-button')

downloadButton.setAttribute(
  'href',
  currentChartNumber === 1
    ? '../output/matchesPerYear.json'
    : '../output/extraRunsIn2016.json'
)
downloadButton.setAttribute(
  'download',
  currentChartNumber === 1 ? 'matchesPerYear.json' : 'extraRunsIn2016.json'
)

window.addEventListener('resize', loadData)
// createUsingD3(currentChartNumber)
loadData()

const createUsingHighCharts = async () => {
  const response = await fetch('../output/matchesWonPerTeamPerYear.json')
  const data = await response.json()

  const xAxis = data.map((ele) => ele.year)
  const distinctTeams = data[0].data.map((team) => team.team)

  const series = distinctTeams.map((teamName) => {
    return {
      name: teamName,
      data: data.map((ele) => {
        const data1 = ele.data
        const temp = data1.filter((team) => team.team === teamName)
        return temp[0].matchWonCount
      })
    }
  })

  Highcharts.chart('container', {
    chart: {
      type: 'column',
      backgroundColor: '#f2f2f2'
    },
    title: {
      text: 'Matches Won Per Team Per Year'
    },
    xAxis: {
      categories: xAxis,
      crosshair: true,
      title: {
        text: 'year'
      }
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Matches Won'
      }
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: series
  })

  const downloadButton = document.querySelector('.download-button')

  downloadButton.setAttribute('href', '../output/matchesWonPerTeamPerYear.json')
  downloadButton.setAttribute('download', 'matchesWonPerTeamPerYear.json')
}
