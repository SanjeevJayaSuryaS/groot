const fs = require('fs').promises
const axios = require('axios')
const path = require('path')

const readFilePromise = async (fileToReadFrom) => {
  try {
    const text = await fs.readFile(path.join(__dirname, fileToReadFrom), 'utf8')
    return text
  } catch {
    throw new Error('failed to read the file')
  }
}

const writeToFilePromise = async (fileToWriteTo, imageUrl) => {
  try {
    await fs.writeFile(path.join(__dirname, fileToWriteTo), imageUrl)
    return imageUrl
  } catch {
    throw new Error('failed to write to the File')
  }
}

const getImagesFromApi = async (apiUrl) => {
  try {
    const dataFromApi = await Promise.all([
      axios.get(apiUrl),
      axios.get(apiUrl),
      axios.get(apiUrl),
      axios.get(apiUrl),
      axios.get(apiUrl)
    ])
    const imageUrls = dataFromApi.map((ele) => ele?.data?.message)
    const imageUrlsAsString = imageUrls.join('\n')
    return imageUrlsAsString
  } catch {
    throw new Error('failed while getting response from api')
  }
}

const fetch5Images = async () => {
  const dogBreedName = await readFilePromise('dogBreed.txt')
  console.log('read data successfully as', dogBreedName)
  const apiUrl = `https://dog.ceo/api/breed/${dogBreedName}/images/random`
  const imageUrlsAsString = await getImagesFromApi(apiUrl)
  await writeToFilePromise('dogImages.txt', imageUrlsAsString)
}

// fetch5Images()

module.exports = {
  readFilePromise,
  writeToFilePromise,
  fetch5Images,
  getImagesFromApi
}
