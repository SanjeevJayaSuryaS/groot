const {
  fetch5Images,
  getImagesFromApi,
  readFilePromise,
  writeToFilePromise
} = require('./fetch5Images')
const axios = require('axios')
const fs = require('fs')
const path = require('path')
const WRONG_URL = 'https://dog.ceo/api/breed/nodog/images/random'

const initializeDogBreed = async () => {
  await fs.writeFile(
    path.join(__dirname, 'dogBreed.txt'),
    'labrador',
    (err) => {
      if (err) console.log('failed')
      console.log('written labrador')
    }
  )
}

beforeAll(() => {
  return initializeDogBreed()
})

describe('fetch5Images', () => {
  test('should read text from file correctly', async () => {
    const text = await readFilePromise('dogBreed.txt')
    expect(text).toBe('labrador')
  })
  test('should throw error for reading from wrong filename', async () => {
    expect.assertions(1)
    await expect(readFilePromise('')).rejects.toStrictEqual(
      new Error('failed to read the file')
    )
  })
  test('should write text to file correctly', async () => {
    const text = await writeToFilePromise('dogBreedTestFile.txt', 'test')
    expect(text).toBe('test')
    fs.unlinkSync(path.join(__dirname, 'dogBreedTestFile.txt'))
  })
  test('should throw error for writing to wrong filename', async () => {
    expect.assertions(1)
    await expect(writeToFilePromise('')).rejects.toStrictEqual(
      new Error('failed to write to the File')
    )
  })
  test('should throw error if invalid apiUrl is provided', async () => {
    await expect(getImagesFromApi(WRONG_URL)).rejects.toStrictEqual(
      new Error('failed while getting response from api')
    )
  })
  test('should make api request 5 times', async () => {
    const spy = jest.spyOn(axios, 'get')
    await fetch5Images()
    expect(spy).toHaveBeenCalledTimes(5)
  })
})
