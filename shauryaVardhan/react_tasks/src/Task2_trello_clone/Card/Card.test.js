import React from 'react'
import { fireEvent, render, screen, act } from '@testing-library/react'
import Card from './Card'
import axios from 'axios'

beforeEach(async () => {
  await act(async () =>
    render(
      <Card
        cardId="123"
        cardName="test"
        fetchListsAndCards={() => {
          return new Promise((resolve, reject) => resolve(1))
        }}
      />
    )
  )
})

describe('test Card.js', () => {
  it('should display cardName', () => {
    expect(screen.getByText('test')).toBeInTheDocument()
  })
  it('should edit card name correctly', async () => {
    const cardEditBtn = screen.getByTestId('cardEditBtn')
    fireEvent.click(cardEditBtn)
    const spy = jest.spyOn(axios, 'put').mockImplementation(() => {
      return new Promise((resolve, reject) => resolve(1))
    })
    const cardEditInput = screen.getByTestId('cardEditInput')
    fireEvent.change(cardEditInput, { target: { value: 'newName' } })
    fireEvent.click(screen.getByText('Submit'))
    expect(spy).toHaveBeenCalledTimes(1)
  })
  it('should throw error if network error in updating card name', async () => {
    axios.put.mockRejectedValueOnce()
    const cardEditBtn = screen.getByTestId('cardEditBtn')
    fireEvent.click(cardEditBtn)
    const cardEditInput = screen.getByTestId('cardEditInput')
    fireEvent.change(cardEditInput, { target: { value: 'newName' } })
    fireEvent.click(screen.getByText('Submit'))
    render(
      <Card
        cardId="123"
        cardName="test"
        fetchListsAndCards={() => {
          return new Promise((resolve, reject) => resolve(1))
        }}
      />
    )
    const errorElement = await screen.findByTestId('errorElement')
    expect(errorElement).toBeInTheDocument()
    expect(errorElement.textContent).toBe('failed to update the card')
  })
  it('should delete card correctly', async () => {
    const cardDeleteBtn = screen.getByTestId('testDelete')
    const spy = jest.spyOn(axios, 'delete').mockImplementation(() => {})
    fireEvent.click(cardDeleteBtn)
    expect(spy).toHaveBeenCalledTimes(1)
  })
  it('should throw error if network error in deleting card', async () => {
    axios.delete.mockRejectedValueOnce()
    const cardDeleteBtn = screen.getByTestId('testDelete')
    fireEvent.click(cardDeleteBtn)
    render(
      <Card
        cardId="123"
        cardName="test"
        fetchListsAndCards={() => {
          return new Promise((resolve, reject) => resolve(1))
        }}
      />
    )
    const errorElement = await screen.findByTestId('errorElement')
    expect(errorElement).toBeInTheDocument()
    expect(errorElement.textContent).toBe('failed to delete the card')
  })
})
