import React, { useState } from 'react'
import './Card.css'
import axios from 'axios'
import { MdEdit, MdDelete } from 'react-icons/md'
import PropTypes from 'prop-types'
import Loader from '../Loader'

export default function Card({
  cardId,
  cardName,
  fetchListsAndCards,
  setLists,
  id
}) {
  const [isEditingCard, setIsEditingCard] = useState(false)
  const [cardInputValue, setCardInputValue] = useState('')
  const [isLoading, setIsLoading] = useState(false)
  const [error, setError] = useState('')

  const handleInputOnChange = (e) => {
    setCardInputValue(e.target.value)
  }

  const handleEditInput = async () => {
    try {
      setIsLoading(true)
      await axios.put(
        `https://api.trello.com/1/cards/${cardId}?key=${process.env.REACT_APP_KEY}&token=${process.env.REACT_APP_TOKEN}&name=${cardInputValue}`
      )
      setIsEditingCard(false)
      const listsAndCards = await fetchListsAndCards(id)
      setIsLoading(false)
      setLists(listsAndCards)
    } catch {
      setError('failed to update the card')
    }
  }

  const handleEditClick = () => {
    setCardInputValue(cardName)
    setIsEditingCard(true)
  }

  const handleDeleteClick = async () => {
    setIsLoading(true)
    try {
      await axios.delete(
        `https://api.trello.com/1/cards/${cardId}?key=${process.env.REACT_APP_KEY}&token=${process.env.REACT_APP_TOKEN}`
      )
      const listsAndCards = await fetchListsAndCards(id)
      setIsLoading(false)
      setLists(listsAndCards)
    } catch (e) {
      setError('failed to delete the card')
    }
  }
  return error ? (
    <p data-testid="errorElement">{error}</p>
  ) : (
    <div className="card">
      {isLoading ? (
        <Loader />
      ) : isEditingCard ? (
        <>
          <input
            type="text"
            value={cardInputValue}
            onChange={handleInputOnChange}
            data-testid="cardEditInput"
            className="card-edit-input"
          />
          <button onClick={handleEditInput}>Submit</button>
        </>
      ) : (
        <>
          <div className="cardText">{cardName}</div>
          <div className="card-actions">
            <div className="card-edit-btn">
              <MdEdit data-testid="cardEditBtn" onClick={handleEditClick} />
            </div>
            <div className="card-delete-btn">
              <MdDelete
                data-testid={`${cardName}Delete`}
                onClick={handleDeleteClick}
              />
            </div>
          </div>
        </>
      )}
    </div>
  )
}

Card.propTypes = {
  cardName: PropTypes.string,
  cardId: PropTypes.string,
  fetchListsAndCards: PropTypes.func,
  id: PropTypes.string,
  setLists: PropTypes.func
}
