import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import axios from 'axios'
import './board.css'
import { MdAdd, MdDelete } from 'react-icons/md'
import Card from '../Card/Card'
import Loader from '../Loader'

export const fetchListsAndCards = async (id) => {
  try {
    const url = `https://api.trello.com/1/boards/${id}/lists?key=${process.env.REACT_APP_KEY}&token=${process.env.REACT_APP_TOKEN}`

    const response = await axios.get(url)
    const cardUrls = response.data.map((list) => {
      return axios.get(
        `https://api.trello.com/1/lists/${list.id}/cards?key=${process.env.REACT_APP_KEY}&token=${process.env.REACT_APP_TOKEN}`
      )
    })
    const cards = await Promise.all(cardUrls)
    const listsWithCards = response.data.map((list, index) => ({
      ...list,
      cards: cards[index].data
    }))
    return listsWithCards
  } catch (e) {
    throw new Error('failed to load the lists')
  }
}

function Board() {
  const { id } = useParams()
  const [lists, setLists] = useState([])
  const [boardName, setBoardName] = useState('loading board name')
  const [editBoardInput, setEditBoardInput] = useState('')
  const [isEditingBoardName, setIsEditingBoardName] = useState(false)
  const [isAddingNewList, setIsAddingNewList] = useState(false)
  const [newListName, setNewListName] = useState('')
  const [isEditingListName, setIsEditingListName] = useState(false)
  const [updatedListName, setUpdatedListName] = useState('')
  const [editingListId, setEditingListId] = useState('')
  const [isAddingCard, setIsAddingCard] = useState(false)
  const [addingCardListId, setAddingCardListId] = useState('')
  const [editCardName, setEditCardName] = useState('')
  const [isLoading, setIsLoading] = useState(false)
  const [error, setError] = useState('')

  useEffect(() => {
    let isMounted = true
    const fetchBoardName = async () => {
      try {
        const response = await axios.get(
          `https://api.trello.com/1/boards/${id}?key=${process.env.REACT_APP_KEY}&token=${process.env.REACT_APP_TOKEN}`
        )
        /* istanbul ignore else */
        if (isMounted) {
          setBoardName(response?.data?.name)
        }
      } catch (e) {
        setError('failed to load the board name')
      }
    }
    const fetchLists = async () => {
      const listsAndCards = await fetchListsAndCards(id)
      /* istanbul ignore else */
      if (isMounted) {
        setLists(listsAndCards)
      }
    }
    fetchBoardName()
    fetchLists()
    return () => {
      isMounted = false
    }
  }, [])

  const editBoardName = () => {
    setEditBoardInput(boardName)
    setIsEditingBoardName(true)
  }

  const handleNewListInput = (e) => {
    setNewListName(e.target.value)
  }

  const createNewList = async () => {
    setIsLoading(true)
    try {
      await axios.post(
        `https://api.trello.com/1/lists?key=${process.env.REACT_APP_KEY}&token=${process.env.REACT_APP_TOKEN}&name=${newListName}&idBoard=${id}&pos=bottom`
      )
      setNewListName('')
      const listsAndCards = await fetchListsAndCards(id)
      setLists(listsAndCards)
      setIsLoading(false)
      setIsAddingNewList(false)
    } catch (e) {
      setError('failed to create a list')
    }
  }

  const handleBoardInputKeyPress = async (e) => {
    if (e.key === 'Enter') {
      try {
        setBoardName('updating')
        setIsEditingBoardName(false)
        await axios.put(
          `https://api.trello.com/1/boards/${id}?key=${process.env.REACT_APP_KEY}&token=${process.env.REACT_APP_TOKEN}&name=${editBoardInput}`
        )
        setBoardName(e.target.value)
      } catch (e) {
        setError('failed to update the board')
      }
    }
  }

  const handleUpdateListInput = (e) => {
    setUpdatedListName(e.target.value)
  }
  const handleListDelete = async (listId) => {
    try {
      await axios.put(
        `https://api.trello.com/1/lists/${listId}/closed?key=${process.env.REACT_APP_KEY}&token=${process.env.REACT_APP_TOKEN}&value=true`
      )
      const listsAndCards = await fetchListsAndCards(id)
      setLists(listsAndCards)
    } catch (e) {
      setError('failed to delete the list')
    }
  }

  const handleListInputKeyPress = async (e, listId) => {
    if (e.key === 'Enter') {
      try {
        await axios.put(
          `https://api.trello.com/1/lists/${listId}?key=${process.env.REACT_APP_KEY}&token=${process.env.REACT_APP_TOKEN}&name=${updatedListName}`
        )
        const listsAndCards = await fetchListsAndCards(id)
        setLists(listsAndCards)
        setIsEditingListName(false)
      } catch (e) {
        setError('failed to update the list')
      }
    }
  }
  const handleEditBoardInput = (e) => {
    setEditBoardInput(e.target.value)
  }

  const handleListEdit = (listId, listName) => {
    setUpdatedListName(listName)
    setEditingListId(listId)
    setIsEditingListName(true)
  }

  const handleAddCard = async (listId) => {
    setIsLoading(true)
    try {
      await axios.post(
        `https://api.trello.com/1/cards?key=${process.env.REACT_APP_KEY}&token=${process.env.REACT_APP_TOKEN}&idList=${listId}&name=${editCardName}`
      )
      const listsAndCards = await fetchListsAndCards(id)
      setLists(listsAndCards)
    } catch (e) {
      setError('failed to add the card')
    }
    setIsLoading(false)
    setIsAddingCard(false)
    setEditCardName('')
  }

  const handleAddCardClick = (listId) => {
    setAddingCardListId(listId)
    setIsAddingCard(true)
  }

  const handleEditCardName = (e) => {
    setEditCardName(e.target.value)
  }

  return error ? (
    <p data-testid="errorElement">{error}</p>
  ) : (
    <section className="board-section">
      <div className="board-name">
        {isEditingBoardName ? (
          <input
            data-testid="editBoardInput"
            type="text"
            value={editBoardInput}
            onChange={handleEditBoardInput}
            onKeyPress={handleBoardInputKeyPress}
          />
        ) : (
          <div onClick={editBoardName} data-testid="boardName">
            {boardName}
          </div>
        )}
      </div>
      <main className="lists-wrapper">
        {lists.length > 0 ? (
          <>
            {lists.map((list) => (
              <div key={list.id} className="list">
                <div className="list-header">
                  {isEditingListName && editingListId === list.id ? (
                    <input
                      type="text"
                      className="list-name-input"
                      data-testid={`${list.name}Input`}
                      value={updatedListName}
                      onChange={handleUpdateListInput}
                      onKeyPress={(e) => handleListInputKeyPress(e, list.id)}
                    />
                  ) : (
                    <h3
                      className="list-header-heading"
                      onClick={() => handleListEdit(list.id, list.name)}
                      data-testid={list.name}
                    >
                      {list.name}
                    </h3>
                  )}
                  <MdDelete
                    data-testid={`${list.name}Delete`}
                    onClick={() => handleListDelete(list.id)}
                  />
                </div>
                <div className="all-list-cards">
                  {list.cards.map((card) => (
                    <Card
                      key={card.id}
                      cardId={card.id}
                      cardName={card.name}
                      fetchListsAndCards={fetchListsAndCards}
                      setLists={setLists}
                      id={id}
                    />
                  ))}
                </div>

                {isAddingCard && addingCardListId === list.id ? (
                  isLoading ? (
                    <Loader />
                  ) : (
                    <div className="add-card-input-wrapper">
                      <input
                        type="text"
                        data-testid={`add${list.name}CardInput`}
                        value={editCardName}
                        onChange={handleEditCardName}
                        placeholder="Enter a title for this card..."
                      />
                      <div className="add-list-actions">
                        <button
                          className="create-list-btn"
                          onClick={() => handleAddCard(list.id)}
                        >
                          Submit
                        </button>
                        <button
                          className="close-create-list-btn"
                          onClick={() => setIsAddingCard(false)}
                        >
                          Close
                        </button>
                      </div>
                    </div>
                  )
                ) : (
                  <div
                    className="add-card"
                    data-testid={`add${list.name}Card`}
                    onClick={() => handleAddCardClick(list.id)}
                  >
                    <MdAdd />
                    Add another card
                  </div>
                )}
              </div>
            ))}
            <div className="list add-list">
              <h3 className="list-header-heading">
                {!isAddingNewList ? (
                  <div
                    className="add-list-btn"
                    onClick={() => setIsAddingNewList(true)}
                  >
                    <MdAdd />
                    Add a new list
                  </div>
                ) : isLoading ? (
                  <Loader />
                ) : (
                  <div>
                    <input
                      type="text"
                      data-testid="newListInput"
                      value={newListName}
                      onChange={handleNewListInput}
                      placeholder="Enter list title..."
                    />
                    <div className="add-list-actions">
                      <button
                        className="create-list-btn"
                        onClick={createNewList}
                      >
                        Submit
                      </button>
                      <button
                        className="close-create-list-btn"
                        onClick={() => setIsAddingNewList(false)}
                      >
                        Close
                      </button>
                    </div>
                  </div>
                )}
              </h3>
            </div>
          </>
        ) : (
          <div>Loading lists</div>
        )}
      </main>
    </section>
  )
}

export default Board
