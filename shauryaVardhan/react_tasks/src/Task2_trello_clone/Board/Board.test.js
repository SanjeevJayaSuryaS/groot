import React from 'react'
import {
  render,
  screen,
  waitFor,
  cleanup,
  fireEvent,
  act
} from '@testing-library/react'
import Board, { fetchListsAndCards } from './Board'
import { MemoryRouter } from 'react-router-dom'
import axios from 'axios'

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useParams: () => ({
    id: '603744e5b240bd86c90ca5b4'
  })
}))

jest.mock('axios')

beforeEach(() => {
  const boardData = {
    data: [
      {
        id: '1',
        name: 'BoardName 1',
        prefs: {
          backgroundImage: 'url'
        }
      }
    ]
  }
  const cardsData = {
    data: [
      {
        id: '1',
        idBoard: '12',
        idList: '123',
        name: 'task0'
      }
    ]
  }
  const listData = {
    data: [
      {
        id: '123',
        idBoard: '123',
        name: 'Doing'
      },
      {
        id: '124',
        idBoard: '123',
        name: 'Done'
      }
    ]
  }
  axios.get.mockImplementation(async (url) => {
    if (
      url.includes('https://api.trello.com/1/lists') &&
      url.includes('cards')
    ) {
      return cardsData
    } else if (
      url.includes('https://api.trello.com/1/board') &&
      url.includes('lists')
    ) {
      return listData
    } else if (url.includes('https://api.trello.com/1/board')) {
      return boardData
    }
  })
  axios.put.mockImplementation(() => {})
  axios.post.mockImplementation(() => {})
})

beforeEach(async () => {
  await act(async () =>
    render(
      <MemoryRouter initialEntries={['/board/603744e5b240bd86c90ca5b4']}>
        <Board />
      </MemoryRouter>
    )
  )
})

afterEach(() => {
  cleanup()
})

describe('tests Board.js', () => {
  it('should show lists', async () => {
    const doingEle = await waitFor(() => screen.getByText(/Doing/i))
    expect(doingEle).toBeInTheDocument()
  })
  it('should throw error if network error in fetching board name', async () => {
    axios.get.mockRejectedValueOnce()
    render(
      <MemoryRouter initialEntries={['/board/603744e5b240bd86c90ca5b4']}>
        <Board />
      </MemoryRouter>
    )
    const errorElement = await screen.findByTestId('errorElement')
    expect(errorElement).toBeInTheDocument()
    expect(errorElement.textContent).toBe('failed to load the board name')
  })
  it('should update the board name correctly', async () => {
    const spy = jest.spyOn(axios, 'put')
    const boardName = screen.getByTestId('boardName')
    fireEvent.click(boardName)
    const editInput = screen.queryByTestId('editBoardInput')
    fireEvent.change(editInput, { target: { value: 'test123' } })
    fireEvent.keyPress(editInput, { key: 'Enter', code: 13, charCode: 13 })
    expect(screen.getByText('updating')).toBeInTheDocument()
    expect(spy).toHaveBeenCalledTimes(1)
  })
  it('should throw error if network error in updating board name', async () => {
    axios.put.mockRejectedValueOnce()
    const boardName = screen.getByTestId('boardName')
    fireEvent.click(boardName)
    const editInput = screen.queryByTestId('editBoardInput')
    fireEvent.change(editInput, { target: { value: 'test123' } })
    fireEvent.keyPress(editInput, { key: 'Enter', code: 13, charCode: 13 })
    render(
      <MemoryRouter initialEntries={['/board/603744e5b240bd86c90ca5b4']}>
        <Board />
      </MemoryRouter>
    )
    const errorElement = await screen.findByTestId('errorElement')
    expect(errorElement).toBeInTheDocument()
    expect(errorElement.textContent).toBe('failed to update the board')
  })
  it('should not update board name if any key other than enter is pressed', async () => {
    const boardName = screen.getByTestId('boardName')
    fireEvent.click(boardName)
    const editInput = screen.queryByTestId('editBoardInput')
    fireEvent.change(editInput, { target: { value: 'test1' } })
    fireEvent.keyPress(editInput, { key: 'q', code: 81, charCode: 81 })
    expect(editInput).toBeInTheDocument()
  })
  it('should update the list name correctly', async () => {
    const listName = screen.getByTestId('Doing')
    const spy = jest.spyOn(axios, 'put')
    fireEvent.click(listName)
    const editInput = screen.queryByTestId('DoingInput')
    fireEvent.change(editInput, { target: { value: 'Done' } })
    fireEvent.keyPress(editInput, { key: 'Enter', code: 13, charCode: 13 })
    expect(spy).toHaveBeenCalledTimes(1)
  })
  it('should throw error if network error in updating list name', async () => {
    axios.put.mockRejectedValueOnce()
    const listName = screen.getByTestId('Doing')
    fireEvent.click(listName)
    const editInput = screen.queryByTestId('DoingInput')
    fireEvent.change(editInput, { target: { value: 'Done' } })
    fireEvent.keyPress(editInput, { key: 'Enter', code: 13, charCode: 13 })
    render(
      <MemoryRouter initialEntries={['/board/603744e5b240bd86c90ca5b4']}>
        <Board />
      </MemoryRouter>
    )
    const errorElement = await screen.findByTestId('errorElement')
    expect(errorElement).toBeInTheDocument()
    expect(errorElement.textContent).toBe('failed to update the list')
  })
  it('should delete the list correctly', async () => {
    const spy = jest.spyOn(axios, 'put')
    const deleteBtn = screen.getByTestId(`DoingDelete`)
    fireEvent.click(deleteBtn)
    expect(spy).toHaveBeenCalledTimes(1)
  })
  it('should throw error if network error in deleting list', async () => {
    axios.put.mockRejectedValueOnce()
    const deleteBtn = screen.getByTestId(`DoingDelete`)
    fireEvent.click(deleteBtn)
    render(
      <MemoryRouter initialEntries={['/board/603744e5b240bd86c90ca5b4']}>
        <Board />
      </MemoryRouter>
    )
    const errorElement = await screen.findByTestId('errorElement')
    expect(errorElement).toBeInTheDocument()
    expect(errorElement.textContent).toBe('failed to delete the list')
  })
  it('should not update list name if any key other than enter is pressed', async () => {
    const listName = screen.getByTestId('Done')
    fireEvent.click(listName)
    const editInput = screen.queryByTestId('DoneInput')
    fireEvent.change(editInput, { target: { value: 'Done1' } })
    fireEvent.keyPress(editInput, { key: 'q', code: 81, charCode: 81 })
    expect(editInput).toBeInTheDocument()
  })
  it('should add card to list correctly', async () => {
    const spy = jest.spyOn(axios, 'post')
    const addCard = screen.getByTestId('addDoneCard')
    fireEvent.click(addCard)
    const editInput = screen.queryByTestId('addDoneCardInput')
    fireEvent.change(editInput, { target: { value: 'newDoneCard' } })
    const submitBtn = screen.getByText('Submit')
    fireEvent.click(submitBtn)
    expect(spy).toHaveBeenCalledTimes(1)
  })
  it('should throw error if network error in adding card to list', async () => {
    axios.post.mockRejectedValueOnce()
    const addCard = screen.getByTestId('addDoneCard')
    fireEvent.click(addCard)
    const editInput = screen.queryByTestId('addDoneCardInput')
    fireEvent.change(editInput, { target: { value: 'newDoneCard' } })
    const submitBtn = screen.getByText('Submit')
    fireEvent.click(submitBtn)
    render(
      <MemoryRouter initialEntries={['/board/603744e5b240bd86c90ca5b4']}>
        <Board />
      </MemoryRouter>
    )
    const errorElement = await screen.findByTestId('errorElement')
    expect(errorElement).toBeInTheDocument()
    expect(errorElement.textContent).toBe('failed to add the card')
  })
  it('should close new card input on clicking on close button', async () => {
    const addCard = screen.getByTestId('addDoneCard')
    fireEvent.click(addCard)
    const closeBtn = screen.getByText('Close')
    fireEvent.click(closeBtn)
    expect(screen.queryByTestId('addDoneCardInput')).toBeNull()
  })
  it('should add new list correctly', async () => {
    const newListBtn = screen.getByText('Add a new list')
    fireEvent.click(newListBtn)
    const spy = jest.spyOn(axios, 'post')
    const newListInput = screen.getByTestId('newListInput')
    fireEvent.change(newListInput, { target: { value: 'newList' } })
    const submitBtn = screen.getByText('Submit')
    fireEvent.click(submitBtn)
    expect(spy).toHaveBeenCalledTimes(1)
  })
  it('should throw error if network error in adding a list', async () => {
    axios.post.mockRejectedValueOnce()
    const newListBtn = screen.getByText('Add a new list')
    fireEvent.click(newListBtn)
    const newListInput = screen.getByTestId('newListInput')
    fireEvent.change(newListInput, { target: { value: 'newList' } })
    const submitBtn = screen.getByText('Submit')
    fireEvent.click(submitBtn)
    render(
      <MemoryRouter initialEntries={['/board/603744e5b240bd86c90ca5b4']}>
        <Board />
      </MemoryRouter>
    )
    const errorElement = await screen.findByTestId('errorElement')
    expect(errorElement).toBeInTheDocument()
    expect(errorElement.textContent).toBe('failed to create a list')
  })
  it('should close new list input on clicking on close button', async () => {
    const newListBtn = screen.getByText('Add a new list')
    fireEvent.click(newListBtn)
    const closeBtn = screen.getByText('Close')
    fireEvent.click(closeBtn)
    expect(screen.queryByTestId('newListInput')).toBeNull()
  })
  it('should throw error if network error during fetching of the lists', async () => {
    axios.get.mockRejectedValueOnce()
    await expect(fetchListsAndCards(123)).rejects.toStrictEqual(
      new Error('failed to load the lists')
    )
  })
})
