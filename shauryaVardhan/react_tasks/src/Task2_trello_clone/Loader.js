import React from 'react'
import './loader.css'

export default function Loader() {
  return (
    <div className="lds-ring" data-testid="loader">
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </div>
  )
}
