import React from 'react'
import './header.css'
import { FaHome, FaSearch, FaPlus } from 'react-icons/fa'
import { Link } from 'react-router-dom'

// eslint-disable-next-line react/prop-types
export default function Header({ searchInput, setSearchInput }) {
  return (
    <nav className="navbar">
      <div className="navbar-left">
        <Link to="/" className="navbar-item">
          <FaHome />
        </Link>
        <div className="navbar-search">
          <input
            type="text"
            placeholder="Jump to..."
            value={searchInput}
            onChange={(e) => setSearchInput(e.target.value)}
          />
          <FaSearch />
        </div>
      </div>
      <Link to="/" className="navbar-heading">
        <div>Trello</div>
      </Link>
      <div>
        <div className="navbar-item">
          <FaPlus />
        </div>
      </div>
    </nav>
  )
}
