import React from 'react'
import { fireEvent, render, screen, act } from '@testing-library/react'
import { MemoryRouter } from 'react-router-dom'
import Header from './Header'

let searchInput = ''

beforeEach(async () => {
  await act(async () =>
    render(
      <MemoryRouter>
        <Header
          searchInput={searchInput}
          setSearchInput={(e) => {
            searchInput = e
          }}
        />
      </MemoryRouter>
    )
  )
})

describe('tests Card.js', () => {
  it('should change input correctly', () => {
    const input = screen.getByPlaceholderText('Jump to...')
    fireEvent.change(input, { target: { value: 'search' } })
    expect(searchInput).toBe('search')
  })
})
