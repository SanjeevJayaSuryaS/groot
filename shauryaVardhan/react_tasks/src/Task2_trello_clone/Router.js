import React, { useState } from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Dashboard from './Dashboard/Dashboard'
import Board from './Board/Board'
import Header from './Header/Header'
export default function Router() {
  const [searchInput, setSearchInput] = useState('')
  return (
    <>
      <BrowserRouter>
        <Header searchInput={searchInput} setSearchInput={setSearchInput} />
        <Switch>
          <Route
            exact
            path="/"
            render={() => <Dashboard searchInput={searchInput} />}
          />
          <Route path="/board/:id" component={Board} />
        </Switch>
      </BrowserRouter>
    </>
  )
}
