import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import axios from 'axios'
import './dashboard.css'
import { MdDelete } from 'react-icons/md'

// eslint-disable-next-line react/prop-types
function Dashboard({ searchInput }) {
  const [boards, setBoards] = useState([])
  const [showModal, setShowModal] = useState(false)
  const [boardName, setBoardName] = useState('')
  const [error, setError] = useState('')
  useEffect(() => {
    let isMounted = true
    const fetchData = async () => {
      try {
        const url = `https://api.trello.com/1/members/me/boards?key=${process.env.REACT_APP_KEY}&token=${process.env.REACT_APP_TOKEN}`
        const response = await axios.get(url)
        /* istanbul ignore else */
        if (isMounted) {
          setBoards(response.data)
        }
      } catch (err) {
        setError('failed while getting boards')
      }
    }

    fetchData()

    return () => {
      isMounted = false
    }
  }, [])

  const history = useHistory()
  const boardClick = (boardId, boardName) => {
    history.push(`/board/${boardId}`, { boardName: boardName })
  }

  const createBoard = async () => {
    try {
      await axios.post(
        `https://api.trello.com/1/boards/?key=${process.env.REACT_APP_KEY}&token=${process.env.REACT_APP_TOKEN}&name=${boardName}`
      )
      const url = `https://api.trello.com/1/members/me/boards?key=${process.env.REACT_APP_KEY}&token=${process.env.REACT_APP_TOKEN}`
      const response = await axios.get(url)
      setBoards(response.data)
      setShowModal(false)
    } catch (e) {
      setError('Failed while creating a board')
    }
  }

  const deleteBoard = async (e, boardId) => {
    e.stopPropagation()
    try {
      await axios.delete(
        `https://api.trello.com/1/boards/${boardId}?key=${process.env.REACT_APP_KEY}&token=${process.env.REACT_APP_TOKEN}`
      )
      const url = `https://api.trello.com/1/members/me/boards?key=${process.env.REACT_APP_KEY}&token=${process.env.REACT_APP_TOKEN}`
      const response = await axios.get(url)
      setBoards(response.data)
    } catch (e) {
      setError('failed while deleting the board')
    }
  }

  const handleBoardName = (e) => {
    setBoardName(e.target.value)
  }
  return error ? (
    <p data-testid="errorElement">{error}</p>
  ) : (
    <section className="dashboard">
      <h1>Dashboard</h1>
      <main className="container">
        {boards.length > 0 ? (
          <>
            {boards
              .filter((board) => new RegExp(searchInput, 'i').test(board.name))
              .map((board) => (
                <div
                  key={board.id}
                  className="board"
                  data-testid={board.id}
                  dataid={board.id}
                  onClick={() => boardClick(board.id, board.name)}
                >
                  <h3>{board.name}</h3>
                  <MdDelete
                    data-testid={board.name}
                    onClick={(e) => deleteBoard(e, board.id)}
                  />
                </div>
              ))}
            <div className="board new-board" onClick={() => setShowModal(true)}>
              Create new board
            </div>
          </>
        ) : (
          <div>Loading boards</div>
        )}
      </main>
      {showModal && (
        <div className="add-board-backdrop" data-testid="modal">
          <div className="add-board-modal">
            <input
              type="text"
              data-testid="create-board-input"
              value={boardName}
              onChange={handleBoardName}
              placeholder="Add board title"
            />
            <button onClick={createBoard} className="create-board-btn">
              Create board
            </button>
            <button
              onClick={() => setShowModal(false)}
              className="close-modal-btn"
            >
              Close
            </button>
          </div>
        </div>
      )}
    </section>
  )
}

export default Dashboard
