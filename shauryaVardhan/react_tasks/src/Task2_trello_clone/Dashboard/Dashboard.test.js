import React from 'react'
import {
  render,
  screen,
  waitFor,
  getByText,
  cleanup,
  fireEvent,
  getByTestId,
  queryByTestId,
  act
} from '@testing-library/react'
import Dashboard from './Dashboard'
import axios from 'axios'
import { createMemoryHistory } from 'history'
import { Router } from 'react-router'

let container = null

const history = createMemoryHistory()
// history.push('/')

jest.mock('axios')

beforeEach(() => {
  const boardData = {
    data: [
      {
        id: '1',
        name: 'BoardName 1',
        prefs: {
          backgroundImage: 'url'
        }
      },
      {
        id: '5fff131db96bb72c457fe451',
        name: 'test'
      }
    ]
  }
  const cardsData = {
    data: [
      {
        id: '1',
        idBoard: '12',
        idList: '123',
        name: 'task0'
      }
    ]
  }
  const listData = {
    data: [
      {
        id: '123',
        idBoard: '123',
        name: 'Doing'
      },
      {
        id: '124',
        idBoard: '123',
        name: 'Done'
      }
    ]
  }
  axios.get.mockImplementation(async (url) => {
    if (
      url.includes('https://api.trello.com/1/lists') &&
      url.includes('cards')
    ) {
      return cardsData
    } else if (
      url.includes('https://api.trello.com/1/board') &&
      url.includes('lists')
    ) {
      return listData
    } else if (url.includes('https://api.trello.com/1/members/me/boards')) {
      return boardData
    }
  })
  axios.put.mockImplementation(() => {})
  axios.post.mockImplementation(() => {})
  axios.delete.mockImplementation(() => {})
})

beforeEach(async () => {
  await act(async () => {
    container = render(
      <Router history={history}>
        <Dashboard />
      </Router>
    ).container
  })
})

afterEach(() => {
  container = null
  cleanup()
})

const openModal = async () => {
  const createBoardElement = await waitFor(
    () => getByText(container, /Create new board/i),
    { timeout: 2500 }
  )
  fireEvent.click(createBoardElement)
}

describe('tests Dashboard.js', () => {
  it('renders correctly', () => {
    const { asFragment } = render(<Dashboard />)
    expect(asFragment()).toMatchSnapshot()
  })
  it('shows board correctly', async () => {
    const createBoardElement = await waitFor(
      () => getByText(container, /Create new board/i),
      { timeout: 2500 }
    )
    expect(createBoardElement).toBeInTheDocument()
  })
  it('should throw error if url not found', async () => {
    axios.get.mockRejectedValueOnce()
    render(<Dashboard />)
    expect(await screen.findByTestId('errorElement')).toBeInTheDocument()
  })
  it('opens up the modal correctly', async () => {
    await openModal()
    expect(getByTestId(container, 'create-board-input')).toBeInTheDocument()
  })
  it('deletes board correctly', async () => {
    const spy = jest.spyOn(axios, 'delete')
    const deleteButton = getByTestId(container, 'BoardName 1')
    fireEvent.click(deleteButton)
    expect(spy).toHaveBeenCalledTimes(1)
  })
  it('should throw error if network lost during delete board', async () => {
    axios.delete.mockRejectedValueOnce()
    render(<Dashboard />)
    const deleteButton = getByTestId(container, 'BoardName 1')
    fireEvent.click(deleteButton)
    expect(await screen.findByTestId('errorElement')).toBeInTheDocument()
  })
  it('adds board correctly', async () => {
    await openModal()
    const spy = jest.spyOn(axios, 'post')
    const inputElement = getByTestId(container, 'create-board-input')
    fireEvent.change(inputElement, { target: { value: 'newBoard' } })
    const submitBtn = getByText(container, /create board/i)
    fireEvent.click(submitBtn)
    expect(spy).toHaveBeenCalledTimes(1)
  })
  it('should throw error if network lost during add board', async () => {
    axios.post.mockRejectedValueOnce()
    render(<Dashboard />)
    await openModal()
    const inputElement = getByTestId(container, 'create-board-input')
    fireEvent.change(inputElement, { target: { value: 'newBoard' } })
    const submitBtn = getByText(container, /create board/i)
    fireEvent.click(submitBtn)
    expect(await screen.findByTestId('errorElement')).toBeInTheDocument()
  })
  it('closes modal on clicking on close button', async () => {
    await openModal()
    const closeBtn = getByText(container, /close/i)
    fireEvent.click(closeBtn)
    expect(queryByTestId(container, 'modal')).toBeNull()
  })

  it('routes correctly on board click', async () => {
    const mockOnClick = jest.fn()
    await waitFor(() => getByText(container, /Create new board/i), {
      timeout: 2500
    })
    const btn = screen.getByTestId('5fff131db96bb72c457fe451')
    btn.addEventListener('click', mockOnClick)
    fireEvent.click(btn)
    setTimeout(() => {
      expect(mockOnClick).toHaveBeenCalledTimes(1)
    }, 1000)
  })
})
