import React, { useState } from 'react'
import './todo.css'
import { v4 as uuidv4 } from 'uuid'

export default function Todo() {
  const [inputValue, setInputValue] = useState('')
  const [currentTodos, setCurrentTodos] = useState([])
  const [currentEditInputId, setCurrentEditInputId] = useState(0)
  const [editInput, setEditInput] = useState('')

  const onKeyPress = (e) => {
    if (e.key === 'Enter') {
      setCurrentTodos((currentTodos) => [
        ...currentTodos,
        {
          todoText: e.target.value,
          completed: false,
          id: uuidv4()
        }
      ])
      setInputValue('')
    }
  }

  const handleEditInputKeyPress = (e, todoId) => {
    if (e.key === 'Enter') {
      const updatedTodos = currentTodos.map((todo) =>
        todoId === todo.id ? { ...todo, todoText: editInput } : todo
      )
      setCurrentTodos(updatedTodos)
      setCurrentEditInputId(-1)
    }
  }

  const toggleCheckedTodo = (todoId) => {
    const updatedTodos = currentTodos.map((todo) =>
      todo.id === todoId ? { ...todo, completed: !todo.completed } : todo
    )
    setCurrentTodos(updatedTodos)
  }

  const editTodo = (todoId) => {
    const currentlyEditingTodo = currentTodos.filter(
      (todo) => todo.id === todoId
    )
    setEditInput(currentlyEditingTodo[0].todoText)
    setCurrentEditInputId(todoId)
  }

  const deleteTodo = (todoId) => {
    const remainingTodos = currentTodos.filter((todo) => todo.id !== todoId)
    setCurrentTodos(remainingTodos)
  }
  const handleEditInput = (e) => {
    setEditInput(e.target.value)
  }
  return (
    <section className="todo-wrapper">
      <main className="container">
        <h1>
          To Do (<span data-testid="numberOfTodos">{currentTodos.length}</span>)
        </h1>
        <input
          type="text"
          className="todo-input"
          placeholder="Enter todo"
          data-testid="todo-input"
          value={inputValue}
          onChange={(e) => setInputValue(e.target.value)}
          onKeyPress={onKeyPress}
        />
        <div data-testid="allTodos">
          {currentTodos.map((todo) => (
            <div className="todo" key={todo.id}>
              <div>
                <input
                  type="checkbox"
                  className="todo-checkbox"
                  checked={todo.completed}
                  onChange={() => toggleCheckedTodo(todo.id)}
                />
                {currentEditInputId !== todo.id ? (
                  <div
                    className={`todo-text ${
                      todo.completed ? 'todo-completed' : ''
                    }`}
                  >
                    {todo.todoText}
                  </div>
                ) : (
                  <input
                    type="text"
                    className="todo-edit-input"
                    value={editInput}
                    data-testid="editInput"
                    onChange={handleEditInput}
                    onKeyPress={(e) => handleEditInputKeyPress(e, todo.id)}
                  />
                )}
              </div>
              <div className="todo-actions">
                <div onClick={() => editTodo(todo.id)}>Edit</div>
                <div onClick={() => deleteTodo(todo.id)}>Delete</div>
              </div>
            </div>
          ))}
        </div>
      </main>
    </section>
  )
}
