import React from 'react'
import {
  render,
  screen,
  fireEvent,
  getByTestId,
  getAllByText,
  queryByText,
  queryByTestId,
  getAllByRole,
  getByText
} from '@testing-library/react'
import renderer from 'react-test-renderer'
import Todo from './Todo'

let container = null
let value = null
let input = null

beforeEach(() => {
  container = render(<Todo />).container
  value = getByTestId(container, 'numberOfTodos')
  input = getByTestId(container, 'todo-input')
})

afterEach(() => {
  container = null
})

const addTodo = (todoText) => {
  fireEvent.change(input, { target: { value: todoText } })
  fireEvent.keyPress(input, { key: 'Enter', code: 13, charCode: 13 })
}

describe('tests Todo.js', () => {
  it('renders correctly', () => {
    const tree = renderer.create(<Todo />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('adds todo correctly', () => {
    addTodo('todo 1')
    expect(value.textContent).toBe('1')
  })

  it('updates the todo count on deletion', () => {
    addTodo('todo 1')
    addTodo('todo 2')
    const deleteButton = getAllByText(container, 'Delete')
    fireEvent.click(deleteButton[1])
    expect(value.textContent).toBe('1')
  })

  it('removes a todo from the list', () => {
    addTodo('todo 1')
    addTodo('todo 2')
    const deleteButton = getAllByText(container, 'Delete')
    fireEvent.click(deleteButton[1])
    const allTodos = getByTestId(container, 'allTodos')
    expect(allTodos.children.length).toBe(1)
  })

  it('deletes todo correctly', () => {
    addTodo('todo 1')
    addTodo('todo 2')
    const deleteButton = getAllByText(container, 'Delete')
    fireEvent.click(deleteButton[1])
    expect(queryByText(container, 'todo 2')).toBeNull()
  })

  it('should display todo as value in input on editing', () => {
    addTodo('todo 1')
    const editButton = getAllByText(container, 'Edit')
    fireEvent.click(editButton[0])
    const editInput = queryByTestId(container, 'editInput')
    expect(editInput.value).toBe('todo 1')
  })

  it('edit button works correctly', () => {
    addTodo('todo 1')
    const editButton = getAllByText(container, 'Edit')
    fireEvent.click(editButton[0])
    const editInput = queryByTestId(container, 'editInput')
    fireEvent.change(editInput, { target: { value: 'updated todo' } })
    fireEvent.keyPress(editInput, { key: 'Enter', code: 13, charCode: 13 })
    expect(queryByText(container, 'todo 1')).toBeNull()
    const updatedElement = screen.getByText(/updated todo/i)
    expect(updatedElement).toBeInTheDocument()
  })

  it('checkbox works correctly', () => {
    addTodo('todo 1')
    const checkboxElement = getAllByRole(container, 'checkbox')
    fireEvent.click(checkboxElement[0])
    const todoElement = getByText(container, 'todo 1')
    expect(todoElement).toHaveClass('todo-completed')
  })

  it('should change the className for only the checked todo', () => {
    addTodo('todo 1')
    addTodo('todo 2')
    const checkboxElement = getAllByRole(container, 'checkbox')
    fireEvent.click(checkboxElement[1])
    expect(container.getElementsByClassName('todo-completed').length).toBe(1)
  })

  it('should not add todo if any key other than enter is pressed', () => {
    fireEvent.change(input, { target: { value: 'test todo' } })
    fireEvent.keyPress(input, { key: 'q', code: 81, charCode: 81 })
    expect(value.textContent).toBe('0')
  })

  it('should render edit input as hidden initially', () => {
    const editInput = queryByTestId(container, 'editInput')
    expect(editInput).toBeNull()
  })

  it('should not update todo if any key other than enter is pressed', () => {
    addTodo('todo 1')
    const editButton = getAllByText(container, 'Edit')
    fireEvent.click(editButton[0])
    const editInput = queryByTestId(container, 'editInput')
    fireEvent.change(editInput, { target: { value: 'updated todo' } })
    fireEvent.keyPress(editInput, { key: 'q', code: 81, charCode: 81 })
    expect(value.textContent).toBe('1')
  })

  it('edit should not update other todos', () => {
    addTodo('todo 1')
    addTodo('todo 2')
    const editButton = getAllByText(container, 'Edit')
    fireEvent.click(editButton[0])
    const editInput = queryByTestId(container, 'editInput')
    fireEvent.change(editInput, { target: { value: 'updated todo' } })
    fireEvent.keyPress(editInput, { key: 'Enter', code: 13, charCode: 13 })
    expect(queryByText(container, 'todo 1')).toBeNull()
    const updatedElement = screen.getByText(/updated todo/i)
    expect(updatedElement).toBeInTheDocument()
    const todo2Element = screen.getByText(/todo 2/i)
    expect(todo2Element).toBeInTheDocument()
  })
})
