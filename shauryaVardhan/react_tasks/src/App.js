import React from 'react'
import './App.css'
import Router from './Task2_trello_clone/Router'

function App() {
  return (
    <div className="App">
      <Router />
    </div>
  )
}

export default App
