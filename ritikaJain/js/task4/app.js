const express = require('express')
const cors = require('cors')
const matchesPerYear = require('../task3/output/matchesPerYear.json')
const matchesWonPerTeamPerYear = require('../task3/output/matchesWonPerTeamPerYear.json')
const extraRunsPerTeam2016 = require('../task3/output/extraRunsPerTeam2016.json')
const app = express()
const port = 3000

app.use(cors())
app.use(express.static('public'))

app.get('/matchesPerYear', (req, res) => res.json(matchesPerYear))
app.get('/matchesWonPerTeamPerYear', (req, res) =>
  res.json(matchesWonPerTeamPerYear)
)
app.get('/extraRunsPerTeam2016', (req, res) => res.json(extraRunsPerTeam2016))

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const drawBarGraph = async () => {
  const resp = await fetch('http://localhost:3000/matchesPerYear')
  const data = await resp.json()

  const svg = d3.select('div#subtask1').append('svg')
  svg
    .attr('height', '500')
    .attr('width', '600')
    .attr('preserveAspectRatio', 'xMinYMin meet')

  const width = svg.attr('width') - 200
  const height = svg.attr('height') - 200

  const g = svg
    .append('g')
    .attr('transform', 'translate(' + 100 + ',' + 100 + ')')

  const xScale = d3.scaleBand().range([0, width]).padding(0.4)
  const yScale = d3.scaleLinear().range([height, 0])

  xScale.domain(Object.keys(data))
  yScale.domain([0, 100])

  g.append('g')
    .attr('transform', 'translate(0,' + height + ')')
    .call(d3.axisBottom(xScale))

  g.append('g').call(d3.axisLeft(yScale))

  g.selectAll('.bar')
    .data(Object.entries(data))
    .enter()
    .append('rect')
    .attr('fill', 'magenta')
    .attr('x', ([key, value]) => xScale(key))
    .attr('y', ([key, value]) => yScale(value))
    .attr('width', xScale.bandwidth())
    .attr('height', ([key, value]) => height - yScale(value))

  g.append('g')
    .attr('transform', 'translate(0,' + height + ')')
    .call(d3.axisBottom(xScale))
    .append('text')
    .attr('y', height - 250)
    .attr('x', width - 200)
    .attr('text-anchor', 'end')
    .attr('stroke', 'black')
    .text('Year')

  g.append('g')
    .call(d3.axisLeft(yScale))
    .append('text')
    .attr('transform', 'rotate(-90)')
    .attr('y', 6)
    .attr('dy', '-5.1em')
    .attr('text-anchor', 'end')
    .attr('stroke', 'black')
    .text('Matches Played')
}

const drawSubTask2 = async () => {
  const resp = await fetch('http://localhost:3000/matchesWonPerTeamPerYear')
  const data = await resp.json()
  const seriesData = Object.values(data).map((item) => Object.entries(item))
  const seriesLabel = Object.keys(data)

  const xLabels = Array.from({ length: 12 }, (_, i) => (i + 2008).toString())

  const chartOptions = {
    chart: {
      type: 'line'
    },
    title: {
      text: 'SubTask - 2'
    },
    subtitle: {
      text: 'Matches Won Per Year Per Team'
    },
    xAxis: {
      categories: xLabels,
      title: {
        text: 'Year'
      }
    },
    yAxis: [
      {
        index: 0,
        alignTicks: false,
        max: 15,
        min: 0,
        title: {
          text: 'matches won'
        },
        labels: {}
      }
    ],
    plotOptions: {
      line: {
        dataLabels: {
          enabled: true
        },
        enableMouseTracking: false
      }
    },
    series: seriesLabel.map((label, i) => {
      return {
        data: seriesData[i],
        turboThreshold: 0,
        type: 'line',
        name: label
      }
    })
  }

  Highcharts.chart('subtask2', chartOptions)
}

const drawSubTask3 = async () => {
  const resp = await fetch('http://localhost:3000/extraRunsPerTeam')
  const data = await resp.json()
  const seriesData = Object.entries(data)
  Highcharts.chart('subtask3', {
    chart: {
      type: 'bar'
    },
    title: {
      text: 'SubTask3'
    },
    subtitle: {
      text: 'Extra Runs conceded by every team in 2016'
    },
    xAxis: {
      categories: seriesData.map(([key, value]) => key)
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Extra Runs',
        align: 'high'
      },
      labels: {
        overflow: 'justify'
      }
    },
    tooltip: {
      valueSuffix: ' millions'
    },
    plotOptions: {
      bar: {
        dataLabels: {
          enabled: true
        }
      }
    },
    legend: {
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'top',
      x: -40,
      y: 80,
      floating: true,
      borderWidth: 1,
      backgroundColor:
        Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
      shadow: true
    },
    credits: {
      enabled: false
    },
    series: [
      {
        name: 'runs',
        data: seriesData.map(([key, value]) => value)
      }
    ]
  })
}

const downloadFile = async (filename) => {
  const resp = await fetch(`http://localhost:3000/${filename}`)
  const data = await resp.json()
  const element = document.createElement('a')
  const dataStr =
    'data:text/plain;charset=utf-8,' +
    encodeURIComponent(JSON.stringify(data, null, 2))
  element.setAttribute('href', dataStr)
  element.setAttribute('download', `${filename}.json`)
  element.style.display = 'none'
  document.body.appendChild(element)
  element.click()
  document.body.removeChild(element)
}
