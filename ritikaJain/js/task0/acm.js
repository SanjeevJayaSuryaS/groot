function totalSub(s1, s2) {
  let count = 0
  for (let i = 0; i < s1.length; i++) {
    count += parseInt(s1[i]) | parseInt(s2[i])
  }

  return count
}

function acm(topic) {
  if (!Array.isArray(topic)) throw new Error('Invalid')

  const arr = []
  let maxSub = 0
  let n = 0
  for (let i = 0; i < topic.length; i++) {
    for (let j = i + 1; j < topic.length; j++) {
      const b = totalSub(topic[i], topic[j])
      if (b > maxSub) maxSub = b
      arr.push(b)
    }
  }

  arr.forEach((i) => {
    if (i === maxSub) n++
  })

  return [maxSub, n]
}
module.exports = acm
