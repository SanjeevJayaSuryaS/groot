const { test, expect } = require('@jest/globals')
const search = require('./binarySearch')

test('case for element present', () => {
  expect(search([1, 2, 3], 3)).toBe(true)
})

test('case for element not present in the array', () => {
  expect(search([1, 2, 3], 5)).toBe(false)
})

test('for an invalid input', () => {
  expect(() => {
    search(null, 5)
  }).toThrowError('Invalid')
})

test('for invalid number to be searched', () => {
  expect(() => {
    search([1, 2, 3], 'cat')
  }).toThrowError('Invalid')
})

test('for left bound', () => {
  expect(search([1, 2, 3, 4], 1)).toBe(true)
})
