const fs = require('fs')
const axios = require('axios')

const { readFile, getUrl, writeFile, main } = require('./apiCall')

beforeEach(() => jest.restoreAllMocks())

describe('Spy on axios get', () => {
  test(`should return 12345`, () => {
    const spy = jest
      .spyOn(axios, 'get')
      .mockImplementation(() => Promise.resolve({ data: { message: '12345' } }))

    return getUrl('hound').then((data) => {
      expect(data).toBe('12345')
      expect(spy).toHaveBeenCalledTimes(1)
    })
  })

  test('should throw an error', () => {
    const spy = jest
      .spyOn(axios, 'get')
      // eslint-disable-next-line prefer-promise-reject-errors
      .mockImplementation(() => Promise.reject('error'))

    return getUrl('hound').catch((err) => {
      expect(err).toBe('error')
      expect(spy).toHaveBeenCalledTimes(1)
    })
  })
})

describe('Spy on fs write', () => {
  test('should accept if file generated', () => {
    const spy = jest
      .spyOn(fs, 'writeFile')
      .mockImplementation((_, __, callback) => {
        callback()
      })

    return writeFile('https://dog.ceo/api/breed/hound/images/random').then(
      () => {
        expect(spy).toHaveBeenCalledTimes(1)
      }
    )
  })

  test('should reject if file not generated', () => {
    jest.spyOn(fs, 'writeFile').mockImplementation((_, __, callback) => {
      callback(new Error('Something went wrong'))
    })

    return expect(() =>
      writeFile('https://dog.ceo/api/breed/hound/images/random')
    ).rejects.toMatchSnapshot()
  })
})

describe('Spy on fs Read', () => {
  test('should accept if file read', () => {
    const spy = jest
      .spyOn(fs, 'readFile')
      .mockImplementation((_, __, callback) => {
        callback()
      })

    return readFile().then(() => {
      expect(spy).toHaveBeenCalledTimes(1)
    })
  })

  test('should reject if file not read', () => {
    const spy = jest
      .spyOn(fs, 'readFile')
      .mockImplementation((_, __, callback) => {
        callback(new Error('something went wrong'))
      })

    return readFile().catch(() => {
      expect(spy).toHaveBeenCalledTimes(1)
    })
  })
})

describe('test for main', () => {
  test('should return true', () => {
    return main().then((result) => {
      expect(result).toBe(true)
    })
  })
})
