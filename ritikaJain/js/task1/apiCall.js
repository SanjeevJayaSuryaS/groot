const fs = require('fs')
const { default: axios } = require('axios')
const path = require('path')

function readFile() {
  return new Promise((resolve, reject) => {
    fs.readFile(
      path.join(__dirname, './fileToBeRead.txt'),
      'utf8',
      (err, data) => {
        if (err) reject(err)
        else resolve(data)
      }
    )
  })
}

function getUrl(breed) {
  return new Promise((resolve, reject) => {
    axios
      .get(`https://dog.ceo/api/breed/${breed}/images/random`)
      .then((res) => {
        const { message } = res.data
        resolve(message)
      })
      .catch((err) => {
        reject(err)
      })
  })
}

function writeFile(url) {
  return new Promise((resolve, reject) => {
    fs.writeFile('./fileWritten.txt', url, (err) => {
      if (err) reject(err)
      else resolve('File is written successfully')
    })
  })
}

function main() {
  return new Promise((resolve, reject) => {
    readFile()
      .then((breed) => getUrl(breed))
      .then((url) => writeFile(url))
      .then(() => {
        resolve(true)
      })
      .catch((err) => {
        reject(err)
      })
  })
}

module.exports = { readFile, getUrl, writeFile, main }
