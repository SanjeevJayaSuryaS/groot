const writeJsonOutput = require('./index')
const fs = require('fs/promises')
const path = require('path')

beforeEach(() => jest.restoreAllMocks())

describe('writeJsonOutput', () => {
  test('should generate 3 files', async () => {
    const spy = jest
      .spyOn(fs, 'writeFile')
      .mockImplementation(() => Promise.resolve(true))
    await writeJsonOutput()
    expect(spy).toHaveBeenCalledTimes(3)
  })

  test('writeJsonOutput on failure', async () => {
    const error = new Error('error')
    jest.spyOn(fs, 'writeFile').mockImplementation(async () => {
      throw error
    })
    await expect(writeJsonOutput()).rejects.toEqual(error)
  })
})

describe('test to check writeFile function of fs module', () => {
  test('should generate all three output files and write them down', async () => {
    await writeJsonOutput()

    expect(
      await fs.stat(path.join(__dirname, '../output/matchesPerYear.json'))
    ).toBeTruthy()

    expect(
      await fs.stat(
        path.join(__dirname, '../output/matchesWonPerTeamPerYear.json')
      )
    ).toBeTruthy()

    expect(
      await fs.stat(path.join(__dirname, '../output/extraRunsPerTeam2016.json'))
    ).toBeTruthy()
  })
})
