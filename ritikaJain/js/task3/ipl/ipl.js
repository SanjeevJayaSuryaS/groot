const path = require('path')
const csv = require('csvtojson')

const getJsonData = (filename) => {
  return csv().fromFile(path.join(__dirname, `../data/${filename}.csv`))
}

const matchesPlayedPerYear = async () => {
  const matches = await getJsonData('matches')
  return matches.reduce((result, current) => {
    result[current.season] = result[current.season] || 1
    result[current.season]++
    return result
  }, {})
}

const matchesWonPerTeamPerYear = async () => {
  const matches = await getJsonData('matches')
  return matches.reduce((result, { season, winner }) => {
    if (winner === 'Delhi Capitals' || winner === 'Delhi Daredevils') {
      result['Delhi Daredevils'] = result['Delhi Daredevils'] || {}
      result['Delhi Daredevils'][season] =
        result['Delhi Daredevils'][season] || 1
      result['Delhi Daredevils'][season]++
    } else {
      result[winner] = result[winner] || {}
      result[winner][season] = result[winner][season] || 1
      result[winner][season]++
    }
    return result
  }, {})
}

const extraRunsConcededPerTeamIn2016 = async () => {
  const matches = await getJsonData('matches')
  const deliveries = await getJsonData('deliveries')
  const matchesPlayedIn2016 = matches.filter((match) => match.season === '2016')
  const extraRuns = {}
  matchesPlayedIn2016.map((match) => {
    const id = match.id

    const deliveryWithId = deliveries.filter(
      (delivery) => delivery.match_id === id
    )

    deliveryWithId.reduce((extraRuns, current) => {
      extraRuns[current.bowling_team] = extraRuns[current.bowling_team] || 0
      extraRuns[current.bowling_team] += parseInt(current.extra_runs)
      return extraRuns
    }, extraRuns)

    return 0
  })

  return extraRuns
}

module.exports = {
  matchesPlayedPerYear,
  matchesWonPerTeamPerYear,
  extraRunsConcededPerTeamIn2016
}
