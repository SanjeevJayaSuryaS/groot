const extraRuns = require('../output/extraRunsPerTeam2016.json')
const matchesPlayed = require('../output/matchesPerYear.json')
const matchesWon = require('../output/matchesWonPerTeamPerYear.json')

const {
  matchesPlayedPerYear,
  matchesWonPerTeamPerYear,
  extraRunsConcededPerTeamIn2016
} = require('./ipl')

beforeEach(() => jest.restoreAllMocks())

describe('tests for matchesPlayedPerYear, matchesWonPerTeamPerYear and extraRunsConcededIn2016', () => {
  test('should generate correct result for matches played per year', async () => {
    const data = await matchesPlayedPerYear()
    expect(data).toEqual(matchesPlayed)
  })

  test('should generate correct result for matches won per year by each team', async () => {
    const data = await matchesWonPerTeamPerYear()
    expect(data).toEqual(matchesWon)
  })

  test('should generate correct result for extra runs by each team in 2016', async () => {
    const data = await extraRunsConcededPerTeamIn2016()
    expect(data).toEqual(extraRuns)
  })
})
