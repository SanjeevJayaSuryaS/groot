const fs = require('fs/promises')
const path = require('path')
const {
  matchesPlayedPerYear,
  matchesWonPerTeamPerYear,
  extraRunsConcededPerTeamIn2016
} = require('./ipl')

const writeJsonOutput = async () => {
  const matchesPlayed = await matchesPlayedPerYear()
  await fs.writeFile(
    path.join(__dirname, '../output/matchesPerYear.json'),
    JSON.stringify(matchesPlayed, undefined, 2)
  )

  const matchesWon = await matchesWonPerTeamPerYear()
  await fs.writeFile(
    path.join(__dirname, '../output/matchesWonPerTeamPerYear.json'),
    JSON.stringify(matchesWon, undefined, 2)
  )

  const extraRuns = await extraRunsConcededPerTeamIn2016()
  await fs.writeFile(
    path.join(__dirname, '../output/extraRunsPerTeam2016.json'),
    JSON.stringify(extraRuns, undefined, 2)
  )
}
module.exports = writeJsonOutput
