const fs = require('fs')
const axios = require('axios')
const path = require('path')

const readFile = () => {
  return new Promise((resolve, reject) => {
    fs.readFile(
      path.join(__dirname, './fileToBeRead.txt'),
      'utf8',
      (err, data) => {
        if (err) reject(err)
        else resolve(data)
      }
    )
  })
}

const writeFile = (url) => {
  return new Promise((resolve, reject) => {
    fs.writeFile('./fileWritten.txt', url, (err) => {
      if (err) reject(err)
      else resolve('File written successfully')
    })
  })
}

const getUrl = async (breed) => {
  try {
    const resp = await axios.get(
      `https://dog.ceo/api/breed/${breed}/images/random`
    )
    return resp.data.message
  } catch (error) {
    console.error(error.message)
  }
}

const getAllUrl = async (breed) => {
  try {
    // filled the array with 0 and then used map so that array is filled with 5 different promises
    const promiseArray = new Array(5).fill(0).map(() => getUrl(breed))
    const images = await Promise.all(promiseArray)
    return images
  } catch (error) {
    console.error(error.message)
  }
}

const main = async () => {
  const breedName = await readFile()
  const images = await getAllUrl(breedName)
  await writeFile(JSON.stringify(images, undefined, 2))
}
module.exports = { readFile, writeFile, getUrl, getAllUrl, main }
