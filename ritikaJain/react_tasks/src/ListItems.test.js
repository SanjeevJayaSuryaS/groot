import renderer from 'react-test-renderer'
import ListItems from './ListItems'
import React from 'react'

test('should render App correctly', () => {
  const tree = renderer
    .create(
      <ListItems
        items={[
          {
            text: 'random-todo',
            key: 'key'
          }
        ]}
      ></ListItems>
    )
    .toJSON()
  expect(tree).toMatchSnapshot()
})
