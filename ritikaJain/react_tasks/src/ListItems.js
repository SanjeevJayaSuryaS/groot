/* eslint-disable react/prop-types */
import React from 'react'
import './ListItems.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

function ListItems(props) {
  // eslint-disable-next-line react/prop-types
  const items = props.items
  const listItems = items.map((item) => {
    return (
      <div className="list" key={item.key}>
        <p>
          <input
            type="text"
            id={item.key}
            value={item.text}
            onChange={(e) => {
              props.updateItems(e.target.value, item.key)
            }}
          />
          <span>
            <FontAwesomeIcon
              className="faicons"
              icon="trash"
              data-testid="delete-button"
              onClick={() => props.deleteItem(item.key)}
            />
          </span>
        </p>
      </div>
    )
  })

  return <div>{listItems}</div>
}

export default ListItems
