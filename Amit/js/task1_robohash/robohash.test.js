const getImage = require('./robohash')
const fs = require('fs')

const fileExists = (file) => fs.existsSync(file)

// TODO: why we are not spying on endPoints
describe('robohash', () => {
  test('Image is successfully uploaded', async () => {
    const fileLocation = './'
    await getImage(0)
    await expect(fileExists(fileLocation + 'image0.png')).toBeTruthy()
  })
})
