const fs = require('fs')
const axios = require('axios')

const url = 'https://robohash.org/'

// Generating a random string
async function randomStringGenerator() {
  return new Promise((resolve, reject) => {
    resolve(Math.random().toString(36).substring(7))
  })
}

// writing data in the file
async function writeFilePromise(data, location, index) {
  return new Promise((resolve, reject) =>
    fs.writeFile(location + '/image' + index + '.png', data, () =>
      resolve(true)
    )
  )
}

// main function
async function getImage(index) {
  const randomString = await randomStringGenerator()

  const response = await axios.get(`${url}${randomString}`, {
    responseType: 'arraybuffer'
  })

  const writeFile = await writeFilePromise(response.data, './', index)

  return writeFile
}

getImage(0)
module.exports = getImage
