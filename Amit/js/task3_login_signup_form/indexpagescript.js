/* istanbul ignore file */
/* eslint-disable no-unused-vars */

// To redirect to register page
function redirectToRegister() {
  window.location = 'secondpage.html'
}

// regex for email
function emailRegexValidation(email) {
  const regexEmail = /([a-z\d.-]+)@([a-z\d-]+)\.([a-z]{2,8})/
  return regexEmail.test(email)
}

// email validation and inside this used regex function for email
const emailValidation = () => {
  const email = document.login.email.value
  const emailMsg = document.getElementById('email__msg')

  if (email.length > 0) {
    if (emailRegexValidation(email)) emailMsg.innerHTML = ''
    else emailMsg.innerHTML = '**Invalid Email'
  } else {
    emailMsg.innerHTML = '**Email required'
  }
}

// password validation
const passwordValidation = () => {
  const password = document.login.password.value
  const passwordMsg = document.getElementById('password__msg')

  if (password.length > 0) {
    if (password.length < 6)
      passwordMsg.innerHTML = '**Password must 6 characters long'
    else passwordMsg.innerHTML = ''
  } else {
    passwordMsg.innerHTML = '**Password required'
  }
}

// validate login page on click of login button
function validateLoginPage() {
  const email = document.login.email.value
  const emailMsg = document.getElementById('email__msg')
  const password = document.login.password.value
  const passwordMsg = document.getElementById('password__msg')

  if (!email) emailMsg.innerHTML = '**Email required'

  if (!password) passwordMsg.innerHTML = '**Password required'

  if (email && password) {
    alert('Successfully logged in')
  }
}
