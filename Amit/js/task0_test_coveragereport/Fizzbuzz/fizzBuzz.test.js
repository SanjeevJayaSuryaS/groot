const fizzBuzz = require('./fizzBuzz')

describe('FizzBuzz', () => {
  test('Input is not a number, Invalid Input', () => {
    const result = () => fizzBuzz('Amit')
    expect(result).toThrow('Invalid Input')
  })
  test('Input number is less than 1, Invalid Input', () => {
    const result = () => fizzBuzz(-1)
    expect(result).toThrow('Invalid Input')
  })
  test('Input number is more than 100, Invalid Input', () => {
    const result = () => fizzBuzz(105)
    expect(result).toThrow('Invalid Input')
  })
  test('Input is multiple of 3 and 5, so output should be FizzBuzz', () => {
    const result = fizzBuzz(15)
    expect(result).toEqual('FizzBuzz')
  })

  test('Input is multiple of 5, so output Buzz', () => {
    const result = fizzBuzz(5)
    expect(result).toEqual('Buzz')
  })
  test('Input is multiple of 3, so output Fizz', () => {
    const result = fizzBuzz(3)
    expect(result).toEqual('Fizz')
  })
  test('Input is number not from 3 and 5, so output is the given number', () => {
    const result = fizzBuzz(4)
    expect(result).toEqual(4)
  })
})
