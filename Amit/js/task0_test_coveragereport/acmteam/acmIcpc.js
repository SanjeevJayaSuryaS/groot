function acmTeam(numberofstudent, numberofsubject, topic) {
  if (numberofstudent < 2 || numberofsubject < 1 || topic.length === 0) {
    throw new Error('Invalid Input')
  }
  const length = topic.length
  let iter, j, k
  let cnt
  let maxi = -1
  let numberofgroup = 0
  for (iter = 0; iter < length - 1; iter++) {
    for (j = iter + 1; j < length; j++) {
      cnt = 0
      for (k = 0; k < topic[0].length; k++) {
        if (topic[iter][k] === '1' || topic[j][k] === '1') cnt = cnt + 1
      }
      if (cnt > maxi) {
        numberofgroup = 1
        maxi = cnt
      } else if (cnt === maxi) {
        numberofgroup++
      }
    }
  }
  return [maxi, numberofgroup]
}

module.exports = acmTeam
