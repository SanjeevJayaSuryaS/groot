const acmTeam = require('./acmIcpc')

describe('acmIcpc', () => {
  test('expected answer to be [5,2]', () => {
    const result = acmTeam(4, 5, ['10101', '11100', '11010', '00101'])
    const expected = [5, 2]
    expect(result).toEqual(expected)
  })

  test('number of student should be greater than 1, Invalid Input', () => {
    const result = () => acmTeam(1, 5, ['10101', '11100', '11010', '00101'])
    expect(result).toThrow('Invalid Input')
  })

  test('number of subject should be greater than 0, Invalid Input', () => {
    const result = () => acmTeam(4, 0, ['1010', '100101', '11010', '1001'])
    expect(result).toThrow('Invalid Input')
  })

  test('if two students know same subject the count should be one', () => {
    const result = acmTeam(2, 2, ['11', '11'])
    expect(result).toEqual([2, 1])
  })

  test('if either of two students know one subject count should be one', () => {
    const result = acmTeam(2, 2, ['10', '01'])
    expect(result).toEqual([2, 1])
  })

  test('checking the condition cnt>max', () => {
    const result = acmTeam(3, 5, ['10101', '10101', '11101'])
    expect(result).toEqual([4, 2])
  })
})
