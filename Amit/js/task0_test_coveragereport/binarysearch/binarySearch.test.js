const binarySearch = require('./binarySearch')

describe('binarySearch', () => {
  test('function return Invalid Input, array is undefined', () => {
    const result = () => binarySearch(0, 5, 1, undefined)
    expect(result).toThrow('Invalid Input')
  })
  test('function return Invalid Input, because string is searched', () => {
    const result = () => binarySearch(0, 5, 'amit', [1, 3, 5, 7, 9, 10])
    expect(result).toThrow('Invalid Input')
  })
  test('function should return index 2, taking case of middle element', () => {
    const result = binarySearch(0, 6, 8, [1, 3, 7, 8, 9, 10, 14])
    expect(result).toBe(3)
  })
  test('function should return index 0, searching left hand side of middle element', () => {
    const result = binarySearch(0, 4, 5, [5, 8, 10, 40, 50])
    expect(result).toBe(0)
  })
  test('function should return index 4, searching right hand side of middle element', () => {
    const result = binarySearch(0, 4, 5, [1, 2, 3, 4, 5])
    expect(result).toBe(4)
  })
  test('function should return index -1, search number not found in array', () => {
    const result = binarySearch(0, 5, 6, [7, 27, 37, 47, 57, 88])
    expect(result).toBe(-1)
  })
})
