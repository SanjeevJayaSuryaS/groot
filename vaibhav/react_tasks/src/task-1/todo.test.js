import React from 'react'
import { render, screen } from '@testing-library/react'

import userEvent from '@testing-library/user-event'

import Todo from './todo'

describe('Todo App', () => {
  test('Renders Todo App', () => {
    render(<Todo />)

    expect(screen.getByRole('textbox')).toBeInTheDocument()

    expect(
      screen.getByRole('button', { name: /Add Task/i })
    ).toBeInTheDocument()
  })

  test('Task should get added', () => {
    render(<Todo />)

    userEvent.type(screen.getByRole('textbox'), 'Task 1')
    userEvent.click(screen.getByRole('button', { name: /Add Task/i }))

    expect(screen.getByTestId('task-text')).toHaveTextContent('Task 1')
  })

  test('Added Task should get deleted', () => {
    render(<Todo />)

    userEvent.type(screen.getByRole('textbox'), 'Task 1')
    userEvent.click(screen.getByRole('button', { name: /Add Task/i }))

    userEvent.click(screen.getByRole('button', { name: /Done/i }))

    expect(screen.queryByTestId('task-text')).toBeNull()
  })
})
