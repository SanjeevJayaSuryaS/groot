import React from 'react'
import './todo.css'
import TaskAdder from './components/taskAdder'
import Task from './components/task'

export default class Todo extends React.Component {
  state = {
    tasks: []
  }

  addTask = (task) => {
    this.setState({ tasks: [...this.state.tasks, task] })
  }

  deleteTask = (taskIndex) => {
    const newTasks = this.state.tasks.filter((value, index) => {
      return index !== taskIndex
    })

    this.setState({ tasks: newTasks })
  }

  render() {
    return (
      <div className="todo">
        <h1>Todo App</h1>

        <TaskAdder addTask={this.addTask} />

        <div className="tasks">
          <ol>
            {this.state.tasks.map((task, index) => (
              <li
                key={index}
                onClick={() => {
                  this.deleteTask(index)
                }}
              >
                {<Task value={task} deleteTask={this.deleteTask} id={index} />}
              </li>
            ))}
          </ol>
        </div>
      </div>
    )
  }
}
