import React from 'react'
import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import renderer from 'react-test-renderer'

import TaskAdder from './taskAdder'

describe('TaskAdder Component', () => {
  test('Should render according to snapshot', () => {
    expect(renderer.create(<TaskAdder />).toJSON()).toMatchSnapshot()
  })

  test('Should call function on button click', () => {
    const addTask = jest.fn()
    render(<TaskAdder addTask={addTask} />)
    userEvent.click(screen.getByRole('button', { name: /Add Task/i }))
    expect(addTask).toHaveBeenCalled()
  })
})
