import React from 'react'
import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import renderer from 'react-test-renderer'

import Task from './task'

describe('Task Component', () => {
  test('Should render according to snapshot', () => {
    expect(renderer.create(<Task />).toJSON()).toMatchSnapshot()
  })

  test('Should call function on button click', () => {
    const deleteTask = jest.fn()

    render(<Task deleteTask={deleteTask} />)

    userEvent.click(screen.getByRole('button', { name: /Done/i }))

    expect(deleteTask).toHaveBeenCalled()
  })
})
