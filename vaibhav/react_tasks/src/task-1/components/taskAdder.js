import React from 'react'
import PropTypes from 'prop-types'

export default class TaskAdder extends React.Component {
  state = {
    input: ''
  }

  handleSubmit = (event) => {
    event.preventDefault()
    this.props.addTask(this.state.input)
    this.setState({ input: '' })
  }

  handleOnChange = (event) => this.setState({ input: event.target.value })

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input
          type="text"
          onChange={this.handleOnChange}
          value={this.state.input}
        />
        <input type="submit" value="Add Task" />
      </form>
    )
  }
}

TaskAdder.propTypes = {
  addTask: PropTypes.func
}
