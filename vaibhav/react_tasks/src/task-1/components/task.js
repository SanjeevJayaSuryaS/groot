import React from 'react'
import PropTypes from 'prop-types'

export default function Task(props) {
  const handleDone = () => {
    props.deleteTask(props.id)
  }

  return (
    <div className="task">
      <span className="task-text" data-testid="task-text">
        {props.value}
      </span>
      <button onClick={handleDone}>Done</button>
    </div>
  )
}

Task.propTypes = {
  deleteTask: PropTypes.func,
  value: PropTypes.string,
  id: PropTypes.number
}
