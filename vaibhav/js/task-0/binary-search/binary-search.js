function isArrayOnlyNumbers(arr) {
  let res = true

  arr.forEach((element) => {
    if (typeof element !== 'number') res = false
  })

  return res
}

/**
 * Perform binary search on an array
 * @param {number[]} arr
 * @param {number} elm
 */
function binarySearch(arr, elm) {
  if (!Array.isArray(arr)) throw new Error('Invalid Input')

  if (arr.length < 1) throw new Error('Invalid Input')

  if (!isArrayOnlyNumbers(arr)) throw new Error('Invalid Input')

  if (JSON.stringify([...arr].sort()) !== JSON.stringify(arr))
    throw new Error('Invalid Input')

  let high = arr.length - 1
  let low = 0
  let mid = Math.floor(low + (high - low) / 2)

  while (high >= low) {
    if (arr[mid] > elm) {
      high = mid - 1
    } else if (arr[mid] < elm) {
      low = mid + 1
    } else {
      return mid
    }

    mid = Math.floor(low + (high - low) / 2)
  }

  return -1
}

module.exports = binarySearch
