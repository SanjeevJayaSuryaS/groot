/**
 * @param {number} n
 * @return {[]}
 */
const fizzBuzz = function (n) {
  if (typeof n !== 'number' || n <= 0) throw new Error('Invalid Input')

  const arr = []

  for (let i = 1; i <= n; i++) {
    if (i % 3 === 0 && i % 5 === 0) {
      arr.push('FizzBuzz')
    } else if (i % 3 === 0) {
      arr.push('Fizz')
    } else if (i % 5 === 0) {
      arr.push('Buzz')
    } else {
      arr.push(i.toString())
    }
  }

  return arr
}

module.exports = fizzBuzz
