import Highcharts from 'highcharts'
import numberOfMatchesWonPerTeamPerYear from '../task-3/output/numberOfMatchesWonPerTeamPerYear.json'
import theme from 'highcharts/themes/high-contrast-dark'
theme(Highcharts)

export const renderNumberOfMatchesWonPerTeamPerYear = (id) => {
  // Create an array [2008, 2009 ... 2019]
  const years = [...Array(2019 - 2008 + 1).keys()].map(
    (_, index) => 2008 + index
  )

  Highcharts.chart(id, {
    title: {
      text: 'Number of matches won in IPL'
    },

    yAxis: {
      title: {
        text: 'Number of matches won'
      }
    },

    xAxis: {
      accessibility: {
        rangeDescription: 'Range: 2008 to 2019'
      }
    },

    legend: {
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle'
    },

    plotOptions: {
      series: {
        label: {
          connectorAllowed: false
        },
        pointStart: 2008
      }
    },

    /** Series contains an array of the following format
     * {
     *   name: 'Sunrisers Hyderabad',
     *   data: [null, null, null, null, null, 10, 6, 7, 11, 8, 10, 6]
     * }
     */
    series: Object.keys(numberOfMatchesWonPerTeamPerYear).map((teamName) => {
      return {
        name: teamName,
        data: years.map((year) =>
          numberOfMatchesWonPerTeamPerYear[teamName][year]
            ? numberOfMatchesWonPerTeamPerYear[teamName][year]
            : null
        )
      }
    }),

    responsive: {
      rules: [
        {
          condition: {
            maxWidth: 500
          },
          chartOptions: {
            legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom'
            }
          }
        }
      ]
    },

    chart: {
      backgroundColor: '#191414'
    }
  })
}
