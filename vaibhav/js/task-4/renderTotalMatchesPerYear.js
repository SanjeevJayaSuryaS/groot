import * as d3 from 'd3'
import totalMatchesPerYear from '../task-3/output/totalMatchesPerYear.json'

export const renderTotalMatchesPerYear = () => {
  const color = '#1DB954'
  const height = 350
  const width = 350
  const margin = { top: 30, right: 0, bottom: 30, left: 40 }

  const data = Object.assign(
    Object.keys(totalMatchesPerYear).map((key) => {
      return {
        name: key,
        value: totalMatchesPerYear[key]
      }
    }),
    { y: '↑ Number of matches played' },
    { x: '→ Years' }
  )

  const x = d3
    .scaleBand()
    .domain(d3.range(data.length))
    .range([margin.left, width - margin.right])
    .padding(0.1)

  const y = d3
    .scaleLinear()
    .domain([0, d3.max(data, (d) => d.value)])
    .nice()
    .range([height - margin.bottom, margin.top])

  const xAxis = (g) =>
    g.attr('transform', `translate(0,${height - margin.bottom})`).call(
      d3
        .axisBottom(x)
        .tickFormat((i) => data[i].name)
        .tickSizeOuter(0)
    )

  const yAxis = (g) =>
    g
      .attr('transform', `translate(${margin.left},0)`)
      .call(d3.axisLeft(y).ticks(null, data.format))
      .call((g) => g.select('.domain').remove())
      .call((g) =>
        g
          .append('text')
          .attr('x', width - 50)
          .attr('y', 10)
          .attr('fill', 'currentColor')
          .attr('text-anchor', 'end')
          .text(data.y)
      )
      .call((g) =>
        g
          .append('text')
          .attr('x', width - 50)
          .attr('y', 20)
          .attr('fill', 'currentColor')
          .attr('text-anchor', 'end')
          .text(data.x)
      )

  const svg = d3.create('svg').attr('viewBox', [0, 0, width, height])

  svg
    .append('g')
    .attr('fill', color)
    .selectAll('rect')
    .data(data)
    .join('rect')
    .attr('x', (d, i) => x(i))
    .attr('y', (d) => y(d.value))
    .attr('height', (d) => y(0) - y(d.value))
    .attr('width', x.bandwidth())

  svg.append('g').call(xAxis)

  svg.append('g').call(yAxis)

  return svg.node()
}
