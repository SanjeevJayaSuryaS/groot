import { renderTotalMatchesPerYear } from './task-4/renderTotalMatchesPerYear'
import { renderNumberOfMatchesWonPerTeamPerYear } from './task-4/renderNumberOfMatchesWonPerTeamPerYear'
import { renderExtraRunPerTeamIn2016 } from './task-4/renderExtraRunPerTeamIn2016'

import totalMatchesPerYear from './task-3/output/totalMatchesPerYear.json'
import numberOfMatchesWonPerTeamPerYear from './task-3/output/numberOfMatchesWonPerTeamPerYear.json'
import extraRunPerTeamIn2016 from './task-3/output/extraRunPerTeamIn2016.json'

import './index.css'

const clear = () => {
  const parent = document.getElementById('container')
  while (parent.firstChild) {
    parent.firstChild.remove()
  }

  const list = document.getElementsByClassName('selected')
  Array.from(list).map((item) => item.setAttribute('class', ''))
}

const setDownload = (content, filename) => {
  const elm = document.getElementById('download')

  elm.setAttribute(
    'href',
    'data:text/plain;charset=utf-8,' + encodeURIComponent(content)
  )

  elm.setAttribute('download', filename)
}

const task1 = () => {
  clear()
  document
    .getElementById('container')
    .appendChild(renderTotalMatchesPerYear())
    .setAttribute('class', 'd3')
  document.getElementById('btn-1').setAttribute('class', 'selected')

  setDownload(JSON.stringify(totalMatchesPerYear), 'totalMatchesPerYear.json')
}

const task3 = () => {
  clear()
  document
    .getElementById('container')
    .appendChild(renderExtraRunPerTeamIn2016())
    .setAttribute('class', 'd3')
  document.getElementById('btn-3').setAttribute('class', 'selected')

  setDownload(
    JSON.stringify(extraRunPerTeamIn2016),
    'extraRunPerTeamIn2016.json'
  )
}

const task2 = () => {
  clear()

  renderNumberOfMatchesWonPerTeamPerYear('container')

  document.getElementById('btn-2').setAttribute('class', 'selected')

  setDownload(
    JSON.stringify(numberOfMatchesWonPerTeamPerYear),
    'numberOfMatchesWonPerTeamPerYear.json'
  )
}

window.task1 = task1
window.task2 = task2
window.task3 = task3

window.addEventListener('DOMContentLoaded', () => {
  task1()
})
