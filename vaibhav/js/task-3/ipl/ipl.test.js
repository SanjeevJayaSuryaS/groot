const ipl = require('./ipl')
const totalMatchesPerYear = require('../expected_output/totalMatchesPerYear.json')
const numberOfMatchesWonPerTeamPerYear = require('../expected_output/numberOfMatchesWonPerTeamPerYear.json')
const extraRunPerTeamIn2016 = require('../expected_output/extraRunPerTeamIn2016.json')

describe('Task 4 - Analyze data IPL data', () => {
  test('Total Matches Per Year: Should return correct data', async () => {
    const result = await ipl.totalMatchesPerYear()

    expect(result).toEqual(totalMatchesPerYear)
  })

  test('Number of matches won per team per year: Should return correct data', async () => {
    const result = await ipl.numberOfMatchesWonPerTeamPerYear()

    expect(result).toEqual(numberOfMatchesWonPerTeamPerYear)
  })

  test('Extra runs per team in 2016: Should return correct data', async () => {
    const result = await ipl.extraRunPerTeamIn2016()

    expect(result).toEqual(extraRunPerTeamIn2016)
  })
})
