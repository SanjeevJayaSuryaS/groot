const fs = require('fs/promises')
const path = require('path')

const {
  totalMatchesPerYear,
  numberOfMatchesWonPerTeamPerYear,
  extraRunPerTeamIn2016
} = require('./ipl')

const writeFile = async (fileName, data) => {
  try {
    await fs.writeFile(fileName, data)
  } catch (err) {
    throw new Error('Cannot write file')
  }
}

const writeTotalMatchesPerYear = async () => {
  const result = await totalMatchesPerYear()

  await writeFile(
    path.join(__dirname, '../output/totalMatchesPerYear.json'),
    JSON.stringify(result, null, 2)
  )
}

const writeNumberOfMatchesWonPerTeamPerYear = async () => {
  const result = await numberOfMatchesWonPerTeamPerYear()

  await writeFile(
    path.join(__dirname, '../output/numberOfMatchesWonPerTeamPerYear.json'),
    JSON.stringify(result)
  )
}

const writeExtraRunPerTeamIn2016 = async () => {
  const result = await extraRunPerTeamIn2016()

  await writeFile(
    path.join(__dirname, '../output/extraRunPerTeamIn2016.json'),
    JSON.stringify(result)
  )
}

// writeTotalMatchesPerYear()
// writeNumberOfMatchesWonPerTeamPerYear()
// writeExtraRunPerTeamIn2016()

module.exports = {
  writeTotalMatchesPerYear,
  writeNumberOfMatchesWonPerTeamPerYear,
  writeExtraRunPerTeamIn2016
}
