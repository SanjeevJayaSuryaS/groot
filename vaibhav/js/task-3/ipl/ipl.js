const csv = require('csvtojson')
const path = require('path')

const getMatchesAsJSON = async () => {
  const json = await csv().fromFile(path.join(__dirname, '../data/matches.csv'))

  json.map((match) => {
    match.team1 =
      match.team1 === 'Delhi Daredevils' ? 'Delhi Capitals' : match.team1

    match.team2 =
      match.team2 === 'Delhi Daredevils' ? 'Delhi Capitals' : match.team2

    match.winner =
      match.winner === 'Delhi Daredevils' ? 'Delhi Capitals' : match.winner

    return match
  })

  return json
}

const getDeliveriesAsJSON = async () => {
  const json = await csv().fromFile(
    path.join(__dirname, '../data/deliveries.csv')
  )

  json.map((delivery) => {
    delivery.batting_team =
      delivery.batting_team === 'Delhi Daredevils'
        ? 'Delhi Capitals'
        : delivery.batting_team

    delivery.bowling_team =
      delivery.bowling_team === 'Delhi Daredevils'
        ? 'Delhi Capitals'
        : delivery.bowling_team

    return delivery
  })

  return json
}

const totalMatchesPerYear = async () => {
  const matches = await getMatchesAsJSON()

  const result = matches.reduce((obj, match) => {
    obj[match.season] = obj[match.season] ? obj[match.season] + 1 : 1

    return obj
  }, {})

  return result
}

const numberOfMatchesWonPerTeamPerYear = async () => {
  const matches = await getMatchesAsJSON()

  const result = matches.reduce((obj, match) => {
    if (match.winner !== '') {
      if (obj[match.winner]) {
        obj[match.winner][match.season] = obj[match.winner][match.season]
          ? obj[match.winner][match.season] + 1
          : 1
      } else {
        obj[match.winner] = {}
        obj[match.winner][match.season] = 1
      }
    }

    return obj
  }, {})

  return result
}

const extraRunPerTeamIn2016 = async () => {
  const matches = await getMatchesAsJSON()
  const deliveries = await getDeliveriesAsJSON()

  const matchIdsForYear2016 = matches.reduce((accumulator, match) => {
    if (match.season === '2016') {
      accumulator.push(match.id)
    }

    return accumulator
  }, [])

  const result = deliveries.reduce((obj, delivery) => {
    if (matchIdsForYear2016.includes(delivery.match_id)) {
      if (obj[delivery.batting_team]) {
        obj[delivery.batting_team] += parseInt(delivery.extra_runs)
      } else {
        obj[delivery.batting_team] = parseInt(delivery.extra_runs)
      }
    }

    return obj
  }, {})

  return result
}

module.exports = {
  totalMatchesPerYear,
  numberOfMatchesWonPerTeamPerYear,
  extraRunPerTeamIn2016
}
