const {
  writeTotalMatchesPerYear,
  writeNumberOfMatchesWonPerTeamPerYear,
  writeExtraRunPerTeamIn2016
} = require('./index')

const fs = require('fs/promises')

jest.mock('fs/promises')

describe('Task 4 - Test cases for index.js', () => {
  test('fs.writeFile should be called 3 times', async () => {
    const spy = jest.spyOn(fs, 'writeFile')

    await writeTotalMatchesPerYear()

    await writeNumberOfMatchesWonPerTeamPerYear()

    await writeExtraRunPerTeamIn2016()

    expect(spy).toHaveBeenCalledTimes(3)
  })

  test('fs.writeFile should fail with an error', async () => {
    fs.writeFile.mockImplementation(async () => {
      throw new Error('Cannot write file')
    })

    return expect(writeTotalMatchesPerYear).rejects.toEqual(
      new Error('Cannot write file')
    )
  })
})
