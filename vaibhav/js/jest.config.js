const globalConfig = require('../../jest.config')

globalConfig.coveragePathIgnorePatterns = ["task-4", "main.js", "webpack.config.js"]

module.exports = globalConfig