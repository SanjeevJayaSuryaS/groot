const fs = require('fs')
const axios = require('axios')

const readFile = (fileName) => {
  return new Promise((resolve, reject) => {
    fs.readFile(fileName, (err, data) => {
      if (err) {
        return reject(new Error('Error in reading file'))
      }

      if (data.length < 1) {
        return reject(new Error('Text file is empty'))
      }

      resolve(data.toString())
    })
  })
}

const writeFile = (url) => {
  return new Promise((resolve, reject) => {
    fs.writeFile('./image.txt', url, (err) => {
      if (err) return reject(new Error('Error in writing file'))

      resolve('Success')
    })
  })
}

const fetchUsingAxios = (breedName) => {
  return new Promise((resolve, reject) => {
    axios
      .get(`https://dog.ceo/api/breed/${breedName}/images`)
      .then((apiData) => resolve(apiData))
      .catch(() => reject(new Error('Cannot fetch from API')))
  })
}

const getAndSaveImage = (fileName) => {
  return new Promise((resolve, reject) => {
    readFile(fileName)
      .then((breedName) => fetchUsingAxios(breedName))
      .then((apiData) => {
        return writeFile(apiData.data.message[0])
      })
      .then((successMessage) => {
        resolve(successMessage)
      })
      .catch((err) => {
        reject(err)
      })
  })
}

/**
 * How to use the above function
 */
// getAndSaveImage('breed.txt')
//   .then((data) => {
//     console.log(data)
//   })
//   .catch((err) => {
//     console.log('Error occurred', err)
//   })

module.exports = getAndSaveImage
