const fs = require('fs')
const getAndSaveImage = require('./getAndSaveImage')
const axios = require('axios')

jest.mock('axios')
jest.mock('fs')

describe('Test cases for Fetching and Saving Dog Images', () => {
  const SUCCESS = 'Success'
  const BREED_NAME = 'hound'
  const BREED_FILE_NAME = 'breed.txt'
  const SUCCESS_RESP = {
    data: {
      message: [
        'https://images.dog.ceo/breeds/hound-afghan/n02088094_3119.jpg'
      ],
      status: 'success'
    }
  }

  // Mock axios and fs to return success
  beforeEach(() => {
    axios.get.mockResolvedValue(SUCCESS_RESP)

    fs.readFile.mockImplementation((_, cb) => {
      cb(undefined, BREED_NAME)
    })

    fs.writeFile.mockImplementation((_, __, cb) => {
      cb()
    })
  })

  test('Should return Success', () => {
    return getAndSaveImage(BREED_FILE_NAME).then((data) => {
      expect(data).toBe(SUCCESS)
    })
  })

  describe('Should throw error', () => {
    test('When API is down', () => {
      axios.get.mockImplementationOnce(() =>
        Promise.reject(new Error('Cannot fetch from API'))
      )

      expect.assertions(1)
      return getAndSaveImage(BREED_FILE_NAME).catch((err) => {
        expect(err.toString()).toEqual('Error: Cannot fetch from API')
      })
    })

    test('When filename is invalid', () => {
      const INVALID_FILE_NAME = 'random.txt'

      fs.readFile.mockImplementationOnce((_, cb) => {
        cb(new Error())
      })

      expect.assertions(1)
      return getAndSaveImage(INVALID_FILE_NAME).catch((err) => {
        expect(err.toString()).toEqual('Error: Error in reading file')
      })
    })

    test('When writeFile fails', () => {
      fs.writeFile.mockImplementationOnce((_, __, cb) => {
        cb(new Error())
      })

      expect.assertions(1)
      return getAndSaveImage(BREED_FILE_NAME).catch((err) => {
        expect(err.toString()).toEqual('Error: Error in writing file')
      })
    })

    test('When text file is empty', () => {
      fs.readFile.mockImplementationOnce((_, cb) => {
        cb(undefined, '')
      })

      expect.assertions(1)
      return getAndSaveImage(BREED_FILE_NAME).catch((err) => {
        expect(err.toString()).toEqual('Error: Text file is empty')
      })
    })
  })
})
