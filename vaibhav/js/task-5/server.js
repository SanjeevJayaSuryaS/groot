/* istanbul ignore file */

import express from 'express'

const app = express()
app.set('view engine', 'ejs')

app.use(express.static('public'))

app.get('/signout', (req, res) => {
  res.render('signout')
})

app.get('/', (req, res) => {
  res.render('index')
})

app.listen(8080, () => {
  console.log('Server started')
})
