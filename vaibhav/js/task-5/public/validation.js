/*
  istanbul ignore file
*/

/* eslint-disable */

function submit() {
  const style =
    'color: #d23d3d; border: 0.1rem solid rgb(173, 47, 47); background: url(error.svg) no-repeat scroll 97% 50%; padding-left: 1.5rem; padding-right: 2rem;'

  validateFirstName(style)
  validateLastName(style)
  validateEmail(style)
  validatePassword(style)
}

function validateFirstName(style) {
  const firstName = document.getElementById('firstName')
  const firstName_error = document.getElementById('firstName-error')

  if (firstName.value === '') {
    firstName.setAttribute('style', style)
    firstName_error.setAttribute('class', 'error-show')
  } else {
    firstName.setAttribute('style', '')
    firstName_error.setAttribute('class', 'error-hide')
  }
}

function validateLastName(style) {
  const lastName = document.getElementById('lastName')
  const lastName_error = document.getElementById('lastName-error')

  if (lastName.value === '') {
    lastName.setAttribute('style', style)
    lastName_error.setAttribute('class', 'error-show')
  } else {
    lastName.setAttribute('style', '')
    lastName_error.setAttribute('class', 'error-hide')
  }
}

function checkEmailAgainstRegExp(email) {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(String(email).toLowerCase())
}

function validateEmail(style) {
  const email = document.getElementById('email')
  const email_error = document.getElementById('email-error')

  if (!checkEmailAgainstRegExp(email.value)) {
    if (email.value === '') {
      email_error.innerText = "Email can't be empty"
    } else {
      email_error.innerText = 'Looks like this is not an Email'
    }

    email.setAttribute('style', style)
    email_error.setAttribute('class', 'error-show')
  } else {
    email.setAttribute('style', '')
    email_error.setAttribute('class', 'error-hide')
  }
}

function validatePassword(style) {
  const pwd = document.getElementById('pwd')
  const pwd_error = document.getElementById('pwd-error')

  if (pwd.value === '') {
    pwd.setAttribute('style', style)
    pwd_error.setAttribute('class', 'error-show')
  } else {
    pwd.setAttribute('style', '')
    pwd_error.setAttribute('class', 'error-hide')
  }
}
