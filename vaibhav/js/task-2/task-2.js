const axios = require('axios')
const fs = require('fs/promises')

const getAndSaveImage = async (fileName) => {
  let breedName

  try {
    breedName = (await fs.readFile(fileName)).toString()
    if (breedName.length < 1) throw new Error('Invalid breed name')
  } catch (err) {
    throw new Error('Cannot read file')
  }

  const url = `https://dog.ceo/api/breed/${breedName}/images`

  let responses, dataToBeSaved
  const axiosRequests = new Array(5)

  try {
    responses = await Promise.all(axiosRequests.fill(axios.get(url)))

    dataToBeSaved = responses.reduce(
      (accumulator, currentValue) =>
        (accumulator += currentValue.data.message[0] + '\n'),
      '' // Initial Value
    )
  } catch (err) {
    throw new Error('Cannot fetch data')
  }

  try {
    await fs.writeFile('./images.txt', dataToBeSaved)
  } catch (err) {
    throw new Error('Cannot write file')
  }

  return 'Success'
}

// Test the code using this
// getAndSaveImage('./breed.txt')

module.exports = getAndSaveImage
