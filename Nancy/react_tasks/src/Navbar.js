import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import './navbar.css'

class Navbar extends Component {
  render() {
    return (
      <nav className="navbar">
        <Link to="./home">Home</Link>
        <Link className="nav-task1 active" to="/Task1">
          Task 1
        </Link>
      </nav>
    )
  }
}

export default Navbar
