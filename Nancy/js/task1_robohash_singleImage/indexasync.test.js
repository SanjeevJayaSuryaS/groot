const { getImage } = require('./indexasync.js')
const fs = require('fs')
const fileExists = (file) => {
  try {
    if (fs.existsSync(file)) {
      return true
    } else {
      return false
    }
  } catch (err) {
    console.error(err)
  }
}

describe('getImage', () => {
  test('Should generate and save image in the folder', async () => {
    const folderName = './task1_robohash_singleImage/images/'
    await getImage(folderName)
    await expect(fileExists(folderName + 'robo.png')).toBeTruthy()
  })
  test('Not be able to generate and store image in the folder', async () => {
    const folderName = undefined
    jest.setTimeout(30000)
    await getImage(folderName)
    await expect(fileExists(folderName + 'robo.png')).toBeFalsy()
  })
})
