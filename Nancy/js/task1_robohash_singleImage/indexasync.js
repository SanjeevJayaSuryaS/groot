const fs = require('fs')
const fetch = require('node-fetch')

const { ROBO_URL } = require('../constant')

const generateStringPromise = () => {
  return new Promise((resolve, reject) => {
    resolve(Math.random().toString(20).substr(2, 10))
  })
}

const getImagePromise = (imageBufferBody, folderName) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(folderName + '/robo.png', imageBufferBody, (err) => {
      if (err) {
        reject(err)
      }
      resolve()
    })
  })
}

const getImage = async (folderName) => {
  try {
    const generatedString = await generateStringPromise()
    const response = await fetch(`${ROBO_URL}${generatedString}`)
    const imageArrayBuffer = await response.arrayBuffer()
    const imgData = Buffer.from(imageArrayBuffer)
    await getImagePromise(imgData, folderName)
    return 'successfully generated'
  } catch (err) {
    return 'Not able to generate'
  }
}
module.exports = {
  generateStringPromise,
  getImagePromise,
  getImage
}
