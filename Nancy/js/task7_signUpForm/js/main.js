/* istanbul ignore file */

const $form = document.getElementById('form')

const renderWarningMessage = (message) => {
  const element = document.createElement('span')
  element.classList.add('warning-span')
  element.textContent = message
  return element
}

const validateInput = (input, message, formField) => {
  if (input.value === '') {
    formField.appendChild(renderWarningMessage(message))
    input.classList.add('input-error')
    input.setAttribute('placeholder', '')
    input.nextElementSibling.style.display = 'block'
  } else {
    input.nextElementSibling.style.display = 'none'
  }
}

$form.addEventListener('submit', (e) => {
  e.preventDefault()

  const name = e.target.name
  const lastName = e.target.lastname
  const email = e.target.email
  const password = e.target.password

  if (name.className === 'input-error') {
    name.classList.remove('input-error')
  }

  if (lastName.className === 'input-error') {
    lastName.classList.remove('input-error')
  }

  if (email.className === 'input-error') {
    email.classList.remove('input-error')
  }

  if (password.className === 'input-error') {
    password.classList.remove('input-error')
  }

  const input = document.querySelectorAll('.form-field')

  const $warnings = $form.querySelectorAll('span.warning-span')
  $warnings.forEach((el) => {
    el.textContent = ''
  })

  validateInput(name, 'First Name cannot be empty', input[0])
  validateInput(lastName, 'Last Name cannot be empty', input[1])
  validateInput(email, 'Email cannot be empty', input[2])
  validateInput(password, 'Password cannot be empty', input[3])
})
