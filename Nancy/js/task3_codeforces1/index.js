const fetch = require('node-fetch')
const CODEFORCES_URL = 'http://codeforces.com/api/user.info'

const nameHighestRating = async (nameArray) => {
  try {
    let URL = CODEFORCES_URL + '?handles='
    nameArray.forEach((handleName) => {
      if (!handleName || !isNaN(handleName)) {
        throw Error('Not able to get the results')
      }
      URL = URL + handleName + ';'
    })
    const document = {}
    const response = await fetch(URL)
    const responseJSON = await response.json()
    const cfData = responseJSON.result
    await cfData.forEach((user) => {
      document[user.handle] = user.rating
    })

    const ratingHighest = Object.keys(document).reduce((a, b) =>
      document[a] > document[b] ? a : b
    )

    return { ratingHighest }
  } catch (err) {
    return 'Not able to get the results'
  }
}

const nameMaxContest = async (nameArray) => {
  try {
    let URL = CODEFORCES_URL + '?handles='
    nameArray.every((handleName) => {
      if (!handleName || !isNaN(handleName)) {
        throw Error('Not able to get the results')
      }
      URL = URL + handleName + ';'
      return URL
    })
    const document = {}
    const response = await fetch(URL)
    const responseJSON = await response.json()
    const cfData = responseJSON.result
    await cfData.forEach((user) => {
      document[user.handle] = user.maxRating
    })

    const ratingMax = Object.keys(document).reduce((a, b) =>
      document[a] > document[b] ? a : b
    )
    return { ratingMax }
  } catch (err) {
    return 'Not able to get the results'
  }
}

module.exports = {
  nameHighestRating,
  nameMaxContest
}
