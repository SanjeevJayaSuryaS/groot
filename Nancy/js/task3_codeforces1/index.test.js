const { nameHighestRating, nameMaxContest } = require('./index.js')

describe('nameHighestRating', () => {
  // const spy = jest.spyOn(console, 'log').mockImplementation()
  test('should give the name of the user with highest rating', async () => {
    const userArray = [
      'lokpati',
      'Ashishgup',
      'T1duS',
      'vivo',
      'Anushi1998',
      'Kruti_20',
      'CMin2021'
    ]
    const response = await nameHighestRating(userArray)
    expect(response).toStrictEqual({ ratingHighest: 'Ashishgup' })
  })
  test('should not be able to retrieve result', async () => {
    const userArray = [
      'lokpati',
      undefined,
      'T1duS',
      'vivo',
      'Anushi1998',
      'Kruti_20',
      'CMin2021'
    ]
    const response = await nameHighestRating(userArray)
    expect(response).toStrictEqual('Not able to get the results')
  })
})

describe('nameMaxContest', () => {
  test('should give the name of the user with the maxRating', async () => {
    const userArray = [
      'lokpati',
      'Ashishgup',
      'T1duS',
      'vivo',
      'Anushi1998',
      'Kruti_20',
      'CMin2021'
    ]
    const response = await nameMaxContest(userArray)
    expect(response).toStrictEqual({ ratingMax: 'T1duS' })
  })

  test('should not be able to retrieve result', async () => {
    const userArray = [
      'lokpati',
      undefined,
      'T1duS',
      'vivo',
      'Anushi1998',
      'Kruti_20',
      'CMin2021'
    ]
    const response = await nameMaxContest(userArray)
    expect(response).toStrictEqual('Not able to get the results')
  })
})
