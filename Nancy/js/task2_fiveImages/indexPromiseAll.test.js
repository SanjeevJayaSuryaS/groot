const { getImage, getImagePromise } = require('./indexPromiseAll')
const fs = require('fs')
const { ROBO_NAME } = require('../constant')

const fileExists = (file) => {
  try {
    if (fs.existsSync(file)) {
      return true
    } else {
      return false
    }
  } catch (err) {
    console.error(err)
  }
}

describe('getImage', () => {
  test('should store image in folder', async () => {
    const folderName = './task2_fiveImages/images/' + ROBO_NAME

    const stringLengthArray = [1, 2, 3, 4, 5]
    await getImage(stringLengthArray, folderName)
    const areImagesPresent = await stringLengthArray.every((element, index) =>
      fileExists(folderName + index + '.png')
    )
    expect(areImagesPresent).toBeTruthy()
  })
  test('Not be able to store image in the folder', async () => {
    const stringLengthArray = [1, 2, 3, 4, 5, undefined]
    const folderName = undefined
    await getImage(stringLengthArray, folderName)
    const areImagesPresent = stringLengthArray.every((element, index) => {
      return fileExists(folderName + index + '.png')
    })
    expect(areImagesPresent).toBeFalsy()
  })
  test('throw an error if the folder name for images does not exist', async () => {
    return expect(getImagePromise()).rejects.toThrow(new Error())
  })
})
