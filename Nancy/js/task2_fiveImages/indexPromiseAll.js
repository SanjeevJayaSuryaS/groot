const fs = require('fs')
const fetch = require('node-fetch')
const { ROBO_URL } = require('../constant')

const generateStringPromise = (randomStringLength) => {
  return new Promise((resolve, reject) => {
    if (isNaN(randomStringLength)) {
      reject(new Error())
    }
    resolve(Math.random().toString(20).substr(2, randomStringLength))
  })
}
const getImagePromise = (imageBufferBody, imgIndex, folderName) => {
  return new Promise((resolve, reject) => {
    if (!folderName) {
      reject(new Error())
    }
    fs.writeFile(folderName + imgIndex + '.png', imageBufferBody, (err) => {
      /* istanbul ignore next */
      if (err) {
        throw err
      }
      resolve()
    })
  })
}
const getImage = async (lengthArr, folderName) => {
  try {
    const stringArray = await Promise.all(
      lengthArr.map((generateStringLength) =>
        generateStringPromise(generateStringLength)
      )
    )
    const imageArray = await Promise.all(
      stringArray.map(async (generatedString) => {
        const response = await fetch(`${ROBO_URL}${generatedString}`)
        const imageArrayBuffer = await response.arrayBuffer()
        const imageData = await Buffer.from(imageArrayBuffer)
        return imageData
      })
    )
    await Promise.all(
      imageArray.map((imgData, index) =>
        getImagePromise(imgData, index, folderName)
      )
    )
    return 'successfully generated'
  } catch (error) {
    return 'Not able to generate the image'
  }
}
module.exports = { getImage, getImagePromise }
