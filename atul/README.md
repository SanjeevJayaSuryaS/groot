Name - **Atul Rana**<br>
College - **JAYEPEE INSTITUTE OF INFORMATION TECHNOLOGY NOIDA**<br>
Hobbies - **Singing , Playing Guitar , Playing Cricket**<br>

JavaScript - **Task - 0**<br>

Description:<br>
Writing the code for the following listed programs and also did the unit testing<br>

Programs Question:<br>
1 - **Write Fizz buzz program and write the unit tests for the same.**<br>
2 - **Write Binary search program and write the unit tests for the same.**<br>
3 - **Solve the ACM ICPC team Problem and write the unit tests for the same.**<br>

JavaScript - **Task -1**<br>

Description:<br>
API End point: https://dog.ceo/dog-api/documentation/breed<br>

1 - **read the breed name from the .txt file eg: retriever**<br>
2 - **use Fetch or Axios to call your api and get the response**<br>
3 - **Get the image from the response and store it in a file**<br>
4 - **Write the unit tests for the task**<br>

JavaScript - **Task -2**<br>

Description:<br>
API End point: https://dog.ceo/dog-api/documentation/breed<br>

1 - **read the breed name from the .txt file eg: retriever**<br>
2 - **use Fetch or Axios to call your api and get the response**<br>
3 - **Get 5 images from the response and store it in a file**<br>
4 - **Write the unit tests for the task**<br>

JavaScript - **Task -3**<br>

Description:<br>

1 - **Get ipl data in .csv format i.e matches.csv and deliveries.csv.https://www.kaggle.com/nowke9/ipldata?select=deliveries.csv**<br>
2 - **Convert CSVtoJSON**<br>
3 - **Transform raw data of IPL to calculate the given Stats**<br>
a - **Total Number of matches played per year for all the years in IPL**<br>
b - **Number of matches won of per team per year in IPL**<br>
c - **Extra runs conceded per team in 2016**<br>
4 - **Create a separate function for each task and store the separate function result in respective json files**<br>

JavaScript - **Task -4**<br>

Description:<br>

1 - **using HTML, CSS and Javascript create a simple dash board having a task bar with 3 buttons for all your sub tasks for task-3**<br>
2 - **Upon clicking on any sub task button in the task bar user should see a graph generated from the json file and a download button from which we can download the output json file.**<br>
a - **Subtask 1 had to be created with D3.js library.**<br>
b - **Subtask 2 had to created using HighCharts library.**<br>
c - **Subtask3 you can use any library for the chart creation.**<br>

JavaScript - **Task -5**<br>

Description:<br>

1 - **Using Google Authentication you have to implement the signin functionality.**<br>
2 - **Have to take care of the error states as well.**<br>
3 - **Should be 100% mobile friendly**<br>
4 - **Implement the routes like / for sign in page, /dashboard, /task-1, /task-2 , /task-3 etc.**<br>

React - **Task -1**

Description:<br>
**ToDo List**

1 - **A basic ToDo list app. where user enters the content in the input box and on pressing enter or clicking on add button the cards is getting added.**<br>
2 - **The card should get deleted on pressing the mouse pointer on any particular card**<br>
3 - **You should keep track of how many cards are present in your app as a global counter.**<br>
