const { convertCsvToJson } = require('.')
const {
  getTotalMatchesPerYear,
  getMatchesWonPerTeam,
  getExtraRunsCalendarYear
} = require('./ipl')

const matchesPerYearOutput = {
  2008: 58,
  2009: 57,
  2010: 60,
  2011: 73,
  2012: 74,
  2013: 76,
  2014: 60,
  2015: 59,
  2016: 60,
  2017: 59,
  2018: 60,
  2019: 60
}
const extraRunsOutput = [
  { teamName: 'Sunrisers Hyderabad', extraRuns: 107 },
  { teamName: 'Mumbai Indians', extraRuns: 102 },
  { teamName: 'Gujarat Lions', extraRuns: 98 },
  { teamName: 'Rising Pune Supergiant', extraRuns: 0 },
  { teamName: 'Royal Challengers Bangalore', extraRuns: 156 },
  { teamName: 'Kolkata Knight Riders', extraRuns: 122 },
  { teamName: 'Delhi Daredevils', extraRuns: 106 },
  { teamName: 'Kings XI Punjab', extraRuns: 100 },
  { teamName: 'Chennai Super Kings', extraRuns: 0 },
  { teamName: 'Rajasthan Royals', extraRuns: 0 },
  { teamName: 'Deccan Chargers', extraRuns: 0 },
  { teamName: 'Kochi Tuskers Kerala', extraRuns: 0 },
  { teamName: 'Pune Warriors', extraRuns: 0 },
  { teamName: 'Rising Pune Supergiants', extraRuns: 108 },
  { teamName: 'Delhi Capitals', extraRuns: 0 }
]

const matchesWonPerTeamOutput = {
  'Sunrisers Hyderabad': {
    2008: 0,
    2009: 0,
    2010: 0,
    2011: 0,
    2012: 0,
    2013: 10,
    2014: 6,
    2015: 7,
    2016: 11,
    2017: 8,
    2018: 10,
    2019: 6
  },
  'Mumbai Indians': {
    2008: 7,
    2009: 5,
    2010: 11,
    2011: 10,
    2012: 10,
    2013: 13,
    2014: 7,
    2015: 10,
    2016: 7,
    2017: 12,
    2018: 6,
    2019: 11
  },
  'Gujarat Lions': {
    2008: 0,
    2009: 0,
    2010: 0,
    2011: 0,
    2012: 0,
    2013: 0,
    2014: 0,
    2015: 0,
    2016: 9,
    2017: 4,
    2018: 0,
    2019: 0
  },
  'Rising Pune Supergiant': {
    2008: 0,
    2009: 0,
    2010: 0,
    2011: 0,
    2012: 0,
    2013: 0,
    2014: 0,
    2015: 0,
    2016: 0,
    2017: 10,
    2018: 0,
    2019: 0
  },
  'Royal Challengers Bangalore': {
    2008: 4,
    2009: 9,
    2010: 8,
    2011: 10,
    2012: 8,
    2013: 9,
    2014: 5,
    2015: 8,
    2016: 9,
    2017: 3,
    2018: 6,
    2019: 5
  },
  'Kolkata Knight Riders': {
    2008: 6,
    2009: 3,
    2010: 7,
    2011: 8,
    2012: 12,
    2013: 6,
    2014: 11,
    2015: 7,
    2016: 8,
    2017: 9,
    2018: 9,
    2019: 6
  },
  'Delhi Daredevils': {
    2008: 7,
    2009: 10,
    2010: 7,
    2011: 4,
    2012: 11,
    2013: 3,
    2014: 2,
    2015: 5,
    2016: 7,
    2017: 6,
    2018: 5,
    2019: 0
  },
  'Kings XI Punjab': {
    2008: 10,
    2009: 7,
    2010: 4,
    2011: 7,
    2012: 8,
    2013: 8,
    2014: 12,
    2015: 3,
    2016: 4,
    2017: 7,
    2018: 6,
    2019: 6
  },
  'Chennai Super Kings': {
    2008: 9,
    2009: 8,
    2010: 9,
    2011: 11,
    2012: 10,
    2013: 12,
    2014: 10,
    2015: 10,
    2016: 0,
    2017: 0,
    2018: 11,
    2019: 10
  },
  'Rajasthan Royals': {
    2008: 13,
    2009: 6,
    2010: 6,
    2011: 6,
    2012: 7,
    2013: 11,
    2014: 7,
    2015: 7,
    2016: 0,
    2017: 0,
    2018: 7,
    2019: 5
  },
  'Deccan Chargers': {
    2008: 2,
    2009: 9,
    2010: 8,
    2011: 6,
    2012: 4,
    2013: 0,
    2014: 0,
    2015: 0,
    2016: 0,
    2017: 0,
    2018: 0,
    2019: 0
  },
  'Kochi Tuskers Kerala': {
    2008: 0,
    2009: 0,
    2010: 0,
    2011: 6,
    2012: 0,
    2013: 0,
    2014: 0,
    2015: 0,
    2016: 0,
    2017: 0,
    2018: 0,
    2019: 0
  },
  'Pune Warriors': {
    2008: 0,
    2009: 0,
    2010: 0,
    2011: 4,
    2012: 4,
    2013: 4,
    2014: 0,
    2015: 0,
    2016: 0,
    2017: 0,
    2018: 0,
    2019: 0
  },
  'Rising Pune Supergiants': {
    2008: 0,
    2009: 0,
    2010: 0,
    2011: 0,
    2012: 0,
    2013: 0,
    2014: 0,
    2015: 0,
    2016: 5,
    2017: 0,
    2018: 0,
    2019: 0
  },
  'Delhi Capitals': {
    2008: 0,
    2009: 0,
    2010: 0,
    2011: 0,
    2012: 0,
    2013: 0,
    2014: 0,
    2015: 0,
    2016: 0,
    2017: 0,
    2018: 0,
    2019: 10
  }
}

describe('Testing functions getTeams,getYears,getTotalMatchesPerYear,getMatchesWonPerTeam,getExtraRunsCalendarYear', () => {
  test('should check if the getTotalMatchesPer function returns correct output', async () => {
    const matchesJson = await convertCsvToJson('matches.csv')
    expect(getTotalMatchesPerYear(matchesJson)).toStrictEqual(
      matchesPerYearOutput
    )
  })
  test('should check if the getMatchesWonPerTeam function returns correct output', async () => {
    const matchesJson = await convertCsvToJson('matches.csv')
    expect(getMatchesWonPerTeam(matchesJson)).toStrictEqual(
      matchesWonPerTeamOutput
    )
  })
  test('should check if the getExtraRunsCalendarYear function returns correct output', async () => {
    const matchesJson = await convertCsvToJson('matches.csv')
    const deliveriesJson = await convertCsvToJson('deliveries.csv')
    expect(
      getExtraRunsCalendarYear(matchesJson, deliveriesJson, '2016')
    ).toStrictEqual(extraRunsOutput)
  })
})
