const { convertCsvToJson, writeData } = require('./index')
const fs = require('fs')
const path = require('path')

describe('testing function convertingCsvToJson ,writingData', () => {
  test('should throw error if cannot convert csv to json', async () => {
    await expect(convertCsvToJson('matches123.csv')).rejects.toStrictEqual(
      new Error('Failed to convert csv to JSON')
    )
  })
  test('should throw error if cannot write data into the file', async () => {
    const text = await writeData({}, '')
    await expect(text).toEqual(new Error('Failed to write into the file'))
  })
  test('should write data to file correctly', async () => {
    const text = await writeData('atul,1', 'test.json')
    expect(text).toBe(
      'Successfully written data for total matches played into file'
    )
    fs.unlinkSync(path.join(__dirname, '../output/test.json'))
  })
})
