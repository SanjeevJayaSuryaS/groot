const path = require('path')
const csv = require('csvtojson')
const fs = require('fs').promises
const {
  getTotalMatchesPerYear,
  getMatchesWonPerTeam,
  getExtraRunsCalendarYear
} = require('./ipl')

const convertCsvToJson = async (fileName) => {
  try {
    const csvFilePath = path.join(__dirname, `../data/${fileName}`)
    const jsonArray = await csv().fromFile(csvFilePath)
    return jsonArray
  } catch {
    throw new Error('Failed to convert csv to JSON')
  }
}
const writeData = async (fileData, fileName) => {
  const jsonString = JSON.stringify(fileData)
  try {
    await fs.writeFile(
      path.join(__dirname, `../output/${fileName}`),
      jsonString
    )
    return 'Successfully written data for total matches played into file'
  } catch {
    return new Error('Failed to write into the file')
  }
}
const totalMatchesPlayed = async () => {
  const matchesJson = await convertCsvToJson('matches.csv')
  const totalMatchesPerYear = await getTotalMatchesPerYear(matchesJson)
  writeData(totalMatchesPerYear, 'totalMatchesPerYear.json')
}
const matchesWon = async () => {
  const matchesJson = await convertCsvToJson('matches.csv')
  const matchesWonPerTeam = getMatchesWonPerTeam(matchesJson)
  writeData(matchesWonPerTeam, 'matchesWonPerTeam.json')
}
const extraRunsPerTeam = async (year) => {
  const matchesJson = await convertCsvToJson('matches.csv')
  const deliveriesJson = await convertCsvToJson('deliveries.csv')
  const extraRunsOneYear = getExtraRunsCalendarYear(
    matchesJson,
    deliveriesJson,
    year
  )
  writeData(extraRunsOneYear, 'ExtraRunsPerTeam2016.json')
}

module.exports = {
  matchesWon,
  totalMatchesPlayed,
  extraRunsPerTeam,
  writeData,
  convertCsvToJson
}
