const getYears = (inputArray) => {
  const year = inputArray.map((data) => data.season)
  const uniqueYear = [...new Set(year)]
  return uniqueYear
}
const getTeams = (inputArray) => {
  const teams = inputArray.map((data) => data.team1)
  const uniqueTeams = [...new Set(teams)]
  return uniqueTeams
}
const getTotalMatchesPerYear = (data) => {
  const uniqueYears = getYears(data)
  const matchesPerYear = uniqueYears.reduce(function (object, element) {
    const matchesCurrentYear = data.filter((ele) => ele.season === element)
    object[element] = matchesCurrentYear.length
    return object
  }, {})
  return matchesPerYear
}
const getMatchesWonPerTeam = (data) => {
  const uniqueTeams = getTeams(data)
  const uniqueYears = getYears(data)
  const matchesWonPerTeam = uniqueTeams.reduce(function (finalObject, teams) {
    const matchesWonPerYear = uniqueYears.reduce(function (middleObject, year) {
      const count = data.reduce(function (counter, ele) {
        if (ele.season === year && ele.winner === teams) {
          counter += 1
        }
        return counter
      }, 0)
      middleObject[year] = count
      return middleObject
    }, {})
    finalObject[teams] = matchesWonPerYear
    return finalObject
  }, {})
  return matchesWonPerTeam
}

const getExtraRunsCalendarYear = (matchesJson, deliveriesJson, year) => {
  const matchesInCalendarYear = matchesJson.filter(
    (match) => match.season === year
  )
  const matchIdsCalendarYear = matchesInCalendarYear.map((match) => match.id)
  const deliveriesCalendarYear = deliveriesJson.filter((delivery) =>
    matchIdsCalendarYear.includes(delivery.match_id)
  )
  const distinctTeams = getTeams(matchesJson)
  const deliveriesPerTeam = distinctTeams.map((team) => {
    return {
      teamName: team,
      deliveries: deliveriesCalendarYear.filter(
        (deliveries) => deliveries.bowling_team === team
      )
    }
  })
  const extraRuns = deliveriesPerTeam.map((team) => {
    const { teamName } = team
    const runsConceded = team.deliveries.reduce((counter, currentValue) => {
      return counter + parseInt(currentValue.extra_runs)
    }, 0)
    return {
      teamName,
      extraRuns: runsConceded
    }
  })
  return extraRuns
}

module.exports = {
  getTeams,
  getYears,
  getTotalMatchesPerYear,
  getMatchesWonPerTeam,
  getExtraRunsCalendarYear
}
