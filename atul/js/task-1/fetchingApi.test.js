const { fetchingApi } = require('./fetchingApi')
const { readDataFromFile } = require('./fetchingApi')
const { writingDataToFile } = require('./fetchingApi')
const axios = require('axios')
const fs = require('fs')
const path = require('path')
const inputFile = 'dogs.txt'
const outputFile = 'apiFetchingLink.txt'
const initializeDogBreed = async (dogBreedName) => {
  await fs.writeFile(path.join(__dirname, 'dogs.txt'), dogBreedName, (err) => {
    if (err) console.log('failed')
    console.log('written boxer')
  })
}

beforeAll(() => {
  return initializeDogBreed('boxer')
})

describe('Testing all the functions for fetching dog images from API and writing data into the file', () => {
  test('should return error for invalid file is passed as input', () => {
    return expect(readDataFromFile('beast.txt')).rejects.toStrictEqual(
      new Error('The File does not exist')
    )
  })
  test('should test if the correct link to api is generated after receiving data from th file', () => {
    return readDataFromFile(inputFile).then((response) => {
      expect(response).toBe('https://dog.ceo/api/breed/boxer/images/random')
    })
  })
  test('should throw error if dog breed from input file is not valid', async () => {
    initializeDogBreed('polar')
    await expect(fetchingApi(inputFile, outputFile)).rejects.toStrictEqual(
      new Error('Dog breed does not exist')
    )
  })
  test('should check if the axios function is called once', async () => {
    await initializeDogBreed('boxer')
    const spy = jest.spyOn(axios, 'get')
    await fetchingApi(inputFile, outputFile)
    expect(spy).toHaveBeenCalled()
    fs.unlinkSync(path.join(__dirname, outputFile))
  })
  test('should check if data not written into the file', async () => {
    await expect(
      writingDataToFile(
        {
          message: 'https://images.dog.ceo/breeds/boxer/n02108089_2791.jpg',
          status: 'success'
        },
        ''
      )
    ).rejects.toStrictEqual(new Error('Could not write into the file'))
  })
  test('should check if data written in the file successfully', () => {
    return writingDataToFile(
      {
        message: 'https://images.dog.ceo/breeds/boxer/n02108089_2791.jpg',
        status: 'success'
      },
      outputFile
    ).then((response) => {
      expect(response).toBe('Written Data into the file')
      fs.unlinkSync(path.join(__dirname, outputFile))
    })
  })
})
