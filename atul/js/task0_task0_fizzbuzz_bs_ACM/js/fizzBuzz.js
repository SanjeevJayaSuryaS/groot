/* In fizzBuzz Problem we have to take input a integer
we have to iterate over the range from 1....givenInteger(n)
and Return "Fizz" if the input number is divisible by 3
Returns "Buzz" is the input number is divisible by 5 
Returns "FizzBuzz" if the input number is divisible by both 3 & 5 */

function fizzBuzz(inputNumber) {
  // check is the Value is not an number
  if (isNaN(inputNumber)) {
    throw new Error('Given Input is not a number')
  }
  // check if the number is not less than equal to 0
  if (inputNumber <= 0) {
    throw new Error('Not a valid input for our problem')
  }
  const finalOutput = Array.apply(0, Array(inputNumber)).map(function (
    value,
    iterator
  ) {
    // Current Number in range to check.
    const currentNumber = iterator + 1
    let currentOutput = ''
    // If Divisible by 3 concatenate "Fizz" to currentOutput
    if (currentNumber % 3 === 0) {
      currentOutput += 'Fizz'
    }
    // If Divisible by 5 concatenate to the currentInput
    if (currentNumber % 5 === 0) {
      currentOutput += 'Buzz'
    }
    // If not divisible by any one
    if (currentOutput === '') {
      return currentNumber.toString()
    }
    return currentOutput
  })

  // Final Array of Outputs
  return finalOutput
}
module.exports = fizzBuzz
