// Importing fizzBuzz file in this case a single fizzBuzz function
const fizzBuzz = require('./fizzBuzz')

const cases = [
  [1, ['1']],
  [
    15,
    [
      '1',
      '2',
      'Fizz',
      '4',
      'Buzz',
      'Fizz',
      '7',
      '8',
      'Fizz',
      'Buzz',
      '11',
      'Fizz',
      '13',
      '14',
      'FizzBuzz'
    ]
  ],
  [3, ['1', '2', 'Fizz']],
  [5, ['1', '2', 'Fizz', '4', 'Buzz']]
]

describe('fizzBuzz', () => {
  describe('Test for  Valid Inputs', () => {
    test.each(cases)(
      'given %p as argument, returns %p',
      (firstArg, expectedResult) => {
        const result = fizzBuzz(firstArg)
        expect(result).toEqual(expectedResult)
      }
    )
  })
  describe('Test for Invalid input', () => {
    test('should throw error if an string input is passed to the function', () => {
      expect(() => fizzBuzz('abc')).toThrow(fizzBuzz.Error)
    })
    test('should throw error if an floating number is passed as the parameter to the function', () => {
      expect(() => fizzBuzz(7.9)).toThrow(fizzBuzz.Error)
    })
    test('should throw error if the object is passed as the parameter to the function', () => {
      expect(() => fizzBuzz({})).toThrow(fizzBuzz.Error)
    })
    test('should throw error if negative number is passed as the parameter to the function', () => {
      expect(() => fizzBuzz(-2)).toThrow(fizzBuzz.Error)
    })
    test('should throw error if 0 is passed as the input parameter', () => {
      expect(() => fizzBuzz(0)).toThrow(fizzBuzz.Error)
    })
  })
})
