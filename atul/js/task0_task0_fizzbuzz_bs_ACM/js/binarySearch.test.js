const binarySearch = require('./binarySearch')
const cases = [
  [[], 3, -1],
  [[1, 2, 3, 4], 5, -1],
  [[1, 2, 3, 4], 2, 1],
  [[1, 2, 3, 4, 5], 1, 0],
  [[-4, -3, -2, 0, 2, 4, 6, 7], 7, 7],
  [[-4, -3, -2, 0, 2, 4, 6, 7], -4, 0]
]

describe('BinarySearch', () => {
  describe('Test for valid inputs', () => {
    test.each(cases)(
      'given %p and %p as arguments, returns %p',
      (firstArg, secondArg, expectedResult) => {
        const result = binarySearch(firstArg, secondArg)
        expect(result).toEqual(expectedResult)
      }
    )
  })
  describe('Test for invalid inputs', () => {
    test('should return error if invalid key value = abc is passed as one of the parameter to the function', () => {
      expect(() => binarySearch([1, 2, 3, 4], 'abc')).toThrow(
        binarySearch.Error
      )
    })
    test('should return error if invalid array is passed as parameter to the function', () => {
      expect(() => binarySearch({}, 7)).toThrow(binarySearch.Error)
    })
    test('should return error if string is passed as parameter instead of array', () => {
      expect(() => binarySearch('abc', 7)).toThrow(binarySearch.Error)
    })
  })
})
