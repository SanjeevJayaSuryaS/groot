function acmTeam(topic) {
  if (!Array.isArray(topic)) {
    throw new Error('Invalid Input')
  }
  const totalStudents = topic.length
  if (totalStudents < 2) {
    throw new Error('Size should be atLeast 2!')
  }
  const totalSubject = topic[0].length
  if (totalSubject < 1) {
    throw new Error('Subject should be atLeast 1!')
  }
  let maximumKnownSubjects = 0
  let totalTeam = 1
  for (let iterator1 = 0; iterator1 < totalStudents - 1; iterator1++) {
    for (
      let iterator2 = iterator1 + 1;
      iterator2 < totalStudents;
      iterator2++
    ) {
      let counter = 0
      for (let iterator3 = 0; iterator3 < totalSubject; iterator3++) {
        if (
          topic[iterator1][iterator3] === '1' ||
          topic[iterator2][iterator3] === '1'
        ) {
          counter++
        }
      }
      if (counter > maximumKnownSubjects) {
        maximumKnownSubjects = counter
        totalTeam = 1
      } else if (counter === maximumKnownSubjects) {
        totalTeam++
      }
    }
  }
  return [maximumKnownSubjects, totalTeam]
}

module.exports = acmTeam
