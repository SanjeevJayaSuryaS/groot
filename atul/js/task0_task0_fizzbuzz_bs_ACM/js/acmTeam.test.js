const acmTeam = require('./acmTeam')

describe('acmTeam', () => {
  test('Should throw error when input is not array of string', () => {
    const result = () => acmTeam(undefined)
    expect(result).toThrow('Invalid Input')
  })
  test('should throw Error error when size array is less than 2', () => {
    const result = () => acmTeam(['11100'])
    expect(result).toThrow('Size should be atLeast 2!')
  })
  test('Should throw error when string is empty', () => {
    const result = () => acmTeam(['', ''])
    expect(result).toThrow('Subject should be atLeast 1!')
  })
  test('should return [5,3]', () => {
    const result = acmTeam(['11100', '11111', '11111'])
    expect(result).toEqual([5, 3])
  })
  test('should return [5,1]', () => {
    const result = acmTeam(['10101', '11110', '00010'])
    expect(result).toEqual([5, 1])
  })
})
