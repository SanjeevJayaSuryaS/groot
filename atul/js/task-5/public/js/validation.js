/* eslint-disable */
/* istanbul ignore file */
const validation = () => {
  let isValid = true
  const fName = document.forms['signUpForFree']['fName'].value
  const lName = document.forms['signUpForFree']['lName'].value
  const email = document.forms['signUpForFree']['email'].value
  const password = document.forms['signUpForFree']['password'].value

  if (fName == '') {
    document.getElementById('firstNameError').innerText =
      '*Name must be filled out'
    isValid = false
  }
  if (lName == '') {
    document.getElementById('lastNameError').innerText =
      '*Last Name must be filled out'
    isValid = false
  }
  const atPosition = email.indexOf('@')
  const dotPosition = email.lastIndexOf('.')
  if (email == '') {
    document.getElementById('emailError').innerText = '*Email must be filled'
    isValid = false
  } else if (
    atPosition < 1 ||
    dotPosition < atPosition + 2 ||
    dotPosition + 2 >= x.length
  ) {
    document.getElementById('emailError').innerText =
      '*Oops!! please check the email entered'
    isValid = false
  }
  if (password == '') {
    document.getElementById('passwordError').innerText =
      '*Password must be filled'
    isValid = false
  } else if (password.length < 6) {
    document.getElementById('passwordError').innerText =
      '*Password to small must be 6 character long'
    isValid = false
  }

  return isValid ? true : false
}
