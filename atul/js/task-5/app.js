/* istanbul ignore file */
const express = require('express')
const app = express()
const path = require('path')
// Static files
app.use(express.static(path.join(__dirname, 'public')))

app.set('views', path.join(__dirname, '/views'))
app.set('view engine', 'ejs')

app.get('/', (req, res) => {
  res.render('index', { root: __dirname })
})

app.use(express.static(path.join(__dirname, '../task-4 IPL(Part-2)')))
app.use(express.static(path.join(__dirname, '../task-3-IPL(Part-1)')))
app.get('/dashboard', (req, res) => {
  res.render('dashboard')
})

// Starting server on a port
app.listen(4000, () => console.log('Server started on port 4000'))
