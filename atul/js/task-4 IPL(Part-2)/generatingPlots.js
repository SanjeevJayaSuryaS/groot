/* eslint-disable */
/* istanbul ignore file */

// set the dimensions and margins of the graph
let inputFileData
let fileName = 'totalMatchesPerYear.json'
let yLabel = 'Extras'
const svg = d3.select('svg')
const margin = { top: 20, right: 20, bottom: 30, left: 40 }
const x = d3.scaleBand().padding(0.1)
const y = d3.scaleLinear()

const highChartContainer = document.querySelector('#container')
const d3Svg = document.querySelector('svg')

document.getElementById('sub3btn').addEventListener('click', function (event) {
  const burger = document.querySelector('.burger')
  const nav = document.querySelector('.nav-links')
  nav.classList.toggle('nav-active')
  burger.classList.toggle('toggle')
  d3Svg.style.display = 'block'
  highChartContainer.style.display = 'none'
  d3.selectAll(svg).remove()
  fileName = 'ExtraRunsPerTeam2016.json'
  loadData(fileName)
  check = 2
})
document.getElementById('sub1btn').addEventListener('click', function (event) {
  const burger = document.querySelector('.burger')
  const nav = document.querySelector('.nav-links')
  nav.classList.toggle('nav-active')
  burger.classList.toggle('toggle')
  d3Svg.style.display = 'block'
  highChartContainer.style.display = 'none'
  d3.selectAll(svg).remove()
  yLabel = 'Matches'
  fileName = 'totalMatchesPerYear.json'
  loadData(fileName)
})
document.getElementById('sub2btn').addEventListener('click', function (event) {
  const burger = document.querySelector('.burger')
  const nav = document.querySelector('.nav-links')
  nav.classList.toggle('nav-active')
  burger.classList.toggle('toggle')
  d3Svg.style.display = 'none'
  highChartContainer.style.display = 'block'
  fileName = 'matchesWonPerTeam.json'
  loadData(fileName)
})

const g = svg
  .append('g')
  .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
  .attr('Title', 'title')

g.append('g').attr('class', 'axis axis--x')

g.append('g').attr('class', 'axis axis--y')
g.append('text')
  .style('font-size', '10px')
  .attr('transform', 'rotate(-90)')
  .attr('y', 6)
  .attr('dy', '0.71em')
  .attr('text-anchor', 'end')
  .attr('class', 'y-label')

g.append('text').attr('class', 'x-labels').style('font-size', '34px')

// .text('Matches Won')

function draw() {
  const bounds = svg.node().getBoundingClientRect()
  const width = bounds.width - margin.left - margin.right
  const height = bounds.height - margin.top - margin.bottom - 150

  x.rangeRound([0, width])
  y.rangeRound([height, 0])
  g.select('.y-label').text(
    fileName === 'totalMatchesPerYear.json' ? 'Matches Won' : 'Extra Runs'
  )
  g.select('.x-labels')
    .attr('x', width / 2 - 100)
    .attr('y', height + 150)
    .text(fileName === 'totalMatchesPerYear.json' ? 'Years' : 'Teams')

  g.select('.axis--x')
    .attr('transform', 'translate(0,' + height + ')')
    .call(d3.axisBottom(x))
    .selectAll('text')
    .style('text-anchor', 'end')
    .style('font-size', '10px')
    .attr('dx', '-0.8em')
    .attr('dy', '0.15em')
    .attr('transform', 'rotate(-65)')

  g.select('.axis--y')
    .call(d3.axisLeft(y).ticks(10))
    .selectAll('text')
    .style('font-size', '10px')

  const bars = g.selectAll('.bar').data(inputFileData)

  // ENTER
  bars
    .enter()
    .append('rect')
    .attr('class', 'bar')
    .attr('x', function (d) {
      return fileName === 'ExtraRunsPerTeam2016.json'
        ? x(d.teamName)
        : x(d.year)
    })
    .attr('y', function (d) {
      return fileName === 'ExtraRunsPerTeam2016.json'
        ? y(d.extraRuns)
        : y(d.matches)
    })
    .attr('width', x.bandwidth())
    .attr('height', function (d) {
      return (
        height -
        (fileName === 'ExtraRunsPerTeam2016.json'
          ? y(d.extraRuns)
          : y(d.matches))
      )
    })

  // UPDATE
  bars
    .attr('x', function (d) {
      return fileName === 'ExtraRunsPerTeam2016.json'
        ? x(d.teamName)
        : x(d.year)
    })
    .attr('y', function (d) {
      return fileName === 'ExtraRunsPerTeam2016.json'
        ? y(d.extraRuns)
        : y(d.matches)
    })
    .attr('width', x.bandwidth())
    .attr('height', function (d) {
      return (
        height -
        (fileName === 'ExtraRunsPerTeam2016.json'
          ? y(d.extraRuns)
          : y(d.matches))
      )
    })

  // EXIT
  bars.exit().remove()
}

const loadData = async (fileName) => {
  inputFileData = await d3.json(`/output/${fileName}`)
  const changeData = () => {
    const finalArray = Object.keys(inputFileData).map((element) => {
      return { year: element, matches: inputFileData[element] }
    })
    console.log(finalArray)
    return finalArray
  }

  if (fileName !== 'matchesWonPerTeam.json') {
    inputFileData =
      fileName === 'ExtraRunsPerTeam2016.json'
        ? inputFileData.filter((element) => element.extraRuns > 0)
        : changeData()
    x.domain(
      inputFileData.map(function (d) {
        return fileName === 'ExtraRunsPerTeam2016.json' ? d.teamName : d.year
      })
    )
    y.domain([
      0,
      d3.max(inputFileData, function (d) {
        return fileName === 'ExtraRunsPerTeam2016.json'
          ? d.extraRuns
          : d.matches
      })
    ])
    window.addEventListener('resize', draw)
    draw()
  } else {
    plottingHighChart(inputFileData)
  }
}

loadData(fileName)

// HightCharts
const plottingHighChart = (theHightChartData) => {
  const seriesData = []
  Object.entries(theHightChartData).map(function (teamData) {
    seriesData.push({
      name: teamData[0],
      data: Object.values(teamData[1])
    })
  })
  console.log(seriesData)
  const chart = Highcharts.chart('container', {
    chart: {
      type: 'column'
    },

    title: {
      text: 'Matches Won Per Team Per Year'
    },

    subtitle: {
      text: 'Matches Won by single team in every calendar year'
    },

    legend: {
      align: 'right',
      verticalAlign: 'middle',
      layout: 'vertical'
    },

    xAxis: {
      categories: Object.keys(theHightChartData),
      labels: {
        x: -10
      }
    },

    yAxis: {
      allowDecimals: false,
      title: {
        text: 'Matches Won'
      }
    },

    series: seriesData,

    responsive: {
      rules: [
        {
          condition: {
            maxWidth: 500
          },
          chartOptions: {
            legend: {
              align: 'center',
              verticalAlign: 'bottom',
              layout: 'horizontal'
            },
            yAxis: {
              labels: {
                align: 'left',
                x: 0,
                y: -5
              },
              title: {
                text: null
              }
            },
            subtitle: {
              text: null
            },
            credits: {
              enabled: false
            }
          }
        }
      ]
    }
  })
  document.getElementById('small').addEventListener('click', function () {
    chart.setSize(400)
  })

  document.getElementById('large').addEventListener('click', function () {
    chart.setSize(600)
  })

  document.getElementById('auto').addEventListener('click', function () {
    chart.setSize(null)
  })
}

const downloadFile = () => {
  const a = document.getElementById('downLoad')
  const downloadLink = `../task-3-IPL(Part-1)/output/${fileName}`
  a.href = downloadLink
  a.download = fileName
}
document
  .getElementById('downloadBtn')
  .addEventListener('click', function (event) {
    downloadFile()
  })

const navSlide = () => {
  const burger = document.querySelector('.burger')
  const nav = document.querySelector('.nav-links')
  burger.addEventListener('click', () => {
    nav.classList.toggle('nav-active')
    burger.classList.toggle('toggle')
  })
  // Burger Animation
}
navSlide()
