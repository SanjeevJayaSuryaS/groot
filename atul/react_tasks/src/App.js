import './App.css'
import React from 'react'
import ToDo from './task1-ToDoList/ToDo'

function App() {
  return (
    <div className="App">
      <ToDo />
    </div>
  )
}

export default App
