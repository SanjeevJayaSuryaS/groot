const fs = require('fs')
const axios = require('axios')
const path = require('path')
function readFile() {
  return new Promise((resolve, reject) => {
    fs.readFile(
      path.join(__dirname, './breed.txt'),
      'utf8',
      function (err, data) {
        if (err) {
          return reject(new Error('file cant be read'))
        } else {
          resolve(data)
        }
      }
    )
  })
}
async function fetchData(name) {
  return axios.get(`https://dog.ceo/api/breed/${name}/images/random`)
}

async function getImage(data) {
  try {
    const promiseArray = new Array(5).fill(0).map(() => fetchData(data))
    const images = await Promise.all(promiseArray)
    const result = images.map((response) => response.data.message)

    return result
  } catch (err) {
    throw new Error('cant get images')
  }
}

function writeData(imageArray) {
  return new Promise((resolve, reject) => {
    fs.writeFile('./result.txt', JSON.stringify(imageArray, null, 2), () => {
      resolve(true)
    })
  })
}
async function main() {
  const breedName = await readFile()
  const images = await getImage(breedName)
  return await writeData(images)
}
main()
module.exports = { main, writeData, getImage, fetchData, readFile }
