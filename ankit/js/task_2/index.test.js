const fs = require('fs')
const axios = require('axios')
const { main, writeData, getImage, fetchData, readFile } = require('./index')
beforeEach(() => jest.restoreAllMocks())
describe('readFile test cases', () => {
  test('should be able to read the file and return the breed name', () => {
    return readFile().then((data) => {
      expect(data).toEqual('african')
    })
  })
  test('failed to read the file and throw an error', () => {
    const spy = jest.spyOn(fs, 'readFile').mockImplementation((_, __, cb) => {
      cb(new Error('file cant be read'))
    })
    const expectedOutput = readFile()
    expect(spy).toHaveBeenCalledTimes(1)
    return expect(expectedOutput).rejects.toEqual(
      new Error('file cant be read')
    )
  })
})
describe('write file', () => {
  test('should be able write file and produce the final result', () => {
    const spy = jest.spyOn(fs, 'writeFile').mockImplementation((_, __, cb) => {
      cb()
    })
    return writeData().then(() => {
      expect(spy).toHaveBeenCalledTimes(1)
    })
  })
  test('should not be able to write file', () => {
    const spy = jest.spyOn(fs, 'writeFile').mockImplementation((_, __, cb) => {
      cb(new Error('file cant be write'))
    })
    return writeData().catch(() => {
      expect(spy).toHaveBeenCalledTimes(1)
    })
  })
})
test('the data after reading file id african', async () => {
  const data = await readFile()
  expect(data).toEqual('african')
})
describe('axios', () => {
  test('axios able to fetch data', async () => {
    const spy = jest.spyOn(axios, 'get').mockImplementation(() => {
      return Promise.resolve({ data: { message: 'defaultValue' } })
    })
    const expectedOutput = await fetchData('african')
    expect(spy).toHaveBeenCalledTimes(1)
    expect(expectedOutput).toEqual({ data: { message: 'defaultValue' } })
  })
})
test('get images in form of array', async () => {
  const spy = jest.spyOn(axios, 'get').mockImplementation(() => {
    return Promise.resolve({ data: { message: 'default Value' } })
  })
  const expectedOutput = await getImage('african')
  expect(spy).toHaveBeenCalledTimes(5)
  expect(expectedOutput).toEqual([
    'default Value',
    'default Value',
    'default Value',
    'default Value',
    'default Value'
  ])
})
test('get images', async () => {
  return expect(getImage).rejects.toEqual(new Error('cant get images'))
})
test('success', async () => {
  const status = await main()
  expect(status).toEqual(true)
})
