const fizzBuzz = require('./fizzBuzz')
describe('includes all the successfully passing test', () => {
  test('if number passed is 3', () => {
    expect(fizzBuzz(3)).toEqual(['1', '2', 'Fizz'])
  })
  test('if n is 15', () => {
    expect(fizzBuzz(15)).toEqual([
      '1',
      '2',
      'Fizz',
      '4',
      'Buzz',
      'Fizz',
      '7',
      '8',
      'Fizz',
      'Buzz',
      '11',
      'Fizz',
      '13',
      '14',
      'FizzBuzz'
    ])
  })
})
describe('includes all the invalid if testCase', () => {
  test('if number is 0 or negative', () => {
    expect(() => {
      fizzBuzz(-1)
    }).toThrowError('Invalid')
  })
  test('if string', () => {
    expect(() => {
      fizzBuzz('a')
    }).toThrowError('Invalid')
  })
})
