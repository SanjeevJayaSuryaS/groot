const acm = function (topic) {
  let ans = 0
  let cnt = 0
  for (let xIterator = 0; xIterator < topic.length; xIterator++) {
    for (let yIterator = xIterator + 1; yIterator < topic.length; yIterator++) {
      let val = 0
      for (
        let zIterator = 0;
        zIterator < topic[xIterator].length;
        zIterator++
      ) {
        if (
          topic[xIterator][zIterator] === '1' ||
          topic[yIterator][zIterator] === '1'
        ) {
          val++
        }
      }
      if (ans < val) {
        ans = val
        cnt = 1
      } else if (ans === val) {
        cnt++
      }
    }
  }
  return [ans, cnt]
}

module.exports = acm
