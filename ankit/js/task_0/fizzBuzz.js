const fizzBuzz = function (number) {
  const result = []

  if (number < 0 || number === 0 || isNaN(number)) {
    throw new Error('Invalid')
  }
  const a = Array.from({ length: number }, (_, i) => i + 1)

  a.forEach(function (i) {
    if (i % 3 === 0 && i % 5 === 0) {
      result.push('FizzBuzz')
    } else if (i % 3 === 0) {
      result.push('Fizz')
    } else if (i % 5 === 0) {
      result.push('Buzz')
    } else {
      result.push(i.toString())
    }
  })
  return result
}

module.exports = fizzBuzz
