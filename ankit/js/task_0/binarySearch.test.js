const binarySearch = require('./binarySearch')

describe('passing test with success', () => {
  test('if n is present in the array return true', () => {
    expect(binarySearch([1, 2, 3], 3)).toEqual(true)
  })
  test('if the element found in left bound', () => {
    expect(binarySearch([1, 2, 3, 4, 5], 1)).toEqual(true)
  })
})

describe('failing test with errors', () => {
  test('if n is not present then return false', () => {
    expect(binarySearch([1, 2, 3], 5)).toEqual(false)
  })
  test('if array is empty or any error', () => {
    expect(binarySearch([], 3)).toEqual(-1)
  })
  test('if the argument is not an array', () => {
    expect(() => {
      binarySearch({ name: 'ankit' }, 2)
    }).toThrow('Invalid')
  })
})
