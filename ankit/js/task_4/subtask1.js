/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */

// eslint-disable-next-line no-var
var fileName = 'subtask_1'
async function problemOne() {
  document.getElementById('container').style.display = 'block'
  document.getElementById('abc').style.display = 'none'
  const subtask1 = await fetch('../task_4/totalNumberOfMatchesPerYear.json')
  const subtask1Data = await subtask1.json()
  const svg = d3.select('svg')
  const margin = 200
  const rect = document.getElementById('container').getBoundingClientRect()
  const width = rect.width - margin
  const height = rect.height - margin
  d3.selectAll('.title').remove()
  svg
    .append('text')
    .attr('transform', 'translate(50,0)')
    .attr('class', 'title')
    .attr('x', width / 9)
    .attr('y', height / 8)
    .attr('font-size', '20px')
    .text('total matches won per year')

  const xScale = d3.scaleBand().range([0, width]).padding(0.4)
  const yScale = d3.scaleLinear().range([height, 0])

  d3.selectAll('.main-group').remove()

  const g = svg
    .append('g')
    .attr('class', 'main-group')
    .attr('transform', 'translate(' + 100 + ',' + 100 + ')')

  xScale.domain(
    subtask1Data.map(function (d) {
      return d.year
    })
  )
  yScale.domain([
    0,
    d3.max(subtask1Data, function (d) {
      return d.value
    })
  ])

  fileName = 'subtask_1'

  g.append('g')
    .attr('transform', 'translate(0,' + height + ')')
    .call(d3.axisBottom(xScale))
    .selectAll('text')
    .attr('dx', '-2em')
    .attr('dy', '-0.2em')
    .attr('transform', 'rotate(-85)')

  g.append('g').call(
    d3
      .axisLeft(yScale)
      .tickFormat(function (d) {
        return d
      })
      .ticks(10)
  )

  const bars = g.selectAll('.bar').data(subtask1Data)

  bars
    .enter()
    .append('rect')
    .attr('class', 'bar')
    .attr('x', function (d) {
      return xScale(d.year)
    })
    .attr('y', function (d) {
      return yScale(d.value)
    })
    .attr('width', xScale.bandwidth())
    .attr('height', function (d) {
      return height - yScale(d.value)
    })
}

async function problemTwo() {
  document.getElementById('abc').style.display = 'block'
  document.getElementById('container').style.display = 'none'
  const subtask1 = await fetch('../task_4/numberOfMatchesWonPerYear.json')
  const subtask1Data = await subtask1.json()
  fileName = 'subtask_2'
  const years = Array.from({ length: 12 }, (_, i) => 2008 + i)
  Highcharts.chart('abc', {
    title: {
      text: 'Number of matches won in IPL'
    },

    yAxis: {
      title: {
        text: 'Number of matches won'
      }
    },

    xAxis: {
      accessibility: {
        rangeDescription: 'Range: 2008 to 2019'
      }
    },

    legend: {
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle'
    },

    plotOptions: {
      series: {
        label: {
          connectorAllowed: false
        },
        pointStart: 2008
      }
    },

    series: Object.keys(subtask1Data).map((teamName) => {
      return {
        name: teamName,
        data: years.map((year) =>
          subtask1Data[teamName][year] ? subtask1Data[teamName][year] : null
        )
      }
    }),

    responsive: {
      rules: [
        {
          condition: {
            maxWidth: 1000
          },
          chartOptions: {
            legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom'
            }
          }
        }
      ]
    },
    chart: {
      backgroundColor: 'wheat'
    }
  })
}

async function problemThree() {
  document.getElementById('container').style.display = 'block'
  document.getElementById('abc').style.display = 'none'
  const subtask1 = await fetch('../task_4/extraRuns.json')
  const subtask1Data = await subtask1.json()
  const svg = d3.select('svg')
  const margin = 200
  const rect = document.getElementById('container').getBoundingClientRect()
  const width = rect.width - margin
  const height = rect.height - margin
  d3.selectAll('.title').remove()
  svg
    .append('text')
    .attr('transform', 'translate(50,0)')
    .attr('class', 'title')
    .attr('x', width / 8)
    .attr('y', height / 9)
    .attr('font-size', '20px')
    .text('extraRuns')

  const xScale = d3.scaleBand().range([width, 0]).padding(0.4)
  const yScale = d3.scaleLinear().range([height, 0])

  d3.selectAll('.main-group').remove()

  const g = svg
    .append('g')
    .attr('class', 'main-group')
    .attr('transform', 'translate(' + 100 + ',' + 100 + ')')

  xScale.domain(
    subtask1Data.map(function (d) {
      return d.teamName
    })
  )
  yScale.domain([
    0,
    d3.max(subtask1Data, function (d) {
      return d.value
    })
  ])
  fileName = 'subtask_3'
  g.append('g')
    .attr('transform', 'translate(0,' + height + ')')
    .call(d3.axisBottom(xScale))
    .selectAll('text')
    .attr('dx', '-5em')
    .attr('dy', '1em')
    .attr('transform', 'rotate(-55)')

  g.append('g').call(
    d3
      .axisLeft(yScale)
      .tickFormat(function (d) {
        return d
      })
      .ticks(9)
  )

  const bars = g.selectAll('.bar').data(subtask1Data)

  bars
    .enter()
    .append('rect')
    .attr('class', 'bar')
    .attr('x', function (d) {
      return xScale(d.teamName)
    })
    .attr('y', function (d) {
      return yScale(d.value)
    })
    .attr('width', xScale.bandwidth())
    .attr('height', function (d) {
      return height - yScale(d.value)
    })
}
if (fileName === 'subtask_1') {
  problemOne()
}
window.addEventListener('resize', () => {
  console.log(fileName)
  if (fileName === 'subtask_2') {
    problemTwo()
  } else {
    fileName === 'subtask_1' ? problemOne() : problemThree()
  }
})
document.getElementById('subtask1').addEventListener('click', () => {
  fileName = 'subtask_1'
})
document.getElementById('subtask2').addEventListener('click', () => {
  fileName = 'subtask_2'
})
document.getElementById('subtask3').addEventListener('click', () => {
  fileName = 'subtask_3'
})
document.getElementById('db').addEventListener('click', async () => {
  let data, name
  if (fileName === 'subtask_1') {
    const subtask1 = await fetch('../task_4/totalNumberOfMatchesPerYear.json')
    const subtask1Data = await subtask1.json()
    data = JSON.stringify(subtask1Data)
    name = 'subtask_1.json'
  } else if (fileName === 'subtask_2') {
    const subtask1 = await fetch('../task_4/numberOfMatchesWonPerYear.json')
    const subtask2Data = await subtask1.json()
    data = JSON.stringify(subtask2Data)
    name = 'subtask_2.json'
  } else {
    const subtask1 = await fetch('../task_4/extraRuns.json')
    const subtask3Data = await subtask1.json()
    data = JSON.stringify(subtask3Data)
    name = 'subtask_3.json'
  }
  const element = document.createElement('a')
  element.setAttribute(
    'href',
    'data:text/plain;charset=utf-8,' + encodeURIComponent(data)
  )
  element.setAttribute('download', name)

  element.style.display = 'none'
  document.body.appendChild(element)

  element.click()

  document.body.removeChild(element)
})
