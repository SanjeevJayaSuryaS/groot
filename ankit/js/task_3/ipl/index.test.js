const fs = require('fs')
const path = require('path')
const {
  writeData,
  extraRuns,
  numberOfMatchesWonPerYear,
  calculateTotalMatchesPerYear
} = require(path.join(__dirname, 'ipl.js'))
const { main } = require(path.join(__dirname, './index'))
const subTask3 = require(path.join(__dirname, '../output/extraRuns.json'))
const subtask2 = require(path.join(
  __dirname,
  '../output/numberOfMatchesWonPerYear.json'
))
const subtask1 = require(path.join(
  __dirname,
  '../output/totalNumberOfMatchesPerYear.json'
))
beforeEach(() => jest.restoreAllMocks())
describe('test for the writeFile function', () => {
  test('checking test for the main function from where all the functions are called', async () => {
    const spy = jest.spyOn(fs, 'writeFile').mockImplementation((_, __, cb) => {
      cb()
    })
    await main()

    expect(spy).toHaveBeenCalledTimes(3)
  })
  test('should be able to write the data', async () => {
    const spy = jest.spyOn(fs, 'writeFile').mockImplementation((_, __, cb) => {
      cb()
    })
    const expectOut = await writeData()
    expect(expectOut).toEqual(true)
    expect(spy).toHaveBeenCalledTimes(1)
  })
  test('should not  be able to write the data', async () => {
    jest.spyOn(fs, 'writeFile').mockImplementation((_, __, cb) => {
      cb(new Error('file cant be write'))
    })
    return expect(writeData).rejects.toEqual(new Error('file cant be write'))
  })
})

describe('contains all the output test', () => {
  test('successful test for ipl subtask_3 returning json file as output', async () => {
    const result = await extraRuns()
    expect(result).toEqual(subTask3)
  })
  test('return json file as output for test for ipl subtask_2', async () => {
    const result = await numberOfMatchesWonPerYear()
    expect(result).toEqual(subtask2)
  })
  test('return output for test for ipl subtask_1', async () => {
    const result = await calculateTotalMatchesPerYear()
    expect(result).toEqual(subtask1)
  })
})
