const {
  writeData,
  extraRuns,
  numberOfMatchesWonPerYear,
  calculateTotalMatchesPerYear
} = require('./ipl')
async function main() {
  const subtask1 = calculateTotalMatchesPerYear()
  await writeData('totalNumberOfMatchesPerYear', subtask1)
  const subtask2 = numberOfMatchesWonPerYear()
  await writeData('numberOfMatchesWonPerYear', subtask2)
  const subtask3 = extraRuns()
  await writeData('extraRuns', subtask3)
}
module.exports = { main }
