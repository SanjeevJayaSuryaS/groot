const path = require('path')
const fs = require('fs')
const { main } = require(path.join(__dirname, './index'))
beforeEach(() => jest.restoreAllMocks())
describe('test for the writeFile function which write the json file', () => {
  test('checking test for the main function from where all the functions are called', async () => {
    const spy = jest.spyOn(fs, 'writeFile').mockImplementation((_, __, cb) => {
      cb()
    })
    await main()

    expect(spy).toHaveBeenCalledTimes(3)
  })
})
