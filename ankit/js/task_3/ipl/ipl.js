const fs = require('fs')
const path = require('path')
const matches = require(path.join(__dirname, '../jsonData/matches.json'))
const deliveries = require(path.join(__dirname, '../jsonData/deliveries.json'))

const writeData = (fileName, data) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(
      path.join(__dirname, `./${fileName}.json`),
      JSON.stringify(data, null, 2),
      (err) => {
        if (err) {
          return reject(new Error('file cant be write'))
        } else {
          resolve(true)
        }
      }
    )
  })
}

function calculateTotalMatchesPerYear() {
  const years = matches.map((item) => {
    return item.season
  })

  const count = {}
  years.forEach((i) => {
    count[i] = (count[i] || 0) + 1
  })
  const key = Object.keys(count)
  const objectLength = key.length
  const arr = Array.from({ length: objectLength }, (_, i) => i)
  const result = arr.map((index) => {
    return { year: key[index], value: count[key[index]] }
  })
  return result
}
function numberOfMatchesWonPerYear() {
  const result = matches.reduce((obj, item) => {
    if (obj[item.winner]) {
      obj[item.winner][item.season] = (obj[item.winner][item.season] || 0) + 1
    } else {
      obj[item.winner] = {}
    }

    return obj
  }, {})

  result['Delhi Capitals'] = {
    ...result['Delhi Capitals'],
    ...result['Delhi Daredevils']
  }
  delete result['Delhi Daredevils']
  return result
}
function extraRuns() {
  const matchIdsForYear2016 = matches.reduce((accumulator, match) => {
    if (match.season === '2016') {
      accumulator.push(match.id)
    }

    return accumulator
  }, [])

  const result = deliveries.reduce((obj, item) => {
    if (matchIdsForYear2016.includes(item.match_id)) {
      if (obj[item.batting_team]) {
        obj[item.batting_team] =
          obj[item.batting_team] + parseInt(item.extra_runs)
      } else {
        obj[item.batting_team] = parseInt(item.extra_runs)
      }
    }
    return obj
  }, {})

  const key = Object.keys(result)
  const keyLength = key.length
  const arr = Array.from({ length: keyLength }, (_, i) => i)
  const output = arr.map((index) => {
    return { teamName: key[index], value: result[key[index]] }
  })

  return output
}
module.exports = {
  writeData,
  extraRuns,
  numberOfMatchesWonPerYear,
  calculateTotalMatchesPerYear
}
