import React from 'react'
import background from './assets/home.svg'

const Home = () => {
  return <img className="home__background" src={background} alt="home" />
}

export default Home
