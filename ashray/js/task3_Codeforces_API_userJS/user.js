const fetch = require('node-fetch')
const { CODEFORCES_URL_USER } = require('../constants')

const getRating = async (handleArray) => {
  try {
    let getURL = CODEFORCES_URL_USER + '?handles='
    handleArray.forEach((handle) => {
      if (!handle || !isNaN(handle)) {
        throw Error('could not retrieve results')
      }
      getURL = getURL + handle + ';'
    })
    const userDocument = {}
    const response = await fetch(getURL)
    const responseJSON = await response.json()
    const codeforcesData = responseJSON.result
    await codeforcesData.forEach((userElement) => {
      userDocument[userElement.handle] = userElement.rating
    })
    const highestRated = Object.keys(userDocument).reduce((a, b) =>
      userDocument[a] > userDocument[b] ? a : b
    )
    return { highestRated }
  } catch (err) {
    return 'could not retrieve results'
  }
}

const maxRating = async (handleArray) => {
  try {
    let getURL = CODEFORCES_URL_USER + '?handles='
    handleArray.forEach((handle) => {
      if (!handle || !isNaN(handle)) {
        throw Error('could not retrieve results')
      }
      getURL = getURL + handle + ';'
    })
    const userDocument = {}
    const response = await fetch(getURL)
    const responseJSON = await response.json()
    const codeforcesData = responseJSON.result
    await codeforcesData.forEach((userElement) => {
      userDocument[userElement.handle] = userElement.maxRating
    })
    const maxRated = Object.keys(userDocument).reduce((a, b) =>
      userDocument[a] > userDocument[b] ? a : b
    )
    return { maxRated }
  } catch (err) {
    return 'could not retrieve results'
  }
}

module.exports = {
  getRating,
  maxRating
}
