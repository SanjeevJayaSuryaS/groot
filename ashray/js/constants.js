const ROBOHASH_URL = 'https://robohash.org/'
const CODEFORCES_URL_USER = 'http://codeforces.com/api/user.info'
const CODEFORCES_URL_CONTEST = 'http://codeforces.com/api/contest.standings'
const ROBO_NAME = 'robo'

module.exports = {
  ROBOHASH_URL,
  CODEFORCES_URL_USER,
  CODEFORCES_URL_CONTEST,
  ROBO_NAME
}
