const downloadImage = require('./indexasyncawait')
const fs = require('fs')

const fileExists = (file) => {
  try {
    if (fs.existsSync(file)) {
      return true
    } else {
      return false
    }
  } catch (err) {
    console.error(err)
  }
}

describe('downloadImage()', () => {
  test('generate and save image in images', async () => {
    const location = './task1_RoboHash_single_image/images/'
    await downloadImage(location)
    await expect(fileExists(location + 'robo.png')).toBeTruthy()
  })

  test('return error', async () => {
    const location = undefined
    await expect(downloadImage(location)).rejects.toThrowError()
    await expect(fileExists(location + 'robo.png')).toBeFalsy()
  })

  test('filewrite returns error', async () => {
    const location = undefined
    jest
      .spyOn(fs, 'writeFile')
      .mockImplementationOnce((_unusedLocation, _unusedImageBody, cb) =>
        cb(Error('mock error'))
      )
    await expect(downloadImage(location)).rejects.toThrowError()
  })
})
