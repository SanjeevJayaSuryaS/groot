const fs = require('fs')
const fetch = require('node-fetch')
const { ROBOHASH_URL } = require('../constants')

const generateTextPromise = () => {
  return new Promise((resolve, reject) => {
    resolve(Math.random().toString(20).substr(2, 10))
  })
}

const createImagePromise = (imageBody, location) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(location + '/robo.png', imageBody, (error) => {
      if (error) {
        reject(error)
      }
      resolve()
    })
  })
}

const downloadImage = async (location) => {
  try {
    const generatedString = await generateTextPromise()
    const response = await fetch(`${ROBOHASH_URL}${generatedString}`)
    const imageArrayBuffer = await response.arrayBuffer()
    const imageData = Buffer.from(imageArrayBuffer)
    await createImagePromise(imageData, location)
    return 'image generated'
  } catch (error) {
    throw new Error('Image could not be generated')
  }
}

module.exports = downloadImage
