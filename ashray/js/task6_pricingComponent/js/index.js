/* istanbul ignore file */
// eslint-disable-next-line no-unused-vars
function changeContent() {
  const checkbox = document.getElementById('checkbox')
  const basicPrice = document.getElementById('basic-price')
  const professionalPrice = document.getElementById('professional-price')
  const masterPrice = document.getElementById('master-price')

  basicPrice.innerHTML = checkbox.checked ? '$19.99' : '$199.99'
  professionalPrice.innerHTML = checkbox.checked ? '$29.99' : '$249.99'
  masterPrice.innerHTML = checkbox.checked ? '$39.99' : '$399.99'
}
