const getTopHandles = require('./contest')

describe('getTopHandles()', () => {
  test('return names of users', async () => {
    const contestID = 566
    const response = await getTopHandles(contestID)
    expect(response).toStrictEqual([
      'rng_58',
      'dreamoon_love_AA',
      'step5',
      'W4yneb0t',
      'Gullesnuffs'
    ])
  })
  test('Should return error', async () => {
    const contestID = undefined
    const response = await getTopHandles(contestID)
    expect(response).toStrictEqual('could not retrieve results')
  })
})
