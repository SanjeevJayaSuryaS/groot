const fs = require('fs')
const fetch = require('node-fetch')
const { ROBOHASH_URL } = require('../constants')

const generateTextPromise = (length) => {
  return new Promise((resolve, reject) => {
    if (isNaN(length) || length < 1) {
      reject(new Error('Image could not be generated'))
    }
    resolve(Math.random().toString(20).substr(2, length))
  })
}

const createImagePromise = (imageBody, index, location) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(location + index + '.png', imageBody, () => {
      resolve()
    })
  })
}

const downloadImage = async (stringlengthArray, location) => {
  try {
    const generatedStringArray = await Promise.all(
      stringlengthArray.map((stringLength) => generateTextPromise(stringLength))
    )
    const imageDataArray = await Promise.all(
      generatedStringArray.map(async (generatedString) => {
        const response = await fetch(`${ROBOHASH_URL}${generatedString}`)
        const imageArrayBuffer = await response.arrayBuffer()
        const imageData = await Buffer.from(imageArrayBuffer)
        return imageData
      })
    )
    await Promise.all(
      imageDataArray.map((imageData, index) =>
        createImagePromise(imageData, index, location)
      )
    )
    console.log('Images generated')
  } catch (err) {
    console.log('Image could not be generated')
  }
}

module.exports = downloadImage
