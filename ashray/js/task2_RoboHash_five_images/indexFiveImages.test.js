const downloadImage = require('./indexFiveImages')
const fs = require('fs')
const { ROBO_NAME } = require('../constants')

const fileExists = (file) => {
  try {
    if (fs.existsSync(file)) {
      return true
    } else {
      return false
    }
  } catch (err) {
    console.error(err)
  }
}

describe('downloadImage()', () => {
  test('Should generate and save images in images directory', async () => {
    const location = './task2_RoboHash_five_images/images/' + ROBO_NAME
    const lengthArray = [1, 2, 3, 4, 5]
    await downloadImage(lengthArray, location)
    const areImagesPresent = await lengthArray.every((element, index) =>
      fileExists(location + index + '.png')
    )
    expect(areImagesPresent).toBeTruthy()
  })
  test('Should return error', async () => {
    const location = './task2_RoboHash_five_images/images/' + ROBO_NAME
    const lengthArray = [1, 2, 3, 4, 5, undefined]
    await downloadImage(lengthArray, location)
    const areImagesPresent = lengthArray.every((element, index) => {
      return fileExists(location + index + '.png')
    })
    expect(areImagesPresent).toBeFalsy()
  })
})
